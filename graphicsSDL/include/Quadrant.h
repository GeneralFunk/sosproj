#ifndef QUADRANT_H
#define QUADRANT_H

#define GLM_FORCE_RADIANS

#include <SDL.h>
#include <SDL_image.h>
#include <glm/glm.hpp>
//#include "GridTree.h"
#include <list>
#include "GameObject.h"


//class GridTree;
//todo: BaseGameObject, Collision, Movement,
class GameObject;

class Quadrant
{
    public:
        Quadrant(SDL_Rect);
        virtual ~Quadrant();
        SDL_Rect Area;
        void CreateSouthEast(int,Quadrant*,SDL_Rect * TotalArea);
        void CreateEast(int,Quadrant*,SDL_Rect * TotalArea);
        void LinkTheRest(Quadrant*);
        void LinkNS(Quadrant*);
        void SortObject(GameObject *);
        void RemoveItem(GameObject *);
        bool UpdateItem(GameObject *);
        void UpdateQuadrants(int);//,SDL_mutex *);
        void AddItem(GameObject *);
        void Render(int,glm::mat4);
        Quadrant * FindQuadByPos(int,int);
        int gPosX(){return Area.x;}
        int gPosY(){return Area.y;}
        Quadrant* North=NULL, *South=NULL,*West=NULL,*East=NULL;
        int qPosX=0, qPosY=0;
        std::list<GameObject*> items;
        bool TilesSet;
        void * gtnum;
    protected:
    private:
        void UpdateItems();//SDL_mutex *);
        void Render(glm::mat4);
        void RenderByRadius(int,int,int,glm::mat4);
        bool listModded=false;

};

#endif // QUADRANT_H
