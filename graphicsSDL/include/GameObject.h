#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H


#include <string>
#include "Quadrant.h"
#include "Physics.h"
#include "H2DTexture.h"

class Physics;
class Quadrant;

class GameObject
{
    public:
        GameObject();
        GameObject(std::string);
        GameObject(H2DTexture&);
        GameObject(H2DTexture&,std::string);
        GameObject(GameObject & rhs);
        //GameObject(GameObject * rhs);
        GameObject operator=(const GameObject * rhs);
        bool CheckType(std::string);
        virtual ~GameObject();
        Physics * phys;
        Quadrant* CurrentQ;
        H2DTexture * Pic;
        std::string typecast;
        virtual void Update();
        void BBtoDSPLY();
        bool Within(int, int, int);
        void SetColor();
        void render();
        void KeepWithin( float posX, float posy, float Distance,Quadrant * Entry);
        bool isStatic,expendable;

        void(*OnClick)(GameObject *, void *);
        void(*OnMouseOver)(GameObject *);
        void(*OnCollide)(GameObject *,GameObject *,int);
        void(*AIupdate)(GameObject *);
        bool mouseRelease;
    protected:

    private:


};

#endif // GAMEOBJECT_H
