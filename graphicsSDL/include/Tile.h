#ifndef TILE_H
#define TILE_H
#include "Biomes.h"
#include "GameObject.h"
#include "H2DTexture.h"


//class GameObject;
//class H2DTexture;

class Tile : public GameObject {
    public:
        Tile();
        Tile(H2DTexture & );
        Tile operator=(const Tile & rhs);
        virtual ~Tile();
        void SetTile();
        int SetTileAvg(int);
        void SetTile(int);
        bool isSet;
        int tileType;
        bool initialized=false;
    protected:
    private:

};

#endif // TILE_H
