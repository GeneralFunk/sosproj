#ifndef BIOMES_H
#define BIOMES_H

#include <math.h>
#include <vector>
#include <string>
#include "GridTree.h"
#include "H2DTexture.h"
#include "Tile.h"
//#include "ListCode.h"
//#include "NodeCode.h"
#include "Tree.h"
#include "Enemy.h"
#include "GameObject.h"
#include "Quadrant.h"

class GridTree;

class Biomes
{
    public:
        Biomes();
        void GenerateChunkMap(GridTree * Chunk, std::vector<GameObject*> &FloorTiles,std::vector<H2DTexture> MasterList);
        double Noise1(double x, double y);
        double SmoothNoise1(double x, double y);
        double InterpolatedNoise_1(double x, double y);
        double Cosine_Interpolate(double a, double b,double x);
        double PerlinNoise_2D(double x, double y);
        void ChunkSetting(Quadrant*ChunkRoot);
        virtual ~Biomes();
        static int ii, perD;
    protected:
    private:
};

#endif // BIOMES_H
