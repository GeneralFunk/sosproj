#ifndef PHYSICS_H
#define PHYSICS_H

#include <SDL.h>
#include <stdlib.h>
#include <cmath>
#include "Quadrant.h"
#include "GameObject.h"
//#include "NodeCode.h"

class Quadrant;
class GameObject;

enum CollType{
Seperate,
Contains,
Contained,
InterSects,
Cursor,
};

class Physics
{
    public:
        Physics();
        virtual ~Physics();
        void Update(GameObject *);
        void HomeIn(float, float, GameObject *);
        bool moveTween(GameObject *);
        bool TeleportTo(float,float,GameObject *);
        GameObject * Collision(GameObject *);

        float posX,posY,posZ;
        float lposX,lposY;
        glm::vec3 Direction;
        glm::vec3 Destination;
        glm::vec2 BBoffset;
        int oX,oY;
        int moveC;
        float MaxSpeed;
        CollType Collided;
        bool Collidable;
        SDL_Rect BoundingRect;
        bool Rev;
        int collideResponse;
        int Ctimes;
        Uint32 CurTime;
    protected:
    private:

};

#endif // PHYSICS_H
