#ifndef PRIMITIVES_H
#define PRIMITIVES_H

#include <gl/glew.h>
#include <glm/glm.hpp>
#include <vector>

class Primitives
{
    public:
        typedef std::vector<glm::vec4> v4v;
        Primitives();
        virtual ~Primitives();
        int generateCube(glm::vec3 dimensions,
                                 glm::vec3 offset,
                                 v4v Color,
                                 v4v TexCoords,
                                 GLuint * VBO,
                                 GLuint * IBO,
                                 GLuint program);
        int loadCustom(v4v Vertices,v4v Normals,
                              v4v Color,v4v TexCoords,std::vector<unsigned int> Indices,
                              GLuint * VBO,GLuint * IBO, GLuint program);

    protected:
    private:
        struct UniqueTC{
            glm::vec4 UTC[4];
            int indicSize;
            GLuint vao;
            GLuint vbo;
        };
        static std::vector<UniqueTC> Uniq;
        const int CubeVertices = 8;
        void makeVBO(GLuint * VBO, GLuint * IBO,
                    unsigned int indicSize,unsigned int structSize, GLuint program);
        void makeVAO(GLuint * VBO, GLuint * VAO,
                          unsigned int indicSize, unsigned int structSize, GLuint program );

        struct VNCT{
            glm::vec4 vertex;
            glm::vec4 normals;
            glm::vec4 color;
            glm::vec4 texcoords;
        };
        VNCT * vnctStruct;
        unsigned int * indices;


};

#endif // PRIMITIVES_H
