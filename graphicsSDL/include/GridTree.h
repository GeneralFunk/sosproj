#ifndef GRIDTREE_H
#define GRIDTREE_H

#include <SDL.h>
#include <SDL_image.h>
#include <vector>
#include <gl/glew.h>
//#include "NodeCode.h"
#include "Biomes.h"
#include "Quadrant.h"
#include "GameObject.h"


class GridTree
{
    public:
        GridTree(){}
        GridTree(int WorldPosX, int WorldPosY,int SquareWidth, int Dividend);
        GridTree operator=(const GridTree & rhs);

        void init(int WorldPosX, int WorldPosY,int SquareWidth, int Dividend,int num);
        void initObjectsVao();
        void GenerateSurroundingChunks(std::vector<GridTree*> & MasterGTList,std::vector<H2DTexture> TileSheet);
        virtual ~GridTree();
        Quadrant * QuadByWorldPos(Quadrant *,int,int);
        Quadrant * QuadByQuadPos(int qx,int qy);
        void Sort(GameObject*);
        bool Stitch(GridTree * Patch);

        Quadrant* TopLeft;
        SDL_Rect mapArea;
        std::vector<Quadrant*> theMap;
        GridTree *North,*South,*East,*West;//*NW,*NE,*SW,*SE;

    protected:
    private:
        int gtnum;
        void split(int);
        void UnStitch();
        void DeleteMap();
        int dx,dy;
        void LinkMap();
};

#endif // GRIDTREE_H
