#ifndef INVENTORY_H
#define INVENTORY_H

#include <list>
#include <sstream>
#include "GameObject.h"
//#include "ListCode.h"
//#include "NodeCode.h"

class InvItem
{
    public:
    int slot=-1;
    int StackSize = 1;
    int Stacked = 0;
    GameObject * Item = 0;
    InvItem()
    {
        slot=-1;
        StackSize = 1;
        Stacked = 0;
        Item = 0;
    }
    InvItem(GameObject * Obj, int SS, int slotted)
    {
        Item = Obj;
        StackSize = SS;
        Stacked=0;
        slot = slotted;
    }
    void IncrementStack(){
        Stacked++;
    }

    void IncrementStack(int incre){
        Stacked = Stacked + incre;
        if(StackSize < Stacked)
        {
            Stacked = StackSize;
        }
    }
};

class Inventory
{
    public:
        Inventory();
        void AddItem(GameObject *, int,int);
        std::string PrintInv(int Spot);
        virtual ~Inventory();
        int thisManyItems;
    protected:
    private:
        std::list<InvItem*> theInventory;
        int lastSlot=-1;

};

#endif // INVENTORY_H
