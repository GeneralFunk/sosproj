#ifndef TREE_H
#define TREE_H

#include "GameObject.h"
#include "H2DTexture.h"
//#include "Inventory.h"
#include "Player.h"
#include "Wood.h"

class Tree : public GameObject
{
    public:
        Tree();
        Tree(H2DTexture &);
        static void Clicked(GameObject *, void *);
        static void MouseOv(GameObject *);
        virtual ~Tree();
    protected:
    private:
};

#endif // TREE_H
