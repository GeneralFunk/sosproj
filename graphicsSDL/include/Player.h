#ifndef PLAYER_H
#define PLAYER_H

#define GLM_FORCE_RADIANS

#include <stdio.h>
#include <string.h>
#include <SDL.h>
#include <glm/glm.hpp>
#include "GameObject.h"
#include "H2DTexture.h"
#include "Inventory.h"

class Player : public GameObject
{
    public:
        Player();
        Player(H2DTexture & );
        bool Move(glm::vec3 &);//,SDL_mutex *);
        void setID(unsigned long long);
        virtual ~Player();
        unsigned long long ServerId;
        std::string Name;
        bool UseDelta;
        Uint32 LastUpdate;
        static void MouseOver(GameObject *);
        static Inventory * PlayerInv;
    protected:
    private:

       const Uint8 *kstate;
       Uint8 *oldkstate;
       int KeyLength;
};

#endif // PLAYER_H
