#ifndef HANDLETTF_H
#define HANDLETTF_H

#define GLM_FORCE_RADIANS

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <stdio.h>
#include <string>
#include <cmath>
#include "H2DTexture.h"


class handleTTF
{
    public:
        handleTTF();
        virtual ~handleTTF();

        bool init(SDL_Renderer *);
        bool loadMedia(std::string,int);

        void render(std::string);
        void render(std::string, glm::mat4);
        void close();
        void setPos(int,int);
        //void setColor(SDL_Color);
        void getTextWidthHeight(std::string,int width, int height);
        int getW()
        {
            return mWidth;
        }
        int getH()
        {
            return mHeight;
        }

        bool doCenter;
    protected:
    private:
        GLuint textex=0;
        //void free();
        //bool loadTextToRender( std::string, SDL_Color);
        TTF_Font *gFont;
        //SDL_Color mColor;
        std::string lastText;
        SDL_Renderer * gRenderer;

        //SDL_Texture* mTexture;
        int posX,posY;
		int mWidth;
		int mHeight;
};

#endif // HANDLETTF_H
