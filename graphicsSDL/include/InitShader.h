#ifndef INITSHADER_H
#define INITSHADER_H

#include <cstdio>
#include <cmath>
#include <string>
#include <iostream>
#include <windows.h>
#include <gl/glew.h>
//#include <gl/freeglut.h>

#define BUFFER_OFFSET( offset )   ((GLvoid*) (offset))

#ifndef M_PI
#  define M_PI  3.14159265358979323846
#endif


//#define GLFW_NO_GLU
//#define  GLFW_INCLUDE_GL3


class InitShader
{
    public:
        InitShader(){}

        virtual ~InitShader() {}

//GLuint doInitShader(const std::string vertexShaderFile,const std::string fragmentShaderFile);

        const GLfloat  DegreesToRadians = M_PI / 180.0;
    static std::string WorkingPath;
    protected:
    private:
};

#endif // INITSHADER_H
