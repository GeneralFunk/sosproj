#ifndef ENEMY_H
#define ENEMY_H
#include "GameObject.h"

class Enemy : public GameObject
{
    public:
        Enemy();
        Enemy(H2DTexture &);

        static void UpdateAI(GameObject *);
        virtual ~Enemy();

        std::string EnemyName,EnemyType;
        GameObject * Target;
    protected:
    private:
        static void GetClosestTarget(Enemy *);
};

#endif // ENEMY_H
