#ifndef SHADERPROGRAM_H
#define SHADERPROGRAM_H

#include <GL\glew.h>

#include <iostream>
#include <cstdarg>
#include <fstream>
#include <vector>
//#include <string.h>

using namespace std;

class ShaderProgram {
public:
        ShaderProgram(const char * types, ...);
        ~ShaderProgram();
        void begin();
        void end();

        GLuint uniform(const char * name);
        ShaderProgram * uniform(const char * name, GLuint * location);

        const static char VERTEX_SHADER          = 'v';
        const static char FRAGMENT_SHADER        = 'f';
        const static char GEOMETRY_SHADER        = 'g';
        const static char TESS_EVALUATION_SHADER = 'e';
        const static char TESS_CONTROL_SHADER    = 'c';
        GLuint program;

protected:
private:

        vector<GLuint> shaders;

        void loadShader(const char * path, int type);
};
#endif // SHADERPROGRAM_H
