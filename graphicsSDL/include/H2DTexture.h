#ifndef H2DTEXTURE_H
#define H2DTEXTURE_H

#define GLM_FORCE_RADIANS

#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
//#include <stdlib.h>
#include <string>
#include <cmath>
#include <vector>
#include <gl/glew.h>
#include <gl/gl.h>
#include <gl/glu.h>
//#include <gl/glext.h>
#include <glm/glm.hpp>
#include <glm\gtc\type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

//#include "SpriteBatch.h"
//#include "InitShader.h"
#include "ShaderProgram .h"
#include "Primitives.h"

struct RGBA
{
    Uint8 r,g,b,a;
    bool U;
};

class H2DTexture
{
    public:
        int Offsetx, Offsety,dimension,renderType;
        H2DTexture();
        H2DTexture(const H2DTexture &);
        H2DTexture operator=(const H2DTexture & rhs);
        virtual ~H2DTexture();
        bool loadFromFile( std::string path );

        void init(SDL_Renderer*);

		//Deallocates texture
		void free();


        void setupAnimation(SDL_Rect,float,int);

        void setPosition(int, int);
        void setDisplay(SDL_Rect);
        void setCenter(SDL_Point);
        void modAbsScale(float);
        void modRelScale(float);
        void modSpScale(double,bool);

        void setDepth(float);

        void AutoRender(glm::mat4);
        void render();
		void render(glm::mat4);
		void renderTransOnly(glm::mat4);

        void initArray();

        void UpdateAnimate();
        void PauseAnim(bool TogPause);
        void StopAnim(bool ReStart);

        void BoingEffect();
        void setEffect(int);

		//Gets image dimensions
		int getWidth(){return mWidth;}
		int getHeight(){return mHeight;}

        //SDL_RendererFlip mFlip;
        Uint32 CurTime,EffectTimeStart,EffectTimeEnd;
//        ListCode<H2DTexture*> Children;
        float depth;
        int AnimateState;
         int csize;
		//SDL_Texture* mTexture;
		bool isDead;

        static GLuint Prog;
        GLuint texture;
        static GLuint BoundTexture;
        static glm::vec4 PlayerPos;
        static std::vector<GLuint> VaosToDeletes;
        static std::vector<GLuint> VbosToDeletes;
		SDL_Rect* mFrame;
		SDL_Rect* mDisplay;
        bool cubeit;
		int MaxFrames;

		float FramesPerSecond;
		bool masterTexture;
    protected:
    private:

        //RGBA Color;
        SDL_Renderer* gRenderer;

		int FrameCount;
		int FrameOffset;
		int pause;
		int stop;

		int indicSize;

		int mWidth;
		int mHeight;

		int posX, posY,frameStartX;
		 //glm::vec4 * Vertices;
		 //glm::vec4 * Colors;
		 //glm::vec4 * TexCoords;
         //GLuint tbuffer=0;
		 //GLuint tvao=0;
		 GLuint tvbo=0;
		 GLuint tibo=0;
		 GLuint shaFoffset;
		 GLuint tex2shad;
		 GLuint transformGL;
		 GLuint theView;
		 GLuint sampler;
		 GLuint target;
        glm::mat4 transformer;
      //  static GLuint vPosition;
      //  static GLuint vColor;
      //  static GLuint vTexCoords;

};

#endif // H2DTEXTURE_H
