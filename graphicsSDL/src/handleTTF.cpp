#include "../include/handleTTF.h"

handleTTF::handleTTF()
{
    gFont = NULL;
    //mTexture = NULL;
    mWidth=0;
    mHeight=0;
    posX=0;
    posY=0;
    lastText = "";
//    mColor = SDL_Color();
//    mColor.a = 255;mColor.b=0,mColor.g=0;mColor.r=0;
    //ctor
}

handleTTF::~handleTTF()
{
    gFont = 0;
    gRenderer = 0;
    //dtor
    //free();
}

bool handleTTF::init(SDL_Renderer * theRend)
{
    bool success = true;
    if( TTF_Init() == -1 )
    {
        printf( "SDL_ttf could not initialize! SDL_ttf Error: %s\n", TTF_GetError() );
        success=false;
    }
    gRenderer = theRend;
    return success;
}

void handleTTF::setPos(int x,int y)
{
    posX=x;posY=y;
}

/*void handleTTF::setColor(SDL_Color Color)
{
    mColor = Color;
}*/

bool handleTTF::loadMedia(std::string path,int fontsize)
{
    bool success = true;

    gFont = TTF_OpenFont( path.c_str(), fontsize );
	if( gFont == NULL )
	{
		printf( "Failed to load lazy font! SDL_ttf Error: %s\n", TTF_GetError() );
		success = false;
	}

    return success;
}

/*bool handleTTF::loadTextToRender( std::string textureText, SDL_Color textColor)
{
    free();

	//Render text surface
	SDL_Surface* textSurface = TTF_RenderText_Solid( gFont, textureText.c_str(), textColor );
	if( textSurface == NULL )
	{
		printf( "Unable to render text surface! SDL_ttf Error: %s\n", TTF_GetError() );
	}
	else
	{
		//Create texture from surface pixels
        mTexture = SDL_CreateTextureFromSurface( gRenderer, textSurface );
		if( mTexture == NULL )
		{
			printf( "Unable to create texture from rendered text! SDL Error: %s\n", SDL_GetError() );
		}
		else
		{
			//Get image dimensions
			mWidth = textSurface->w;
			mHeight = textSurface->h;
		}

		//Get rid of old surface
		SDL_FreeSurface( textSurface );
	}

	//Return success
	return mTexture != NULL;
}*/

/*void handleTTF::free()
{
	//Free texture if it exists
	if( mTexture != NULL )
	{
		SDL_DestroyTexture( mTexture );
		mTexture = NULL;
		mWidth = 0;
		mHeight = 0;
	}
}*/

void handleTTF::render( std::string DisplayText)
{
    SDL_Surface* msg = TTF_RenderText_Blended( gFont,DisplayText.c_str(),{255,255,255} );

    // create new texture, with default filtering state (==mipmapping on)
    //GLuint textex;
    if(textex == 0)
        glGenTextures( 1, &textex );
    if(H2DTexture::BoundTexture != textex)
    {
        glBindTexture( GL_TEXTURE_2D, textex );
        H2DTexture::BoundTexture = textex;
    }
    // disable mipmapping on the new texture
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
    glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, msg->w, msg->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, msg->pixels );

    SDL_FreeSurface( msg );
    //glEnable( GL_BLEND );
    //glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
    int h,w;
    TTF_SizeUTF8(gFont,DisplayText.c_str(),&w,&h);
    glBegin(GL_QUADS);
    if(doCenter)
    {
        glTexCoord2f( 0.0f, 0.0f ); glVertex3f( posX-w/2, posY-h/2,600 );
        glTexCoord2f( 1.0f, 0.0f ); glVertex3f( posX+w/2, posY-h/2,600 );
        glTexCoord2f( 1.0f, 1.0f ); glVertex3f( posX+w/2,  posY+h/2,600 );
        glTexCoord2f( 0.0f, 1.0f ); glVertex3f( posX-w/2,  posY+h/2, 600);
    }
    else
    {
        glTexCoord2f( 0.0f, 0.0f ); glVertex3f( posX, posY,600 );
        glTexCoord2f( 1.0f, 0.0f ); glVertex3f( posX+w, posY,600 );
        glTexCoord2f( 1.0f, 1.0f ); glVertex3f( posX+w, posY+h,600 );
        glTexCoord2f( 0.0f, 1.0f ); glVertex3f( posX, posY+h,600 );
    }
    glEnd();


}

void handleTTF::getTextWidthHeight(std::string DisplayText,int w, int h)
{
    TTF_SizeUTF8(gFont,DisplayText.c_str(),&w,&h);
}


glm::vec4 normalize(glm::vec4 a, glm::vec4 b, float length)
{
    //get the distance between a and b along the x and y axes
    float dx = b.x - a.x;
    float dy = b.y - a.y;
    //right now, sqrt(dx^2 + dy^2) = distance(a,b).
    //we want to modify them so that sqrt(dx^2 + dy^2) = the given length.
    dx = dx * length / glm::distance(a,b);
    dy = dy * length / glm::distance(a,b);
    glm::vec4 c =  glm::vec4();
    c.x = a.x + dx;
    c.y = a.y + dy;
    return c;
}

void handleTTF::render(std::string DisplayText, glm::mat4 VP)
{
    SDL_Surface* msg = TTF_RenderText_Blended( gFont,DisplayText.c_str(),{255,255,255} );

    if(textex == 0)
        glGenTextures( 1, &textex );
    if(H2DTexture::BoundTexture != textex)
    {
        glBindTexture( GL_TEXTURE_2D, textex );
        H2DTexture::BoundTexture = textex;
    }
    // disable mipmapping on the new texture
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
    glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, msg->w, msg->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, msg->pixels );

    SDL_FreeSurface( msg );

    int h,w;
    TTF_SizeUTF8(gFont,DisplayText.c_str(),&w,&h);
    h*=4;
    w*=4;

    glm::vec4 Pos3;
    glBegin (GL_QUADS);
    float depth = 1.1;
    if(doCenter)
    {
        Pos3 = glm::vec4(posX-(w/2),posY-(h/2),depth,1);
        glTexCoord2f (0.0, 0.0);
        Pos3 = VP*Pos3;
        glVertex3f (Pos3.x, Pos3.y, Pos3.z);

        glTexCoord2f (1.0, 0.0);
        Pos3 = glm::vec4(posX+(w/2),posY-(h/2),depth,1);
        Pos3 = VP*Pos3;
        glVertex3f (Pos3.x,Pos3.y, Pos3.z);

        glTexCoord2f (1.0, 1.0);
        Pos3 = glm::vec4(posX+(w/2),posY +(h/2),depth,1);
        Pos3 = VP*Pos3;
        glVertex3f (Pos3.x,Pos3.y, Pos3.z);

        glTexCoord2f (0.0, 1.0);
        Pos3 = glm::vec4(posX-(w/2),posY+(h/2),depth,1);
        Pos3 = VP*Pos3;
        glVertex3f (Pos3.x,Pos3.y, Pos3.z);
    }
    else
    {
        Pos3 = glm::vec4(posX,posY,depth,1);
        glTexCoord2f (0.0, 0.0);
        Pos3 = VP*Pos3;
        glVertex3f (Pos3.x, Pos3.y, Pos3.z);

        glTexCoord2f (1.0, 0.0);
        Pos3 = glm::vec4(posX+w,posY,depth,1);
        Pos3 = VP*Pos3;
        glVertex3f (Pos3.x,Pos3.y, Pos3.z);

        glTexCoord2f (1.0, 1.0);
        Pos3 = glm::vec4(posX+w,posY +h,depth,1);
        Pos3 = VP*Pos3;
        glVertex3f (Pos3.x,Pos3.y, Pos3.z);

        glTexCoord2f (0.0, 1.0);
        Pos3 = glm::vec4(posX,posY+h,depth,1);
        Pos3 = VP*Pos3;
        glVertex3f (Pos3.x,Pos3.y, Pos3.z);
    }

    glEnd ();
}


void handleTTF::close()
{
	//Free loaded images
	//free();
	//Free global font
	TTF_CloseFont( gFont );
	gFont = NULL;

	TTF_Quit();
}
