#include "../include/Tile.h"

Tile::Tile()
{
    if(initialized)
        return;
    Pic = NULL;
    phys = new Physics();
    typecast = "Tile";
    CurrentQ = NULL;
    isStatic = true;

    OnCollide = NULL;
    OnMouseOver = NULL;
    OnClick = NULL;

    isSet = false;
    initialized = true;
    tileType = 0;
    //ctor
}

Tile::Tile(H2DTexture & theTex)
{
    if(initialized)
        return;
    phys = new Physics();
    typecast = "Tile";
    CurrentQ = NULL;
    isStatic = true;

    OnCollide = NULL;
    OnMouseOver = NULL;
    OnClick = NULL;

    Pic = new H2DTexture(theTex);
    isSet = false;
    initialized = true;
    tileType = 0;
    //ctor
}

Tile Tile::operator=(const Tile & rhs)
{
    initialized=rhs.initialized;
    phys = rhs.phys;
    typecast = rhs.typecast;
    CurrentQ = rhs.CurrentQ;
    isStatic = rhs.isStatic;

    OnCollide = rhs.OnCollide;
    OnMouseOver = rhs.OnMouseOver;
    OnClick = rhs.OnClick;

    Pic = rhs.Pic;
    isSet = rhs.isSet;
    tileType = rhs.tileType;
    return *this;
}

void Tile::SetTile(int setTo)
{
    switch(setTo)
    {
        case 0:
            //Snow(this);
            isSet = true;
            tileType = 0;
            Pic->setupAnimation({0,0,64,64},0,0);
        break;
        case 1:
            //Grass(this);
        isSet = true;
        tileType = 1;
        Pic->setupAnimation({64,0,64,64},0,0);
        break;
        case 2:
            //Dirt(this);
        isSet = true;
        tileType = 2;
        Pic->setupAnimation({128,0,64,64},0,0);
        break;
        case 3:
            //Sand(this);
        isSet = true;
        tileType = 3;
        Pic->setupAnimation({192,0,64,64},0,0);
        break;
        case 4:
            //River(this);
        isSet = true;
        tileType = 4;
        Pic->setupAnimation({384,0,64,64},0.5f,2);
        //phys->Collidable = true;
        phys->collideResponse=1;
        BBtoDSPLY();
        break;
        case 5:
            //Ocean(this);
        isSet = true;
        tileType = 5;
        Pic->setupAnimation({384,0,64,64},0,0);
        //phys->Collidable = true;
        phys->collideResponse=1;
        BBtoDSPLY();
            break;
        case 6:
            //Trippy(this);
            isSet = true;
            tileType = 6;
            Pic->setupAnimation({320,0,64,64},0,0);
            break;
        case 7:
            //Stone(this);
        isSet = true;
        tileType = 7;
        Pic->setupAnimation({256,0,64,64},0,0);
        //phys->Collidable = true;
        phys->collideResponse=1;
        BBtoDSPLY();
            break;
    }
}
void Tile::SetTile()
{

    Biomes B = Biomes();
    int setTo = B.PerlinNoise_2D((int)(phys->posX/(1256/2)),(int)(phys->posY/(1256/2)))*50+57;
    setTo = setTo%100;
    if((setTo%13)<3)
    {
        //Trippy(this);
        isSet = true;
        tileType = 6;
        Pic->setupAnimation({320,0,64,64},0,0);
    }
    else if(setTo <=10)
    {
        //Snow(this);
        isSet = true;
        tileType = 0;
        Pic->setupAnimation({0,0,64,64},0,0);
    }
    else if(setTo <=40)
    {
        //Grass(this);
        isSet = true;
        tileType = 1;
        Pic->setupAnimation({64,0,64,64},0,0);
    }
    else if(setTo <=60)
    {
        //Dirt(this);
        isSet = true;
        tileType = 2;
        Pic->setupAnimation({128,0,64,64},0,0);
    }
    else if(setTo <=70)
    {
        //Sand(this);
        isSet = true;
        tileType = 3;
        Pic->setupAnimation({192,0,64,64},0,0);
    }
    else if(setTo <=77)
    {
        //Stone(this);
        isSet = true;
        tileType = 7;
        Pic->setupAnimation({256,0,64,64},0,0);
       //phys->Collidable = true;
        phys->collideResponse=1;
        BBtoDSPLY();
    }
    else if(setTo <=85)
    {
        //River(this);
        isSet = true;
        tileType = 4;
        Pic->setupAnimation({384,0,64,64},0.5f,2);
       //phys->Collidable = true;
        phys->collideResponse=1;
        BBtoDSPLY();
    }
    else
    {
        //Ocean(this);
        isSet = true;
        tileType = 5;
        Pic->setupAnimation({384,0,64,64},0,0);
       //phys->Collidable = true;
        phys->collideResponse=1;
        BBtoDSPLY();
    }

}
int Tile::SetTileAvg(int Last)
{

    Biomes B = Biomes();
    int setTo = B.PerlinNoise_2D((int)(phys->posX/(256)),(int)(phys->posY/(256)))*50+57;
    setTo = (setTo%100+Last)/2;
      if((setTo%13)<3)
    {
        //Trippy(this);
        isSet = true;
        tileType = 6;
        Pic->setupAnimation({320,0,64,64},0,0);
    }
    else if(setTo <=10)
    {
        //Snow(this);
        isSet = true;
        tileType = 0;
        Pic->setupAnimation({0,0,64,64},0,0);
    }
    else if(setTo <=40)
    {
        //Grass(this);
        isSet = true;
        tileType = 1;
        Pic->setupAnimation({64,0,64,64},0,0);
    }
    else if(setTo <=60)
    {
        //Dirt(this);
        isSet = true;
        tileType = 2;
        Pic->setupAnimation({128,0,64,64},0,0);
    }
    else if(setTo <=70)
    {
        //Sand(this);
        isSet = true;
        tileType = 3;
        Pic->setupAnimation({192,0,64,64},0,0);
    }
    else if(setTo <=77)
    {
        //Stone(this);
        isSet = true;
        tileType = 7;
        Pic->setupAnimation({256,0,64,64},0,0);
       //phys->Collidable = true;
        phys->collideResponse=1;
        BBtoDSPLY();
    }
    else if(setTo <=85)
    {
        //River(this);
        isSet = true;
        tileType = 4;
        Pic->setupAnimation({384,0,64,64},0.5f,2);
       //phys->Collidable = true;
        phys->collideResponse=1;
        BBtoDSPLY();
    }
    else
    {
        //Ocean(this);
        isSet = true;
        tileType = 5;
        Pic->setupAnimation({384,0,64,64},0,0);
       //phys->Collidable = true;
        phys->collideResponse=1;
        BBtoDSPLY();
    }
    return setTo;
}


Tile::~Tile()
{
   // if(CurrentQ)
   //     CurrentQ->RemoveItem(this);
    delete Pic;
    delete phys;

    phys = NULL;
    CurrentQ = NULL;
    Pic = NULL;
}
