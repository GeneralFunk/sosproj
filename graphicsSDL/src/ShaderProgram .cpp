#include "../include/ShaderProgram .h"


ShaderProgram::ShaderProgram(const char * types, ...) {
        program = glCreateProgram();
       // int scount = strlen(types);
        va_list va_shaders;
        va_start(va_shaders, types);

        const char * path = 0;
        for(int i = 0; types[i]; i++) {

                switch (types[i]) {
                case 'v':
                        path = va_arg(va_shaders, const char *);
                        loadShader(path, GL_VERTEX_SHADER);
                        break;
                case 'f':
                        path = va_arg(va_shaders, const char *);
                        loadShader(path, GL_FRAGMENT_SHADER);
                        break;
                case 'g':
                        path = va_arg(va_shaders, const char *);
                        loadShader(path, GL_GEOMETRY_SHADER);
                        break;
                case 'e':
                        path = va_arg(va_shaders, const char *);
                        loadShader(path, GL_TESS_EVALUATION_SHADER);
                        break;
                case 'c':
                        path = va_arg(va_shaders, const char *);
                        loadShader(path, GL_TESS_CONTROL_SHADER);
                        break;
                default:
                        break;
                }
        }

        va_end(va_shaders);

        glLinkProgram(program);
        glUniform1f(glGetUniformLocation(program, "PI"), 3.1415926535897932384626433832795);
}

ShaderProgram::~ShaderProgram() {
        glDeleteProgram(program);
        for (vector<GLuint>::iterator sh = shaders.begin(); sh != shaders.end(); ++sh) {
                glDeleteShader(*sh);
        }
}

void ShaderProgram::begin() {
        glUseProgram(program);
}

void ShaderProgram::end() {
        glUseProgram(0);
}

GLuint ShaderProgram::uniform(const char * name) {
        return glGetUniformLocation(program, name);
}
ShaderProgram * ShaderProgram::uniform(const char * name, GLuint * location) {
        GLuint loc = glGetUniformLocation(program, name);
        if (location) *location = loc;
        return this;
}


void ShaderProgram::loadShader(const char * path, int type) {

        ifstream shaderFile;

        shaderFile.open(path, ifstream::in | ifstream::binary | ifstream::ate);
        if (!shaderFile) {
                cout << "Could not open or find \"" << path << '"' << endl;
                return;
        }

        int soulen = shaderFile.tellg();
        shaderFile.seekg(ios::beg);

        if (soulen <= 0) {
                cout << "Empty file in \"" << path << '"' << endl;
                return;
        }

        GLchar * shsou = (GLchar*)new char[soulen+1];

        for (int i = 0; i < soulen; i++) {
                char c;
                shaderFile.read(&c,1);
                shsou[i] = (c!='\r')? c : '\n' ;
        }
        shsou[soulen] = '\0';
        shaderFile.close();

        GLuint shader = glCreateShader(type);
        glShaderSource(shader, 1, (const GLchar**)&shsou, &soulen);
        glCompileShader(shader);
        delete[] shsou;

        GLint compiled;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);

        if(!compiled) {
                char * compileLog;
                int logLen;
                int sz;
                glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLen);
                compileLog = new char[logLen];
                glGetShaderInfoLog(shader, (unsigned int)logLen, &sz, compileLog);
                cout << compileLog << endl;
                glDeleteShader(shader);
                delete[] compileLog;
                return;
        }

        glAttachShader(program, shader);


        shaders.push_back(shader);
}
