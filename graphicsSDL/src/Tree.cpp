#include "../include/Tree.h"

Inventory * Player::PlayerInv;

Tree::Tree():GameObject("Tree")
{
    //ctor
    OnMouseOver = &MouseOv;
    OnClick = &Clicked;
}

Tree::Tree(H2DTexture & thePic):GameObject(thePic,"Tree")
{
    //ctor
    OnMouseOver = &MouseOv;
    OnClick = &Clicked;
}

void Tree::Clicked(GameObject * Obj,void * Inv)
{
    //if(!Inv)
    //{
        Obj->Pic->setEffect(1000);
      //  return;
    //}

    //Inventory * P1inv = (Inventory *)Inv;
    Player::PlayerInv->AddItem(new Wood(),99,10);
}

void Tree::MouseOv(GameObject * Obj)
{
    if(SDL_GetMouseState( NULL, NULL )&SDL_BUTTON(1))
    {
        if(Obj->mouseRelease)
            Obj->OnClick(Obj,NULL);
        Obj->mouseRelease = false;
    }
    else
        Obj->mouseRelease = true;
}

Tree::~Tree()
{
 //   if(CurrentQ)
 //       CurrentQ->RemoveItem(this);
    delete Pic;
    delete phys;

    phys = NULL;
    CurrentQ = NULL;
    Pic = NULL;
}
