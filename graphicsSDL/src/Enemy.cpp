#include "../include/Enemy.h"


Enemy::Enemy():GameObject("Enemy")
{
    //ctor
    phys->collideResponse = 7;
    Target = NULL;
    AIupdate = &UpdateAI;
    phys->MaxSpeed = 17;
}

Enemy::Enemy(H2DTexture & thePic):GameObject(thePic,"Enemy")
{
    //ctor
    phys->collideResponse = 7;
    Target = NULL;
    AIupdate = &UpdateAI;
    phys->MaxSpeed = 17;
    Pic->cubeit = true;
}

void Enemy::GetClosestTarget(Enemy * Parent)
{
    Quadrant * HoldQ = Parent->CurrentQ;
    //HoldQ = HoldQ->FindQuadByPos(HoldQ->Area.x-1,HoldQ->Area.y-1);
    std::list<GameObject*>::iterator Hold = HoldQ->items.begin();
    Parent->Target = NULL;
    for(int x = Parent->CurrentQ->Area.x-Parent->CurrentQ->Area.w+1; x < Parent->CurrentQ->Area.x+Parent->CurrentQ->Area.w*2; x+=Parent->CurrentQ->Area.w)
    {
        for(int y = Parent->CurrentQ->Area.y-Parent->CurrentQ->Area.h+1; y < Parent->CurrentQ->Area.y+Parent->CurrentQ->Area.h*2; y+=Parent->CurrentQ->Area.h)
        {
            if(HoldQ != HoldQ->FindQuadByPos(x,y))
            {
                HoldQ = HoldQ->FindQuadByPos(x,y);

                Hold = HoldQ->items.begin();
                while(Hold != HoldQ ->items.end() && *Hold != (GameObject *)0xFEEEFEEE)
                {

                    if((*Hold) != (GameObject *)Parent)
                    {
                        if((*Hold)->typecast=="Player")
                        {
                            if(!Parent->Target)
                            {
                                Parent->Target = *Hold;
                            }
                            else
                            {
                                float tx = Parent->Target->phys->posX-Parent->phys->posX;
                                float ty = Parent->Target->phys->posY-Parent->phys->posY;
                                float nx = (*Hold)->phys->posX-Parent->phys->posX;
                                float ny = (*Hold)->phys->posY-Parent->phys->posY;
                                if(tx*tx+ty*ty > nx*nx+ny*ny)
                                {
                                    Parent->Target = *Hold;
                                }
                            }
                        }
                    }
                    Hold++;
                }

            }

        }
    }
}

void Enemy::UpdateAI(GameObject * Parent)
{
    Enemy * Hold = (Enemy *)Parent;
    GetClosestTarget(Hold);
    if(Hold->Target){
        Hold->phys->HomeIn(Hold->Target->phys->posX,Hold->Target->phys->posY,Parent);
    }
    Hold->Pic->setPosition(Hold->phys->posX,Hold->phys->posY);

    Hold->Pic->UpdateAnimate();
}

Enemy::~Enemy()
{
    //dtor
}
