#include "../include/Inventory.h"

Inventory::Inventory()
{
    //ctor
    theInventory = std::list<InvItem*>();
    thisManyItems = 0;
}

void Inventory::AddItem(GameObject * AddThis,int stacksize, int incStack)
{
    if(theInventory.empty())
    {
        theInventory.push_back(new InvItem(AddThis,stacksize,++lastSlot));
        (*theInventory.begin())->IncrementStack(incStack);
        thisManyItems++;
        return;
    }
    std::list<InvItem*>::iterator Hold = theInventory.begin();
    while(Hold != theInventory.end())
    {
        if((*Hold)->Item->typecast == AddThis->typecast)
        {
           if((*Hold)->Stacked < (*Hold)->StackSize)
           {
              (*Hold)->IncrementStack(incStack);
              return;
           }
        }
        Hold++;
    }
    theInventory.push_back(new InvItem(AddThis,stacksize,++lastSlot));
    thisManyItems++;
    (theInventory.back())->IncrementStack(incStack);

}


std::string Inventory::PrintInv(int Spot)
{
    std::string outString = "";
    if(theInventory.empty())
        return outString;

    std::list<InvItem*>::iterator Hold = theInventory.begin();

    while(Hold != theInventory.end())
    {
        if((*Hold)&&(*Hold)!=(InvItem*)0xFEEEFEEE  && (*Hold)->slot == Spot)
        {
            outString = (*Hold)->Item->typecast;
            outString += ": ";
            std::ostringstream buff;
            buff << (int)(*Hold)->Stacked;
            outString += buff.str();
            return outString;
        }
        Hold++;
    }
    return outString;
}

Inventory::~Inventory()
{
    while(theInventory.size())
    {
        delete theInventory.back();
        theInventory.pop_back();
    }
    //dtor
}
