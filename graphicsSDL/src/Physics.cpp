#include "../include/Physics.h"

Physics::Physics()
{
    //ctor
    BBoffset = glm::vec2(0);
    posZ = 0;
    posX = rand()%600;
    posY = rand()%600;
    Direction = glm::vec3(0);
    Destination = glm::vec3(0);
    MaxSpeed = 50;
    oX = posX;
    oY = posY;
    lposX = posX;
    lposY = posY;

    moveC = rand()%360;
    Collided = Seperate;
    Collidable = false;
    BoundingRect = {posX-4,posY-4,8,8};
    collideResponse = 0;
    Ctimes=0;
    CurTime = SDL_GetTicks();
}

void Physics::Update(GameObject * Parent)
{
    if(!Parent->CurrentQ)
        return;
    Collided = Seperate;
    lposX = posX; lposY = posY;

    BoundingRect.x = posX+BBoffset.x;
    BoundingRect.y = posY+BBoffset.y;

    GameObject * response = Collision(Parent);
   /* if(Collided != Seperate && Collided != Cursor)
    {
        posX = lposX;
        posY = lposY;
       // if(Parent->Pic)
         //   Parent->Pic->setPosition(posX,posY);
        BoundingRect.x = posX;
        BoundingRect.y = posY;
    }*/

    moveC++;

    moveC %= 360;

    /*if(Parent->typecast != "Player")
    {
        posX += MaxSpeed*cos(moveC);
        posY += MaxSpeed*sin(moveC);
    }*/

    Parent->CurrentQ->UpdateItem(Parent);
    Ctimes += Collided;

    //moveTween(Parent);
}


GameObject * Physics::Collision(GameObject * Parent)
{

    Quadrant * HoldQ = Parent->CurrentQ;
    //HoldQ = HoldQ->FindQuadByPos(HoldQ->Area.x-1,HoldQ->Area.y-1);
    std::list<GameObject*>::iterator Hold = HoldQ->items.begin();
    Collided = Seperate;
    for(int x = Parent->CurrentQ->Area.x-Parent->CurrentQ->Area.w+1; x < Parent->CurrentQ->Area.x+Parent->CurrentQ->Area.w*2; x+=Parent->CurrentQ->Area.w)
    {
        for(int y = Parent->CurrentQ->Area.y-Parent->CurrentQ->Area.h+1; y < Parent->CurrentQ->Area.y+Parent->CurrentQ->Area.h*2; y+=Parent->CurrentQ->Area.h)
        {
            if(HoldQ != HoldQ->FindQuadByPos(x,y))
            {
                HoldQ = HoldQ->FindQuadByPos(x,y);

                Hold = HoldQ->items.begin();
                while(Hold != HoldQ ->items.end() && *Hold != (GameObject *)0xFEEEFEEE)
                {
                    if((*Hold)->phys->Collidable)
                    if((*Hold) != Parent)
                    {
                        if ( !((BoundingRect.y + BoundingRect.h < (*Hold)->phys->BoundingRect.y)||
                               (BoundingRect.y > (*Hold)->phys->BoundingRect.y + (*Hold)->phys->BoundingRect.h)||
                               (BoundingRect.x > (*Hold)->phys->BoundingRect.x+(*Hold)->phys->BoundingRect.w)||
                               (BoundingRect.x + BoundingRect.w < (*Hold)->phys->BoundingRect.x) ))
                        {
                            if((*Hold)->typecast=="cursor")
                            {
                                Collided = Cursor;
                                if(Parent->OnMouseOver != NULL)
                                {
                                    Parent->OnMouseOver(Parent);
                                }
                                return *Hold;
                            }
                            else
                            {
                                Collided = InterSects;

                                if(Parent->OnCollide != NULL)
                                {
                                    Parent->OnCollide(Parent,(*Hold),Collided);
                                }
                                return *Hold;

                            }

                        }

                       /* if(Collided != Seperate)
                        {
                            return *Hold;
                        }*/

                    }
                    Hold++;
                }

            }

        }
    }
    return NULL;
}

bool Physics::TeleportTo(float x, float y, GameObject * Parent)
{
    bool success = true;
    lposX = posX;
    lposY = posY;
    posX += (x-posX);//%MaxSpeed;
    posY += (y-posY);//%MaxSpeed;
    //Parent->Pic->setPosition(posX,posY);
    BoundingRect.x = posX+BBoffset.x;
    BoundingRect.y = posY+BBoffset.y;
    GameObject * response = Collision(Parent);
    if(Collided != Seperate && Collided != Cursor && response && response->phys->collideResponse==0)
    {
        posX = lposX;
        posY = lposY;
       // Parent->Pic->setPosition(posX,posY);
        BoundingRect.x = posX+BBoffset.x;
        BoundingRect.y = posY+BBoffset.y;
        success = false;
    }
    Parent->CurrentQ->UpdateItem(Parent);
    return success;
}

void Physics::HomeIn(float x, float y, GameObject * Parent)
{
    lposX = posX;
    lposY = posY;
    float delta = SDL_GetTicks()-CurTime;
    delta/=1000;
    x = (x-posX);
    y = (y-posY);
    float c = sqrt(x*x+y*y);
    x/=c;
    y/=c;
    posX += (x*MaxSpeed*delta);
    posY += (y*MaxSpeed*delta);
    CurTime = SDL_GetTicks();

//    Parent->Pic->setPosition(posX,posY);
    BoundingRect.x = posX+BBoffset.x;
    BoundingRect.y = posY+BBoffset.y;
    GameObject * response = Collision(Parent);
    if(Collided != Seperate && Collided != Cursor)
    {
        posX = lposX;
        posY = lposY;
        if(response)
        {
            if(response->phys->collideResponse == 1){
                posX += (x*delta)/3;
                posY += (y*delta)/3;
            }
            else if(response->phys->collideResponse == 7)
            {
                posX -= (x*delta)/2;
                posY -= (y*delta)/2;
            }
        }
       // Parent->Pic->setPosition(posX,posY);
        BoundingRect.x = posX+BBoffset.x;
        BoundingRect.y = posY+BBoffset.y;
    }
    Parent->CurrentQ->UpdateItem(Parent);
}

bool Physics::moveTween(GameObject * Parent)
{
    bool success=true;
    lposX = posX;
    lposY = posY;
    double delta = SDL_GetTicks()-CurTime;
    delta/=1000;

    posX += (Direction.x*MaxSpeed*delta);
    posY += (Direction.y*MaxSpeed*delta);
    CurTime = SDL_GetTicks();

//    Parent->Pic->setPosition(posX,posY);
    BoundingRect.x = posX+BBoffset.x;
    BoundingRect.y = posY+BBoffset.y;
    GameObject * response = Collision(Parent);
    if(Collided != Seperate && Collided != Cursor)
    {
        posX = lposX;
        posY = lposY;
        if(response){
            if(response->phys->collideResponse == 1){
                posX += (Direction.x*MaxSpeed*delta)/3;
                posY += (Direction.y*MaxSpeed*delta)/3;
            }
            else if(response->phys->collideResponse == 7)
            {
                posX -= (Direction.x*MaxSpeed*delta)/2;
                posY -= (Direction.y*MaxSpeed*delta)/2;
            }
        }
       // Parent->Pic->setPosition(posX,posY);
        BoundingRect.x = posX+BBoffset.x;
        BoundingRect.y = posY+BBoffset.y;
        success = false;
    }
    Parent->CurrentQ->UpdateItem(Parent);
    return success;
}

Physics::~Physics()
{
    //dtor
}
