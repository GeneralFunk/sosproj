#include "../include/Primitives.h"

std::vector<Primitives::UniqueTC> Primitives::Uniq;

Primitives::Primitives()
{
    //ctor
}

void Primitives::makeVAO(GLuint * VBO, GLuint * VAO,
                          unsigned int indicSize, unsigned int structSize, GLuint program ){

    glGenVertexArrays(1,VAO);
    glBindVertexArray(*VAO);

    glGenBuffers(1, VBO);
	glBindBuffer(GL_ARRAY_BUFFER, *VBO);
	glBufferData(GL_ARRAY_BUFFER, structSize, vnctStruct, GL_STATIC_DRAW);

	GLuint vPosition = glGetAttribLocation(program, "vPosition");
	glEnableVertexAttribArray(vPosition);
	glVertexAttribPointer(vPosition, 4, GL_FLOAT, GL_FALSE, sizeof(VNCT), (GLvoid*)(0));

	GLuint vNormal = glGetAttribLocation(program, "vNormal");
	glEnableVertexAttribArray(vNormal);
	glVertexAttribPointer(vNormal, 4, GL_FLOAT, GL_FALSE, sizeof(VNCT), (GLvoid*)(sizeof(glm::vec4)));

	GLuint vColor = glGetAttribLocation(program, "vColor");
	glEnableVertexAttribArray(vColor);
	glVertexAttribPointer(vColor, 4, GL_FLOAT, GL_FALSE, sizeof(VNCT), (GLvoid*)(2 * sizeof(glm::vec4)));

    GLuint vTexCoords = glGetAttribLocation(program, "vTexCoords");
	glEnableVertexAttribArray(vTexCoords);
	glVertexAttribPointer(vTexCoords, 4, GL_FLOAT, GL_FALSE, sizeof(VNCT), (GLvoid*)(3 * sizeof(glm::vec4)));

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(*VAO);
	delete[]vnctStruct;
	delete[]indices;

}

void Primitives::makeVBO(GLuint * VBO, GLuint * IBO,
                          unsigned int indicSize, unsigned int structSize, GLuint program ){

    glGenBuffers(1, VBO);
	glBindBuffer(GL_ARRAY_BUFFER, *VBO);
	glBufferData(GL_ARRAY_BUFFER, structSize, vnctStruct, GL_STATIC_DRAW);
	glGenBuffers(1, IBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *IBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicSize, indices, GL_STATIC_DRAW);

	GLuint vPosition = glGetAttribLocation(program, "vPosition");
	glEnableVertexAttribArray(vPosition);
	glVertexAttribPointer(vPosition, 4, GL_FLOAT, GL_FALSE, sizeof(VNCT), (GLvoid*)(0));

	GLuint vNormal = glGetAttribLocation(program, "vNormal");
	glEnableVertexAttribArray(vNormal);
	glVertexAttribPointer(vNormal, 4, GL_FLOAT, GL_FALSE, sizeof(VNCT), (GLvoid*)(sizeof(glm::vec4)));

	GLuint vColor = glGetAttribLocation(program, "vColor");
	glEnableVertexAttribArray(vColor);
	glVertexAttribPointer(vColor, 4, GL_FLOAT, GL_FALSE, sizeof(VNCT), (GLvoid*)(2 * sizeof(glm::vec4)));

    GLuint vTexCoords = glGetAttribLocation(program, "vTexCoords");
	glEnableVertexAttribArray(vTexCoords);
	glVertexAttribPointer(vTexCoords, 4, GL_FLOAT, GL_FALSE, sizeof(VNCT), (GLvoid*)(3 * sizeof(glm::vec4)));

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	delete[]vnctStruct;
	delete[]indices;

}
int Primitives::loadCustom(v4v Vertices,v4v Normals,v4v Color,v4v TexCoords,std::vector<unsigned int> Indices,GLuint * VBO,GLuint * IBO, GLuint program){

    vnctStruct = new VNCT[Vertices.size()];
    indices = new unsigned int[Indices.size()];
    for(unsigned int i =0; i < Vertices.size();i++)
    {
        vnctStruct[i].vertex =Vertices[i];
        vnctStruct[i].normals =Normals[i];
        vnctStruct[i].color =Color[i];
        vnctStruct[i].texcoords =TexCoords[i];
    }
    for(unsigned int i =0; i < Indices.size();i++){
        indices[i] = Indices[i];
    }


    makeVBO(VBO,IBO,sizeof(unsigned int)*Indices.size(),sizeof(VNCT)*Vertices.size(),program);
    return Indices.size();
}


int Primitives::generateCube(glm::vec3 dimensions, glm::vec3 offset,
                              v4v Color,v4v TexCoords,
                              GLuint * VBO,GLuint * IBO, GLuint program){

    for(int i = 0; i < Primitives::Uniq.size();i++)
    {
        bool match=true;
        for(int ii = 0; ii < 4; ii++){
            if(Primitives::Uniq[i].UTC[ii] != TexCoords[ii])
                match=false;
        }
        if(match){
            *VBO = Primitives::Uniq[i].vbo;
            *IBO = Primitives::Uniq[i].vao;
            return Primitives::Uniq[i].indicSize;
        }

    }

    const int indicSize = 36;
    vnctStruct = new VNCT[indicSize];// CubeVertices*3];
    indices = new unsigned int[indicSize];

    glm::vec3 center = dimensions/glm::vec3(2);
    glm::vec4 ofst = glm::vec4(offset,0);
    //top
    vnctStruct[0].vertex=glm::vec4(-center.x,-center.y,center.z,1)+ofst;
    vnctStruct[1].vertex=glm::vec4(center.x,-center.y,center.z,1)+ofst;
    vnctStruct[2].vertex=glm::vec4(center.x,center.y,center.z,1)+ofst;
    vnctStruct[3].vertex=glm::vec4(-center.x,center.y,center.z,1)+ofst;

    //right
    vnctStruct[4].vertex=glm::vec4(center.x,center.y,center.z,1)+ofst;
    vnctStruct[5].vertex=glm::vec4(center.x,-center.y,center.z,1)+ofst;
    vnctStruct[6].vertex=glm::vec4(center.x,-center.y,-center.z,1)+ofst;
    vnctStruct[7].vertex=glm::vec4(center.x,center.y,-center.z,1)+ofst;

    //front
    vnctStruct[8].vertex=glm::vec4(-center.x,center.y,center.z,1)+ofst;
    vnctStruct[9].vertex=glm::vec4(center.x,center.y,center.z,1)+ofst;
    vnctStruct[10].vertex=glm::vec4(center.x,center.y,-center.z,1)+ofst;
    vnctStruct[11].vertex=glm::vec4(-center.x,center.y,-center.z,1)+ofst;

    //left
    vnctStruct[12].vertex=glm::vec4(-center.x,-center.y,center.z,1)+ofst;
    vnctStruct[13].vertex=glm::vec4(-center.x,center.y,center.z,1)+ofst;
    vnctStruct[14].vertex=glm::vec4(-center.x,center.y,-center.z,1)+ofst;
    vnctStruct[15].vertex=glm::vec4(-center.x,-center.y,-center.z,1)+ofst;

    //back
    vnctStruct[16].vertex=glm::vec4(center.x,-center.y,center.z,1)+ofst;
    vnctStruct[17].vertex=glm::vec4(-center.x,-center.y,center.z,1)+ofst;
    vnctStruct[18].vertex=glm::vec4(-center.x,-center.y,-center.z,1)+ofst;
    vnctStruct[19].vertex=glm::vec4(center.x,-center.y,-center.z,1)+ofst;

    //bottom
    vnctStruct[20].vertex=glm::vec4(-center.x,center.y,-center.z,1)+ofst;
    vnctStruct[21].vertex=glm::vec4(center.x,center.y,-center.z,1)+ofst;
    vnctStruct[22].vertex=glm::vec4(center.x,-center.y,-center.z,1)+ofst;
    vnctStruct[23].vertex=glm::vec4(-center.x,-center.y,-center.z,1)+ofst;

    //dupes
    vnctStruct[24].vertex=glm::vec4(-center.x,-center.y,center.z,1)+ofst;
    vnctStruct[25].vertex=glm::vec4(center.x,center.y,center.z,1)+ofst;
    vnctStruct[26].vertex=glm::vec4(center.x,center.y,center.z,1)+ofst;
    vnctStruct[27].vertex=glm::vec4(center.x,-center.y,-center.z,1)+ofst;
    vnctStruct[28].vertex=glm::vec4(-center.x,center.y,center.z,1)+ofst;
    vnctStruct[29].vertex=glm::vec4(center.x,center.y,-center.z,1)+ofst;
    vnctStruct[30].vertex=glm::vec4(-center.x,-center.y,center.z,1)+ofst;
    vnctStruct[31].vertex=glm::vec4(-center.x,center.y,-center.z,1)+ofst;
    vnctStruct[32].vertex=glm::vec4(center.x,-center.y,center.z,1)+ofst;
    vnctStruct[33].vertex=glm::vec4(-center.x,-center.y,-center.z,1)+ofst;
    vnctStruct[34].vertex=glm::vec4(-center.x,center.y,-center.z,1)+ofst;
    vnctStruct[35].vertex=glm::vec4(center.x,-center.y,-center.z,1)+ofst;


    //t-0 r-1 f-2 l-3 ba-4 bo-5
    glm::vec4 nrm[6];
    glm::vec4 Left = (vnctStruct[3].vertex-vnctStruct[0].vertex);
    glm::vec4 Right = (vnctStruct[1].vertex-vnctStruct[0].vertex);
    nrm[0] = glm::vec4(glm::cross(glm::vec3(Left.x,Left.y,Left.z),glm::vec3(Right.x,Right.y,Right.z)),0);
    Left = vnctStruct[22].vertex-vnctStruct[2].vertex;
    Right = vnctStruct[1].vertex-vnctStruct[2].vertex;
    nrm[1] = glm::vec4(glm::cross(glm::vec3(Left.x,Left.y,Left.z),glm::vec3(Right.x,Right.y,Right.z)),0);
    Left = vnctStruct[23].vertex-vnctStruct[3].vertex;
    Right = vnctStruct[2].vertex-vnctStruct[3].vertex;
    nrm[2] = glm::vec4(glm::cross(glm::vec3(Left.x,Left.y,Left.z),glm::vec3(Right.x,Right.y,Right.z)),0);
    Left = vnctStruct[20].vertex-vnctStruct[0].vertex;
    Right = vnctStruct[3].vertex-vnctStruct[0].vertex;
    nrm[3] = glm::vec4(glm::cross(glm::vec3(Left.x,Left.y,Left.z),glm::vec3(Right.x,Right.y,Right.z)),0);
    Left = vnctStruct[1].vertex-vnctStruct[0].vertex;
    Right = vnctStruct[20].vertex-vnctStruct[0].vertex;
    nrm[4] = glm::vec4(glm::cross(glm::vec3(Left.x,Left.y,Left.z),glm::vec3(Right.x,Right.y,Right.z)),0);
    Left = vnctStruct[21].vertex-vnctStruct[20].vertex;
    Right = vnctStruct[23].vertex-vnctStruct[20].vertex;
    nrm[5] = glm::vec4(glm::cross(glm::vec3(Left.x,Left.y,Left.z),glm::vec3(Right.x,Right.y,Right.z)),0);

////t-0 bo-5 r-1 f-2 l-3 ba-4
    vnctStruct[0].normals = glm::normalize(nrm[0]+nrm[4]+nrm[3]);
    vnctStruct[1].normals = glm::normalize(nrm[0]+nrm[4]+nrm[1]);
    vnctStruct[2].normals = glm::normalize(nrm[0]+nrm[2]+nrm[1]);
    vnctStruct[3].normals = glm::normalize(nrm[0]+nrm[2]+nrm[3]);

    vnctStruct[20].normals = glm::normalize(nrm[5]+nrm[4]+nrm[3]);
    vnctStruct[21].normals = glm::normalize(nrm[5]+nrm[4]+nrm[1]);
    vnctStruct[22].normals = glm::normalize(nrm[5]+nrm[2]+nrm[1]);
    vnctStruct[23].normals = glm::normalize(nrm[5]+nrm[2]+nrm[3]);

    vnctStruct[4].normals = vnctStruct[21].normals;
    vnctStruct[5].normals = vnctStruct[1].normals;
    vnctStruct[6].normals = vnctStruct[2].normals;
    vnctStruct[7].normals = vnctStruct[23].normals;

    vnctStruct[8].normals = vnctStruct[23].normals;
    vnctStruct[9].normals = vnctStruct[22].normals;
    vnctStruct[10].normals = vnctStruct[2].normals;
    vnctStruct[11].normals = vnctStruct[3].normals;

    vnctStruct[12].normals = vnctStruct[20].normals;
    vnctStruct[13].normals = vnctStruct[23].normals;
    vnctStruct[14].normals = vnctStruct[3].normals;
    vnctStruct[15].normals = vnctStruct[0].normals;

    vnctStruct[16].normals = vnctStruct[21].normals;
    vnctStruct[17].normals = vnctStruct[1].normals;
    vnctStruct[18].normals = vnctStruct[0].normals;
    vnctStruct[19].normals = vnctStruct[20].normals;

    //dupes
    vnctStruct[24].normals = glm::normalize(nrm[0]+nrm[4]+nrm[3]);
    vnctStruct[25].normals = glm::normalize(nrm[0]+nrm[2]+nrm[1]);
    vnctStruct[26].normals = vnctStruct[21].normals;
    vnctStruct[27].normals = vnctStruct[2].normals;
    vnctStruct[28].normals = vnctStruct[23].normals;
    vnctStruct[29].normals = vnctStruct[2].normals;
    vnctStruct[30].normals = vnctStruct[20].normals;
    vnctStruct[31].normals = vnctStruct[3].normals;
    vnctStruct[32].normals = vnctStruct[21].normals;
    vnctStruct[33].normals = vnctStruct[0].normals;
    vnctStruct[34].normals = glm::normalize(nrm[5]+nrm[4]+nrm[3]);
    vnctStruct[35].normals = glm::normalize(nrm[5]+nrm[2]+nrm[1]);


    for(int i = 0; i < indicSize; i++)
    {
        vnctStruct[i].color = Color[i];
        vnctStruct[i].texcoords = TexCoords[i];
    }


    //top
    indices[0]=0;   indices[1]=1;    indices[2]=2;
    indices[3]=24;   indices[4]=3;    indices[5]=25;

    //right
    indices[6]=4;   indices[7]=5;    indices[8]=6;
    indices[9]=26;   indices[10]=7;    indices[11]=27;

    //front
    indices[12]=8;   indices[13]=9;    indices[14]=10;
    indices[15]=28;   indices[16]=11;    indices[17]=29;

    //left
    indices[18]=12;   indices[19]=13;    indices[20]=14;
    indices[21]=30;   indices[22]=15;    indices[23]=31;

    //back
    indices[24]=16;   indices[25]=17;    indices[26]=18;
    indices[27]=32;   indices[28]=19;    indices[29]=33;

    //bottom
    indices[30]=20;   indices[31]=21;    indices[32]=22;
    indices[33]=34;   indices[34]=23;    indices[35]=35;

    std::vector<VNCT> hold = std::vector<VNCT>(36);
    for(int i=0;i<36;i++)
        hold[i]=vnctStruct[indices[i]];
    for(int i=0;i<36;i++)
        vnctStruct[i]=hold[i];

    makeVAO(VBO,IBO,sizeof(unsigned int)*indicSize,sizeof(VNCT)*indicSize,program);

    UniqueTC HoldUTC;
    for(int ii = 0; ii < 4; ii++){
        HoldUTC.UTC[ii] = TexCoords[ii];
    }
    HoldUTC.vbo = *VBO;
    HoldUTC.vao = *IBO;
    HoldUTC.indicSize = indicSize;
    Primitives::Uniq.push_back(HoldUTC);


    return indicSize;
}

Primitives::~Primitives()
{
    //dtor
}
