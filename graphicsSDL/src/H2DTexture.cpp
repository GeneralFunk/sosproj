#include "../include/H2DTexture.h"

GLuint H2DTexture::BoundTexture;
GLuint H2DTexture::Prog;
glm::vec4 H2DTexture::PlayerPos;
std::vector<GLuint> H2DTexture::VaosToDeletes;
std::vector<GLuint> H2DTexture::VbosToDeletes;

/*GLuint H2DTexture::vPosition;
GLuint H2DTexture::vColor;
GLuint H2DTexture::vTexCoords;*/

H2DTexture::H2DTexture()
{
    cubeit = false;
    AnimateState = 1;
    Offsetx = 0; Offsety=0;
   // mTexture = NULL;
	mWidth = 0;
	mHeight = 0;
	mFrame = NULL;
	mDisplay = NULL;
	FrameCount = 0;
	MaxFrames = 1;
	FramesPerSecond = 1;
    masterTexture = false;
    CurTime = SDL_GetTicks();
    EffectTimeEnd = 0;
//    Children = ListCode<H2DTexture*>();
    depth = 0;
    isDead = true;
    renderType = 0;

/*
    Vertices = new glm::vec4[12];
    Colors = new glm::vec4[12];
    TexCoords = new glm::vec4[12];

//top
    Vertices[0] = glm::vec4(0,0,0,1);
    Vertices[1] = glm::vec4(1,0,0,1);
    Vertices[2] = glm::vec4(1,-1,0,1);
    Vertices[3] = glm::vec4(0,-1,0,1);
//right
    Vertices[4] = glm::vec4(1,0,0,1);
    Vertices[5] = glm::vec4(1,0,-1,1);
    Vertices[6] = glm::vec4(1,-1,-1,1);
    Vertices[7] = glm::vec4(1,-1,0,1);
//front
    Vertices[8] = glm::vec4(0,-1,0,1);
    Vertices[9] = glm::vec4(1,-1,0,1);
    Vertices[10] = glm::vec4(1,-1,-1,1);
    Vertices[11] = glm::vec4(0,-1,-1,1);
/* ///Left
    Vertices[12] = glm::vec4(0,0,0,1);
    Vertices[13] = glm::vec4(0,-1,0,1);
    Vertices[14] = glm::vec4(0,-1,-1,1);
    Vertices[15] = glm::vec4(0,0,-1,1);
//Back
    Vertices[16] = glm::vec4(1,0,0,1);
    Vertices[17] = glm::vec4(0,0,0,1);
    Vertices[18] = glm::vec4(0,0,-1,1);
    Vertices[19] = glm::vec4(1,0,-1,1);
//bottom
    Vertices[21] = glm::vec4(0,0,-1,1);
    Vertices[22] = glm::vec4(0,1,-1,1);
    Vertices[23] = glm::vec4(1,-1,-1,1);
    Vertices[24] = glm::vec4(0,-1,-1,1);
**
//top
    Colors[0] = glm::vec4(0,0,1,1);
    Colors[1] = glm::vec4(0,1,0,1);
    Colors[2] = glm::vec4(0,0,1,1);
    Colors[3] = glm::vec4(1,1,1,1);
//right
    Colors[4] = glm::vec4(0,0,1,1);
    Colors[5] = glm::vec4(0,1,0,1);
    Colors[6] = glm::vec4(0,0,1,1);
    Colors[7] = glm::vec4(1,1,1,1);
//front
    Colors[8] = glm::vec4(0,0,1,1);
    Colors[9] = glm::vec4(0,1,0,1);
    Colors[10] = glm::vec4(0,0,1,1);
    Colors[11] = glm::vec4(1,1,1,1);

//top
    TexCoords[0] = glm::vec4(0,0,0,1);
    TexCoords[1] = glm::vec4(1,0,0,1);
    TexCoords[2] = glm::vec4(1,1,0,1);
    TexCoords[3] = glm::vec4(0,1,0,1);
//right
    TexCoords[4] = glm::vec4(0,0,0,1);
    TexCoords[5] = glm::vec4(0,1,0,1);
    TexCoords[6] = glm::vec4(1,1,0,1);
    TexCoords[7] = glm::vec4(0,1,0,1);
//front
    TexCoords[8] = glm::vec4(0,0,0,1);
    TexCoords[9] = glm::vec4(0,1,0,1);
    TexCoords[10] = glm::vec4(1,1,0,1);
    TexCoords[11] = glm::vec4(0,1,0,1);
tvao = 0;*/
tvbo =0;
tibo = 0;
transformer = glm::mat4(1.0f);
}

H2DTexture::H2DTexture(const H2DTexture & ToCopy)
{
    transformer = ToCopy.transformer;
    EffectTimeEnd = 0;
    AnimateState = ToCopy.AnimateState;
    texture = ToCopy.texture;
    Offsetx = ToCopy.Offsetx; Offsety=ToCopy.Offsety;
    masterTexture = false;
    gRenderer = ToCopy.gRenderer;
 //   mTexture = ToCopy.mTexture;
	mWidth = ToCopy.mWidth;
	mHeight = ToCopy.mHeight;
mFrame=NULL;
mDisplay=NULL;
if(ToCopy.mFrame){
    mFrame = new SDL_Rect();
	mFrame->h = ToCopy.mFrame->h;
	mFrame->w = ToCopy.mFrame->w;
	mFrame->x = ToCopy.mFrame->x;
	mFrame->y = ToCopy.mFrame->y;
}

if(ToCopy.mDisplay){
    mDisplay = new SDL_Rect();
	mDisplay->h = ToCopy.mDisplay->h;
	mDisplay->w = ToCopy.mDisplay->w;
	mDisplay->x = ToCopy.mDisplay->x;
	mDisplay->y = ToCopy.mDisplay->y;
}

	FrameCount = ToCopy.FrameCount;
	MaxFrames = ToCopy.MaxFrames;
	FramesPerSecond = ToCopy.FramesPerSecond;

    depth = ToCopy.depth;
    CurTime = ToCopy.CurTime;
    frameStartX = ToCopy.frameStartX;
    if(frameStartX < 0)
        frameStartX = 0;
    isDead = false;
    renderType = ToCopy.renderType;

    StopAnim(false);

 /*   Vertices = new glm::vec4[12];
    Colors = new glm::vec4[12];
    TexCoords = new glm::vec4[12];

    for(int i =0; i < 12; i++){
        Vertices[i] = ToCopy.Vertices[i];
        Colors[i] = ToCopy.Colors[i];
        TexCoords[i] = ToCopy.TexCoords[i];
    }
tvao = 0;*/
tvbo = 0;
tibo = 0;
}

H2DTexture H2DTexture::operator=(const H2DTexture & ToCopy){
    transformer = ToCopy.transformer;
    EffectTimeEnd = 0;
    AnimateState = ToCopy.AnimateState;
    texture = ToCopy.texture;
    Offsetx = ToCopy.Offsetx; Offsety=ToCopy.Offsety;
    masterTexture = false;
    gRenderer = ToCopy.gRenderer;
 //   mTexture = ToCopy.mTexture;
	mWidth = ToCopy.mWidth;
	mHeight = ToCopy.mHeight;

    if(ToCopy.mFrame){
        if(!mFrame)
            mFrame = new SDL_Rect();
        mFrame->h = ToCopy.mFrame->h;
        mFrame->w = ToCopy.mFrame->w;
        mFrame->x = ToCopy.mFrame->x;
        mFrame->y = ToCopy.mFrame->y;
    }
    else
        mFrame = NULL;

    if(mDisplay){
        if(!mDisplay)
            mDisplay = new SDL_Rect();
        mDisplay->h = ToCopy.mDisplay->h;
        mDisplay->w = ToCopy.mDisplay->w;
        mDisplay->x = ToCopy.mDisplay->x;
        mDisplay->y = ToCopy.mDisplay->y;
    }
    else{
        mDisplay=NULL;
    }
	FrameCount = ToCopy.FrameCount;
	MaxFrames = ToCopy.MaxFrames;
	FramesPerSecond = ToCopy.FramesPerSecond;

    depth = ToCopy.depth;
    CurTime = ToCopy.CurTime;
    frameStartX = ToCopy.frameStartX;
    if(frameStartX < 0)
        frameStartX = 0;
    isDead = false;
    renderType = ToCopy.renderType;

    StopAnim(false);
/*    Vertices = new glm::vec4[12];
    Colors = new glm::vec4[12];
    TexCoords = new glm::vec4[12];

    Vertices[0] = ToCopy.Vertices[0];
    Vertices[1] = ToCopy.Vertices[1];
    Vertices[2] = ToCopy.Vertices[2];
    Vertices[3] = ToCopy.Vertices[3];
    Vertices[4] = ToCopy.Vertices[4];
    Vertices[5] = ToCopy.Vertices[5];
    Vertices[6] = ToCopy.Vertices[6];
    Vertices[7] = ToCopy.Vertices[7];
    Vertices[8] = ToCopy.Vertices[8];
    Vertices[9] = ToCopy.Vertices[9];
    Vertices[10] = ToCopy.Vertices[10];
    Vertices[11] = ToCopy.Vertices[11];

    Colors[0] = ToCopy.Colors[0];
    Colors[1] = ToCopy.Colors[1];
    Colors[2] = ToCopy.Colors[2];
    Colors[3] = ToCopy.Colors[3];
    Colors[4] = ToCopy.Colors[0];
    Colors[5] = ToCopy.Colors[1];
    Colors[6] = ToCopy.Colors[2];
    Colors[7] = ToCopy.Colors[3];
    Colors[8] = ToCopy.Colors[0];
    Colors[9] = ToCopy.Colors[1];
    Colors[10] = ToCopy.Colors[2];
    Colors[11] = ToCopy.Colors[3];

    TexCoords[0] = ToCopy.TexCoords[0];
    TexCoords[1] = ToCopy.TexCoords[1];
    TexCoords[2] = ToCopy.TexCoords[2];
    TexCoords[3] = ToCopy.TexCoords[3];
    TexCoords[4] = ToCopy.TexCoords[0];
    TexCoords[5] = ToCopy.TexCoords[1];
    TexCoords[6] = ToCopy.TexCoords[2];
    TexCoords[7] = ToCopy.TexCoords[3];
    TexCoords[8] = ToCopy.TexCoords[0];
    TexCoords[9] = ToCopy.TexCoords[1];
    TexCoords[10] = ToCopy.TexCoords[2];
    TexCoords[11] = ToCopy.TexCoords[3];
    tvao = 0;*/
    tvbo=0;
    tibo=0;
    return *this;
}

H2DTexture::~H2DTexture()
{

    if(masterTexture)
    {
        glDeleteTextures(1,&texture);
    }
    //free();
    gRenderer=NULL;

    delete mFrame;
    delete mDisplay;


   /* if(tvao)
        glDeleteVertexArrays(1,&tvao);
    if(tbuffer)
        glDeleteBuffers(1,&tbuffer);*/
   /* if(tibo)
        glDeleteVertexArrays(1,&tibo);
    if(tvbo)
        glDeleteBuffers(1,&tvbo);*/
   /* if(tibo)
        VaosToDeletes.push_back(tibo);
    if(tvbo)
        VbosToDeletes.push_back(tvbo);*/
    //if(sampler)
      //  glDeleteSamplers(1,&sampler);
    //tvao=0;
    tibo = 0;
    tvbo = 0;
    mFrame = NULL;
    mDisplay = NULL;
/*    delete[] Vertices;
    delete[] Colors;
    delete[] TexCoords;
    Vertices=NULL;
    Colors=NULL;
    TexCoords=NULL;*/

}

void H2DTexture::init(SDL_Renderer* theRend)
{
    int imgFlags = IMG_INIT_PNG;
    if( !( IMG_Init( imgFlags ) & imgFlags ) )
    {
        printf( "SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError() );

    }
    gRenderer = theRend;
}

bool H2DTexture::loadFromFile( std::string path )
{

    SDL_Surface *tex = IMG_Load(path.c_str());


    if(tex)
    {
        glGenTextures(1, &texture);
        glBindTexture(GL_TEXTURE_2D, texture);
        glTexImage2D(GL_TEXTURE_2D, 0, tex->format->BytesPerPixel, tex->w, tex->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, tex->pixels);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        printf("OK\n");
        SDL_FreeSurface(tex);
    }
    else
    {
        printf("Failed\nQuitting...");

    }

    //Get image dimensions
    mWidth = tex->w;
    mHeight = tex->h;
    if(mFrame==NULL)
    {
        mFrame = new SDL_Rect();
    }
        mFrame->x = 0;mFrame->y=0;
        mFrame->h = mHeight;
        mFrame->w = mWidth;

    if(mDisplay==NULL)
    {
        mDisplay = new SDL_Rect();
    }
    mDisplay->x = 0;mDisplay->y=0;
    mDisplay->h = mHeight;
    mDisplay->w = mWidth;
//Vertices[1]= glm::vec4(mWidth,0,0,1);
//Vertices[2]= glm::vec4(mWidth,mHeight,0,1);
//Vertices[3]= glm::vec4(0,mHeight,0,1);
    H2DTexture::BoundTexture = texture;
    isDead = false;
	return texture;
}

/*void H2DTexture::KillChildren()
{
    if(Children.Size > 0)
    {
        NodeCode<H2DTexture*> * Hold = Children.First;

        while(Hold)
        {
            if(Hold->Next != NULL)
            {
                Hold = Hold->Next;
                delete Hold->Prev;
                Hold->Prev = 0;
            }
            else
            {
                delete Hold;
                Hold = NULL;
                Children.First = 0;
                Children.Last = 0;
                Children.Size = 0;
            }
        }
    }
}*/

void H2DTexture::free()
{
	//Free texture if it exists

	/*if( mTexture != NULL )
	{
		SDL_DestroyTexture( mTexture );
		mTexture = NULL;
		mWidth = 0;
		mHeight = 0;
	}*/
}


void H2DTexture::setPosition(int x, int y)
{
    posX = x; posY = y;
    mDisplay->x = x;
    mDisplay->y = y;
}

void H2DTexture::setDisplay(SDL_Rect DisplayArea)
{
    *mDisplay = DisplayArea;
}


void H2DTexture::modAbsScale(float Scale)
{
    if(Scale<0)
        Scale=-Scale;
    mDisplay->h = Scale*mFrame->h;
    mDisplay->w = Scale*mFrame->w;
}

void H2DTexture::modSpScale(double Scale,bool reverseDir)
{
    if(reverseDir)
    {
        mDisplay->h = 3*sin(Scale)*mFrame->h;
        mDisplay->w = 3*cos(Scale)*mFrame->w;
    }
    else
    {
        mDisplay->h = 3*cos(Scale)*mFrame->h;
        mDisplay->w = 3*sin(Scale)*mFrame->w;
    }
}

void H2DTexture::modRelScale(float Scale)
{
    if(Scale==0)
        return;
    mDisplay->h *= Scale;
    mDisplay->w *= Scale;
}

void H2DTexture::setDepth(float D)
{
    depth = D;
}

void H2DTexture::setupAnimation(SDL_Rect initialFrame,float FPS,int MaxF)
{
    *mFrame = initialFrame;
    frameStartX= mFrame->x;
    FramesPerSecond = FPS;
    MaxFrames = MaxF;
    StopAnim(false);
    AnimateState=1;
}

void H2DTexture::BoingEffect()
{
    float sash = (SDL_GetTicks()-EffectTimeStart);
    sash = 1+0.1*sin(sash/100);
    //modAbsScale(sash);
    transformer = glm::scale(transformer,glm::vec3(sash,sash,1));
    //mDisplay->w = mFrame->w*((float)((SDL_GetTicks()-EffectTimeStart)%1000)/1000);
    //EffectTimeStart = SDL_GetTicks();
    if(SDL_GetTicks()-EffectTimeStart >= EffectTimeEnd-EffectTimeStart)
    {
        EffectTimeEnd = 0;
       // modAbsScale(1);
       transformer = glm::mat4(1.0f);
    }

}

void H2DTexture::setEffect(int Length)
{
    EffectTimeStart = SDL_GetTicks();
    EffectTimeEnd = EffectTimeStart + Length;
}

void H2DTexture::UpdateAnimate()
{
    if(EffectTimeEnd > 0)
    {
        BoingEffect();
    }
    if(MaxFrames <= 1)
        return;
    if(stop)
    {
        mFrame->x= frameStartX;
    }
    else if(pause <=0)
    {
        int FPS = (1000/FramesPerSecond);
        FrameCount = (int)(((SDL_GetTicks()-CurTime)*AnimateState)/FPS)%MaxFrames;
        if(FrameOffset > 0)
        {
            FrameCount = (FrameOffset-FrameCount)%MaxFrames;
        }
        mFrame->x= frameStartX+mFrame->w*FrameCount;
    }
    //TexCoords[0] = glm::vec4((float)mFrame->x/(float)mWidth, (float)(mFrame->y)/(float)mHeight,0,0);
    //TexCoords[1] = glm::vec4((float)(mFrame->x+mFrame->w)/(float)mWidth, (float)(mFrame->y)/(float)mHeight,1,0);
    //TexCoords[2] = glm::vec4((float)(mFrame->x+mFrame->w)/(float)mWidth, (float)(mFrame->y+mFrame->h)/(float)mHeight,1,1);
    //TexCoords[3] = glm::vec4((float)mFrame->x/(float)mWidth, (float)(mFrame->y+mFrame->h)/(float)mHeight,0,1);

}

void H2DTexture::PauseAnim(bool TogPause)
{
    if(TogPause)
        pause = mFrame->x;
    else
    {
        FrameOffset = (pause-(int)(((SDL_GetTicks()-CurTime)*AnimateState)/(1000/FramesPerSecond))%MaxFrames);
        pause = -1;
    }

}

void H2DTexture::initArray(){
    if(tvbo || tibo)
        return;
    glUseProgram(0);
/*//top
    Vertices[0]= glm::vec4(0,0,depth,1);
    Vertices[1]= glm::vec4(mDisplay->w,0,depth,1);
    Vertices[2]= glm::vec4(mDisplay->w,mDisplay->h,depth,1);
    Vertices[3]= glm::vec4(0,mDisplay->h,depth,1);
//right
    Vertices[4]= glm::vec4(mDisplay->w,mDisplay->h,depth,1);
    Vertices[5]= glm::vec4(mDisplay->w,0,depth,1);
    Vertices[6]= glm::vec4(mDisplay->w,0,depth-mDisplay->h,1);
    Vertices[7]= glm::vec4(mDisplay->w,mDisplay->h,depth-mDisplay->h,1);
//front
    Vertices[8]= glm::vec4(0,mDisplay->h,depth,1);
    Vertices[9]= glm::vec4(mDisplay->w,mDisplay->h,depth,1);
    Vertices[10]= glm::vec4(mDisplay->w,mDisplay->h,depth-mDisplay->h,1);
    Vertices[11]= glm::vec4(0,mDisplay->h,depth-mDisplay->h,1);


    Colors[0] = glm::vec4(0,0,1,1);
    Colors[1] = glm::vec4(0,1,0,1);
    Colors[2] = glm::vec4(0,0,1,1);
    Colors[3] = glm::vec4(1,1,1,1);
    Colors[4] = glm::vec4(0,0,1,1);
    Colors[5] = glm::vec4(0,1,0,1);
    Colors[6] = glm::vec4(0,0,1,1);
    Colors[7] = glm::vec4(1,1,1,1);
    Colors[8] = glm::vec4(0,0,1,1);
    Colors[9] = glm::vec4(0,1,0,1);
    Colors[10] = glm::vec4(0,0,1,1);
    Colors[11] = glm::vec4(1,1,1,1);

    TexCoords[0] = glm::vec4((float)mFrame->x/(float)mWidth, (float)(mFrame->y)/(float)mHeight,0,0);
    TexCoords[1] = glm::vec4((float)(mFrame->x+mFrame->w)/(float)mWidth, (float)(mFrame->y)/(float)mHeight,1,0);
    TexCoords[2] = glm::vec4((float)(mFrame->x+mFrame->w)/(float)mWidth, (float)(mFrame->y+mFrame->h)/(float)mHeight,1,1);
    TexCoords[3] = glm::vec4((float)mFrame->x/(float)mWidth, (float)(mFrame->y+mFrame->h)/(float)mHeight,0,1);

    TexCoords[4] = glm::vec4((float)mFrame->x/(float)mWidth, (float)(mFrame->y)/(float)mHeight,0,0);
    TexCoords[5] = glm::vec4((float)(mFrame->x+mFrame->w)/(float)mWidth, (float)(mFrame->y)/(float)mHeight,1,0);
    TexCoords[6] = glm::vec4((float)(mFrame->x+mFrame->w)/(float)mWidth, (float)(mFrame->y+mFrame->h)/(float)mHeight,1,1);
    TexCoords[7] = glm::vec4((float)mFrame->x/(float)mWidth, (float)(mFrame->y+mFrame->h)/(float)mHeight,0,1);

    TexCoords[8] = glm::vec4((float)mFrame->x/(float)mWidth, (float)(mFrame->y)/(float)mHeight,0,0);
    TexCoords[9] = glm::vec4((float)(mFrame->x+mFrame->w)/(float)mWidth, (float)(mFrame->y)/(float)mHeight,1,0);
    TexCoords[10] = glm::vec4((float)(mFrame->x+mFrame->w)/(float)mWidth, (float)(mFrame->y+mFrame->h)/(float)mHeight,1,1);
    TexCoords[11] = glm::vec4((float)mFrame->x/(float)mWidth, (float)(mFrame->y+mFrame->h)/(float)mHeight,0,1);

	glGenVertexArrays(1, &tvao);
	glBindVertexArray(tvao);

	glGenBuffers(1, &tbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, tbuffer);
    csize = sizeof(*Vertices) * 12;
	glBufferData(GL_ARRAY_BUFFER, csize*3,
		NULL, GL_DYNAMIC_DRAW);

	glBufferSubData(GL_ARRAY_BUFFER, 0, csize, Vertices);
	glBufferSubData(GL_ARRAY_BUFFER, csize,csize, Colors);
	glBufferSubData(GL_ARRAY_BUFFER, csize*2,csize, TexCoords);*/
	if(!Prog)
    {
	//InitShader IS = InitShader();
        ShaderProgram * SP = new ShaderProgram("vf","vshader.glsl", "fshader.glsl");
        Prog = SP->program;// IS.doInitShader("..\\..\\vshader.glsl", "..\\..\\fshader.glsl");
    }
	glUseProgram(Prog);

	/*GLuint vPosition = glGetAttribLocation(Prog, "vPosition");
	glEnableVertexAttribArray(vPosition);
	glVertexAttribPointer(vPosition, 4, GL_FLOAT, GL_FALSE, 0,0);

	GLuint vColor = glGetAttribLocation(Prog, "vColor");
	glEnableVertexAttribArray(vColor);
	glVertexAttribPointer(vColor, 4, GL_FLOAT, GL_FALSE, 0,
		(GLvoid*)(csize));

    GLuint vTexCoords = glGetAttribLocation(Prog, "vTexCoords");
	glEnableVertexAttribArray(vTexCoords);
	glVertexAttribPointer(vTexCoords, 4, GL_FLOAT, GL_FALSE, 0,
		(GLvoid*)(csize*2));
*/

        std::vector<glm::vec4> Color = std::vector<glm::vec4>(36);
        std::vector<glm::vec4> TexCoords = std::vector<glm::vec4>(36);
        for(int i =0; i < 36; i++)
            Color[i]=glm::vec4(0,1,1,1);
        TexCoords[0] = glm::vec4((float)mFrame->x/(float)mWidth, (float)(mFrame->y)/(float)mHeight,0,0);
        TexCoords[1] = glm::vec4((float)(mFrame->x+mFrame->w)/(float)mWidth, (float)(mFrame->y)/(float)mHeight,1,0);
        TexCoords[2] = glm::vec4((float)(mFrame->x+mFrame->w)/(float)mWidth, (float)(mFrame->y+mFrame->h)/(float)mHeight,1,1);
        TexCoords[3] = glm::vec4((float)mFrame->x/(float)mWidth, (float)(mFrame->y+mFrame->h)/(float)mHeight,0,1);

        TexCoords[4] = glm::vec4((float)mFrame->x/(float)mWidth, (float)(mFrame->y)/(float)mHeight,0,0);
        TexCoords[5] = glm::vec4((float)(mFrame->x+mFrame->w)/(float)mWidth, (float)(mFrame->y)/(float)mHeight,1,0);
        TexCoords[6] = glm::vec4((float)(mFrame->x+mFrame->w)/(float)mWidth, (float)(mFrame->y+mFrame->h)/(float)mHeight,1,1);
        TexCoords[7] = glm::vec4((float)mFrame->x/(float)mWidth, (float)(mFrame->y+mFrame->h)/(float)mHeight,0,1);

        TexCoords[8] = glm::vec4((float)mFrame->x/(float)mWidth, (float)(mFrame->y)/(float)mHeight,0,0);
        TexCoords[9] = glm::vec4((float)(mFrame->x+mFrame->w)/(float)mWidth, (float)(mFrame->y)/(float)mHeight,1,0);
        TexCoords[10] = glm::vec4((float)(mFrame->x+mFrame->w)/(float)mWidth, (float)(mFrame->y+mFrame->h)/(float)mHeight,1,1);
        TexCoords[11] = glm::vec4((float)mFrame->x/(float)mWidth, (float)(mFrame->y+mFrame->h)/(float)mHeight,0,1);

        TexCoords[12] = glm::vec4((float)mFrame->x/(float)mWidth, (float)(mFrame->y)/(float)mHeight,0,0);
        TexCoords[13] = glm::vec4((float)(mFrame->x+mFrame->w)/(float)mWidth, (float)(mFrame->y)/(float)mHeight,1,0);
        TexCoords[14] = glm::vec4((float)(mFrame->x+mFrame->w)/(float)mWidth, (float)(mFrame->y+mFrame->h)/(float)mHeight,1,1);
        TexCoords[15] = glm::vec4((float)mFrame->x/(float)mWidth, (float)(mFrame->y+mFrame->h)/(float)mHeight,0,1);

        TexCoords[16] = glm::vec4((float)mFrame->x/(float)mWidth, (float)(mFrame->y)/(float)mHeight,0,0);
        TexCoords[17] = glm::vec4((float)(mFrame->x+mFrame->w)/(float)mWidth, (float)(mFrame->y)/(float)mHeight,1,0);
        TexCoords[18] = glm::vec4((float)(mFrame->x+mFrame->w)/(float)mWidth, (float)(mFrame->y+mFrame->h)/(float)mHeight,1,1);
        TexCoords[19] = glm::vec4((float)mFrame->x/(float)mWidth, (float)(mFrame->y+mFrame->h)/(float)mHeight,0,1);

        TexCoords[20] = glm::vec4((float)mFrame->x/(float)mWidth, (float)(mFrame->y)/(float)mHeight,0,0);
        TexCoords[21] = glm::vec4((float)(mFrame->x+mFrame->w)/(float)mWidth, (float)(mFrame->y)/(float)mHeight,1,0);
        TexCoords[22] = glm::vec4((float)(mFrame->x+mFrame->w)/(float)mWidth, (float)(mFrame->y+mFrame->h)/(float)mHeight,1,1);
        TexCoords[23] = glm::vec4((float)mFrame->x/(float)mWidth, (float)(mFrame->y+mFrame->h)/(float)mHeight,0,1);

        TexCoords[24] = glm::vec4((float)mFrame->x/(float)mWidth, (float)(mFrame->y)/(float)mHeight,0,0);
        TexCoords[25] = glm::vec4((float)(mFrame->x+mFrame->w)/(float)mWidth, (float)(mFrame->y+mFrame->h)/(float)mHeight,1,1);
        TexCoords[26] = glm::vec4((float)mFrame->x/(float)mWidth, (float)(mFrame->y)/(float)mHeight,0,0);
        TexCoords[27] = glm::vec4((float)(mFrame->x+mFrame->w)/(float)mWidth, (float)(mFrame->y+mFrame->h)/(float)mHeight,1,1);
        TexCoords[28] = glm::vec4((float)mFrame->x/(float)mWidth, (float)(mFrame->y)/(float)mHeight,0,0);
        TexCoords[29] = glm::vec4((float)(mFrame->x+mFrame->w)/(float)mWidth, (float)(mFrame->y+mFrame->h)/(float)mHeight,1,1);
        TexCoords[30] = glm::vec4((float)mFrame->x/(float)mWidth, (float)(mFrame->y)/(float)mHeight,0,0);
        TexCoords[31] = glm::vec4((float)(mFrame->x+mFrame->w)/(float)mWidth, (float)(mFrame->y+mFrame->h)/(float)mHeight,1,1);
        TexCoords[32] = glm::vec4((float)mFrame->x/(float)mWidth, (float)(mFrame->y)/(float)mHeight,0,0);
        TexCoords[33] = glm::vec4((float)(mFrame->x+mFrame->w)/(float)mWidth, (float)(mFrame->y+mFrame->h)/(float)mHeight,1,1);
        TexCoords[34] = glm::vec4((float)mFrame->x/(float)mWidth, (float)(mFrame->y)/(float)mHeight,0,0);
        TexCoords[35] = glm::vec4((float)(mFrame->x+mFrame->w)/(float)mWidth, (float)(mFrame->y+mFrame->h)/(float)mHeight,1,1);


        Primitives Prim;
        indicSize=Prim.generateCube(glm::vec3(1),
                                    glm::vec3(0),
                                    Color,TexCoords,
                                    &tvbo,&tibo,Prog);

    transformGL = glGetUniformLocation(Prog,"Trix");
    tex2shad = glGetUniformLocation(Prog,"myTexture");
    theView = glGetUniformLocation(Prog,"View");
    shaFoffset = glGetUniformLocation(Prog,"frameOffset");
    target = glGetUniformLocation(Prog,"Target");
    //glGenSamplers(1 , &sampler);
//glSamplerParameteri(sampler , GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
//glSamplerParameteri(sampler , GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    //glSamplerParameteri(sampler , GL_TEXTURE_MIN_FILTER , GL_LINEAR);
    //glSamplerParameteri(sampler , GL_TEXTURE_MAG_FILTER , GL_LINEAR);

return;
}

void H2DTexture::StopAnim(bool ReStart)
{
    stop = ReStart;
    if(FramesPerSecond>0 && MaxFrames >0)
        FrameOffset = (frameStartX-(int)(((SDL_GetTicks()-CurTime)*AnimateState)/(1000/FramesPerSecond))%MaxFrames);
    else
        FrameOffset = 0;
    pause = -1;

}

void H2DTexture::AutoRender(glm::mat4 View)
{
    if(!this)
        return;
    if(!tibo)
        initArray();

    switch(renderType)
    {
    case 0:
        render();
        break;
    case 1:
        render(View);
        break;
    case 2:
        renderTransOnly(View);
        break;
    default:
        break;
    }
}

void H2DTexture::render()
{
    if(isDead)
        return;

    if(H2DTexture::BoundTexture != texture)
    {
        glBindTexture (GL_TEXTURE_2D, texture);
        H2DTexture::BoundTexture = texture;
    }

    glBegin (GL_QUADS);
    glTexCoord2f ((float)mFrame->x/(float)mWidth, 0.0);
    glVertex3f (mDisplay->x, mDisplay->y, depth);
    glTexCoord2f ((float)(mFrame->x+mFrame->w)/(float)mWidth, 0.0);
    glVertex3f (mDisplay->x+mDisplay->w, mDisplay->y, depth);
    glTexCoord2f ((float)(mFrame->x+mFrame->w)/(float)mWidth, 1.0);
    glVertex3f (mDisplay->x+mDisplay->w, mDisplay->y+mDisplay->h, depth);
    glTexCoord2f ((float)mFrame->x/(float)mWidth, 1.0);
    glVertex3f (mDisplay->x, mDisplay->y+mDisplay->h, depth);
    glEnd ();

}

void H2DTexture::renderTransOnly(glm::mat4 VP)
{
    if(isDead)
        return;
glm::mat4 recenter = glm::translate(glm::mat4(1.0f),glm::vec3(-(float)mDisplay->w/2,-(float)mDisplay->h/2,0));
glm::mat4 uncenter = glm::translate(glm::mat4(1.0f),glm::vec3((float)mDisplay->w/2,(float)mDisplay->h/2,depth));
glm::mat4 scaleMe = glm::scale(glm::mat4(1.0f),glm::vec3((float)mDisplay->w,(float)mDisplay->h,(float)mDisplay->h));
glm::mat4 hold = glm::translate(glm::mat4(1.0f),glm::vec3(mDisplay->x,mDisplay->y,depth-(float)mDisplay->h/2)) *(transformer*uncenter*scaleMe);
    glUseProgram(Prog);
    glBindVertexArray( tibo );
    //glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, tibo);

    glBindTexture (GL_TEXTURE_2D, texture);
    //glUniform1i(tex2shad, 0);
    //H2DTexture::BoundTexture = texture;

    GLfloat fhold = mFrame->x-frameStartX;
    fhold /= (float)mWidth;

    glUniform1f(shaFoffset,fhold);
    glUniformMatrix4fv(theView,1,GL_FALSE,glm::value_ptr(VP));
    glUniformMatrix4fv(transformGL,1,GL_FALSE,glm::value_ptr(hold));

	//glDrawElements(GL_TRIANGLES, indicSize, GL_UNSIGNED_INT, 0);
	//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glDrawArrays( GL_TRIANGLES, 0, 6 );
    glBindVertexArray( 0 );
    glUseProgram(0);
    glBindTexture (GL_TEXTURE_2D, 0);

}

void H2DTexture::render(glm::mat4 VP)
{
    if(isDead)
        return;
    glm::mat4 recenter = glm::translate(glm::mat4(1.0f),glm::vec3(-(float)mDisplay->w/2,-(float)mDisplay->h/2,0));
    glm::mat4 uncenter = glm::translate(glm::mat4(1.0f),glm::vec3((float)mDisplay->w/2,(float)mDisplay->h/2,depth));
    glm::mat4 scaleMe = glm::scale(glm::mat4(1.0f),glm::vec3((float)mDisplay->w,(float)mDisplay->h,(float)mDisplay->h)*glm::vec3(0.95));
    glm::mat4 hold;
    if(!cubeit)
        hold = glm::translate(glm::mat4(1.0f),glm::vec3(mDisplay->x,mDisplay->y,depth*60-(float)mDisplay->h/2)) *(transformer*uncenter*scaleMe);
    else
        hold = glm::translate(glm::mat4(1.0f),glm::vec3(mDisplay->x,mDisplay->y,depth+mDisplay->h)) *(transformer*uncenter*scaleMe);
         glUseProgram(Prog);
    glBindVertexArray( tibo );
    //glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, tibo);

    //glActiveTexture(GL_TEXTURE0);
    glBindTexture (GL_TEXTURE_2D, texture);
    //  glUniform1i(tex2shad, 0);
    //glBindSampler(0, sampler );
    //H2DTexture::BoundTexture = texture;

GLfloat fhold = mFrame->x;//frameStartX;
fhold /= (float)mWidth;
fhold -= (float)frameStartX/(float)mWidth;

    glUniform1f(shaFoffset,fhold);
    glUniform4fv(target,1,glm::value_ptr(PlayerPos));
	glUniformMatrix4fv(theView,1,GL_FALSE,glm::value_ptr(VP));
	glUniformMatrix4fv(transformGL,1,GL_FALSE,glm::value_ptr(hold));

    if(!cubeit)
        glDrawArrays( GL_TRIANGLES, 0, 6 );
    else
        glDrawArrays( GL_TRIANGLES, 0, 36 );
	//glDrawElements(GL_TRIANGLES, indicSize, GL_UNSIGNED_INT, 0);



    //glUniform1f(shaFoffset,fhold);

	//glUniformMatrix4fv(theView,1,GL_FALSE,glm::value_ptr(VP));
	//glm::mat4 hold2 = glm::translate(glm::mat4(1.0f),glm::vec3(0,0,-0.1))* hold*decenter*glm::scale(glm::mat4(1.0f),glm::vec3(1.1,1.1,1))*center;
	//glUniformMatrix4fv(transformGL,1,GL_FALSE,glm::value_ptr(hold2));

    //glDrawArrays( GL_QUADS, 0, 4 );

    glBindVertexArray( 0 );
    //glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindTexture(GL_TEXTURE_2D,0);

    glUseProgram(0);

}


