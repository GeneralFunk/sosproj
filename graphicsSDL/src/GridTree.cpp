#include "../include/GridTree.h"

GridTree::GridTree(int WPX, int WPY, int Dimension, int Number)
{
    North=0;South=0;West=0;East=0;
    mapArea = {WPX,WPY,Dimension,Dimension};
    TopLeft = new Quadrant({WPX,WPY,Dimension/Number,Dimension/Number});
    dx = Number;
    dy = Number;
    theMap = std::vector<Quadrant *>();
    split(Number);
    LinkMap();
}

void GridTree::init(int WPX, int WPY, int Dimension, int Number,int num)
{
    gtnum = num;
    North=0;South=0;West=0;East=0;
    mapArea = {WPX,WPY,Dimension,Dimension};
    TopLeft = new Quadrant({WPX,WPY,Dimension/Number,Dimension/Number});
    dx = Number;
    dy = Number;
    theMap = std::vector<Quadrant *>();
    split(Number);
    LinkMap();
}

void GridTree::initObjectsVao(){
    Quadrant * Access = TopLeft;
    Quadrant * Row = TopLeft->South;

    while(Access){
        std::list<GameObject*>::iterator Hold = Access->items.begin();
        while(Hold != Access->items.end())
        {
            (*Hold)->Pic->initArray();
            Hold++;
        }
        Access = Access->East;
        if((!Access || Access->Area.x >= mapArea.x+mapArea.w) && Row)
        {
            if(Row->Area.y >= mapArea.y+mapArea.h){
                Access = NULL;
            }
            else{
                Access = Row;
                Row = Row->South;
            }

        }
    }

}

GridTree GridTree::operator=(const GridTree & rhs){
    TopLeft = rhs.TopLeft;
    mapArea = rhs.mapArea;
    theMap = rhs.theMap;
    North=rhs.North;
    South=rhs.South;
    East=rhs.East;
    West=rhs.West;

    return *this;
}

void GridTree::split(int N)
{
    TopLeft->gtnum = this;
    TopLeft->CreateSouthEast(N,NULL,&mapArea);
    TopLeft->LinkTheRest(NULL);

}

void GridTree::LinkMap(){
    Quadrant * Hold = TopLeft;
    Quadrant * HoldLeft = TopLeft->South;

    while(Hold){
        while(Hold->East){
            theMap.push_back(Hold);
            Hold = Hold->East;
        }
        theMap.push_back(Hold);
        Hold = HoldLeft;
        if(HoldLeft)
        HoldLeft = HoldLeft->South;
    }

}

void GridTree::UnStitch(){
    int k = dx*dy-dx;
    for(int i=0; i < dy;i++){
        if(theMap[i*dx]&&theMap[i*dx]->West)
            theMap[i*dx]->West->East=NULL;

        if(theMap[(i+1)*dx-1]&&theMap[(i+1)*dx-1]->East)
            theMap[(i+1)*dx-1]->East->West=NULL;

        if(theMap[i]&&theMap[i]->North)
            theMap[i]->North->South=NULL;

        if(theMap[i+k]&&theMap[i+k]->South)
            theMap[i+k]->South->North=NULL;
    }

}

bool GridTree::Stitch(GridTree * Patch){

    SDL_Rect pmap = Patch->mapArea;
    if(pmap.x+pmap.w == mapArea.x)
    {
        West = Patch;
        Patch->East=this;
        for(int i =0; i< dy;i++)
        {
            Patch->theMap[(i+1)*dx-1]->East=theMap[i*dx];
            theMap[i*dx]->West=Patch->theMap[(i+1)*dx-1];
        }

        return true;
    }
    if(pmap.x == mapArea.x+mapArea.w)
    {
        East = Patch;
        Patch->West=this;
        for(int i =0; i< dy;i++)
        {
            Patch->theMap[i*dx]->West=theMap[(i+1)*dx-1];
            theMap[(i+1)*dx-1]->East=Patch->theMap[i*dx];
        }
        return true;
    }
    int k = dx*dy-dx;
    if(pmap.y+pmap.h == mapArea.y)
    {
        North = Patch;
        Patch->South=this;
        for(int i =0; i< dx;i++)
        {
            Patch->theMap[i+k]->South=theMap[i];
            theMap[i]->North=Patch->theMap[i+k];
        }
        return true;
    }
    if(pmap.y == mapArea.y+mapArea.h)
    {
        South = Patch;
        Patch->North=this;
        for(int i =0; i< dx;i++)
        {
            Patch->theMap[i]->North=theMap[i+k];
            theMap[i+k]->South=Patch->theMap[i];
        }
        return true;
    }
    return false;
}

void GridTree::GenerateSurroundingChunks(std::vector<GridTree*> & MasterGTList,std::vector<H2DTexture> TileSheet){
    int offx=0,offy=0;
    Biomes B= Biomes();
    std::vector<GridTree*> OutList = std::vector<GridTree*>(9);
    std::vector <GameObject*> mapGenTiles((mapArea.w/(mapArea.w/dx/16))*(mapArea.w/(mapArea.w/dx/16)));
    Uint32 Timer=0,TotalTime=SDL_GetTicks();
    for(int i = 0; i <9;i++)
    {
        OutList[i]=NULL;
    }
    OutList[4]=this;

    if(!North || North==(GridTree*)0xFEEEFEEE){
            offy+=1;

        Timer=SDL_GetTicks();
        North = new GridTree();
        North->init(mapArea.x,mapArea.y-mapArea.h,mapArea.w,dx,gtnum-3);
        OutList[1]=North;
        B.GenerateChunkMap(North,mapGenTiles,TileSheet);
        Stitch(OutList[1]);
        std::cout << "Generate North: " << SDL_GetTicks()-Timer<<std::endl;
    }
    if(!South || South==(GridTree*)0xFEEEFEEE){
            offy+=-1;
        South = new GridTree();
        South->init(mapArea.x,mapArea.y+mapArea.h,mapArea.w,dx,gtnum+3);
        OutList[7]=South;
        B.GenerateChunkMap(OutList[7],mapGenTiles,TileSheet);
        Stitch(OutList[7]);
        std::cout << "Generate South: " << SDL_GetTicks()-Timer<<std::endl;
    }
    if(!West || West==(GridTree*)0xFEEEFEEE){
            offx+=-1;
        Timer = SDL_GetTicks();
        West = new GridTree();
        West->init(mapArea.x-mapArea.w,mapArea.y,mapArea.w,dx,gtnum-1);
        OutList[3]=West;
        B.GenerateChunkMap(OutList[3],mapGenTiles,TileSheet);
        Stitch(OutList[3]);
        std::cout << "Generate West: " << SDL_GetTicks()-Timer<<std::endl;
    }
    if(!East || East==(GridTree*)0xFEEEFEEE){
            offx+=1;
        Timer = SDL_GetTicks();
        East = new GridTree();
        East->init(mapArea.x+mapArea.w,mapArea.y,mapArea.w,dx,gtnum+1);
        OutList[5]=East;
        B.GenerateChunkMap(OutList[5],mapGenTiles,TileSheet);
        Stitch(OutList[5]);
        std::cout << "Generate East: " << SDL_GetTicks()-Timer<<std::endl;
    }
    if(!offx && !offy)
        return;
    Timer = SDL_GetTicks();
    if(offx >0){
        if(offy > 0){//NE
            delete MasterGTList[0];//->UnStitch();
            delete MasterGTList[3];//->UnStitch();
            delete MasterGTList[6];//->UnStitch();
            delete MasterGTList[7];//->UnStitch();
            delete MasterGTList[8];//->UnStitch();
            OutList[3]=MasterGTList[1];
            OutList[6]=MasterGTList[4];
            OutList[7]=MasterGTList[5];
        }
        else if(offy < 0){//SE
            delete MasterGTList[0];//->UnStitch();
            delete MasterGTList[1];//->UnStitch();
            delete MasterGTList[2];//->UnStitch();
            delete MasterGTList[3];//->UnStitch();
            delete MasterGTList[6];//->UnStitch();
            OutList[0]=MasterGTList[4];
            OutList[1]=MasterGTList[5];
            OutList[3]=MasterGTList[7];
        }
        else{//E
            delete MasterGTList[0];//->UnStitch();
            delete MasterGTList[3];//->UnStitch();
            delete MasterGTList[6];//->UnStitch();
            OutList[0]=MasterGTList[1];
            OutList[3]=MasterGTList[4];
            OutList[6]=MasterGTList[7];
            OutList[1]=MasterGTList[2];
            OutList[7]=MasterGTList[8];
        }
    }else if(offx <0){
        if(offy > 0){//NW
            delete MasterGTList[2];//->UnStitch();
            delete MasterGTList[5];//->UnStitch();
            delete MasterGTList[6];//->UnStitch();
            delete MasterGTList[7];//->UnStitch();
            delete MasterGTList[8];//->UnStitch();
            OutList[5]=MasterGTList[1];
            OutList[8]=MasterGTList[4];
            OutList[3]=MasterGTList[7];
        }
        else if(offy < 0){//SW
            delete MasterGTList[0];//->UnStitch();
            delete MasterGTList[1];//->UnStitch();
            delete MasterGTList[2];//->UnStitch();
            delete MasterGTList[5];//->UnStitch();
            delete MasterGTList[8];//->UnStitch();
            OutList[1]=MasterGTList[3];
            OutList[2]=MasterGTList[4];
            OutList[5]=MasterGTList[7];
        }
        else{//W
            delete MasterGTList[2];//->UnStitch();
            delete MasterGTList[5];//->UnStitch();
            delete MasterGTList[8];//->UnStitch();
            OutList[2]=MasterGTList[1];
            OutList[5]=MasterGTList[4];
            OutList[8]=MasterGTList[7];
            OutList[1]=MasterGTList[0];
            OutList[7]=MasterGTList[6];
        }
    }else if(offy > 0){//N
            delete MasterGTList[6];//->UnStitch();
            delete MasterGTList[7];//->UnStitch();
            delete MasterGTList[8];//->UnStitch();
            OutList[6]=MasterGTList[3];
            OutList[7]=MasterGTList[4];
            OutList[8]=MasterGTList[5];
            OutList[3]=MasterGTList[0];
            OutList[5]=MasterGTList[2];
    }
    else if(offy < 0){//S
            delete MasterGTList[0];//->UnStitch();
            delete MasterGTList[1];//->UnStitch();
            delete MasterGTList[2];//->UnStitch();
            OutList[0]=MasterGTList[3];
            OutList[1]=MasterGTList[4];
            OutList[2]=MasterGTList[5];
            OutList[3]=MasterGTList[6];
            OutList[5]=MasterGTList[8];
    }
    std::cout << "Chunk Deletion: " << SDL_GetTicks()-Timer<<std::endl;

    if(!North->West || North->West==(GridTree*)0xFEEEFEEE){
        Timer = SDL_GetTicks();
        North->West = new GridTree();
        North->West->init(mapArea.x-mapArea.w,mapArea.y-mapArea.h,mapArea.w,dx,gtnum-4);
        OutList[0]=North->West;
        B.GenerateChunkMap(OutList[0],mapGenTiles,TileSheet);
        North->Stitch(OutList[0]);
        West->Stitch(OutList[0]);
        std::cout << "Generate North West: " << SDL_GetTicks()-Timer<<std::endl;
    }
    if(!North->East || North->East==(GridTree*)0xFEEEFEEE){
        Timer = SDL_GetTicks();
        North->East = new GridTree();
        North->East->init(mapArea.x+mapArea.w,mapArea.y-mapArea.h,mapArea.w,dx,gtnum-2);
        OutList[2]=North->East;
        B.GenerateChunkMap(OutList[2],mapGenTiles,TileSheet);
        North->Stitch(OutList[2]);
        East->Stitch(OutList[2]);
       std::cout << "Generate North East: " << SDL_GetTicks()-Timer<<std::endl;
    }
    if(!South->West || South->West==(GridTree*)0xFEEEFEEE){
        Timer = SDL_GetTicks();
        South->West = new GridTree();
        South->West->init(mapArea.x-mapArea.w,mapArea.y+mapArea.h,mapArea.w,dx,gtnum+2);
        OutList[6]=South->West;
        B.GenerateChunkMap(OutList[6],mapGenTiles,TileSheet);
        South->Stitch(OutList[6]);
        West->Stitch(OutList[6]);
        std::cout << "Generate South West: " << SDL_GetTicks()-Timer<<std::endl;
    }
    if(!South->East || South->East==(GridTree*)0xFEEEFEEE){
        Timer = SDL_GetTicks();
        South->East = new GridTree();
        South->East->init(mapArea.x+mapArea.w,mapArea.y+mapArea.h,mapArea.w,dx,gtnum+4);
        OutList[8]=South->East;
        B.GenerateChunkMap(OutList[8],mapGenTiles,TileSheet);
        South->Stitch(OutList[8]);
        East->Stitch(OutList[8]);
        std::cout << "Generate South East: " << SDL_GetTicks()-Timer<<std::endl;
    }
    for(unsigned int i = 0; i <9;i++){
        MasterGTList[i]=OutList[i];
    }
    std::cout << "Generate Total Time: " << SDL_GetTicks()-TotalTime<<std::endl;
}

void GridTree::Sort(GameObject * gObject)
{
    TopLeft->SortObject(gObject);
}

Quadrant * GridTree::QuadByWorldPos(Quadrant * Q,int X, int Y)
{
    if(Q==NULL)
        return TopLeft->FindQuadByPos(X,Y);
    return Q->FindQuadByPos(X,Y);
}

Quadrant * GridTree::QuadByQuadPos(int qx, int qy){
    if(qx > dx || qy > dy || qx < 0 || qy < 0)
        return 0;
    return theMap[qx+qy*dx];

}

void GridTree::DeleteMap()
{
    for(int i = 0; i < dx; i++)
    {
        for(int j = 0; j < dy; j++)
        {
            delete theMap[i+j*dx];
            theMap[i+j*dx]=0;
        }
    }
    theMap.clear();
}
GridTree::~GridTree()
{
    if(North)
    {
        North->South=0;
    }
    if(South)
    {
        South->North=0;
    }
    if(West)
    {
        West->East=0;
    }
    if(East)
    {
        East->West=0;
    }
    North=0;South=0;West=0;East=0;
    UnStitch();
    DeleteMap();
    TopLeft = 0;
    //DeleteMap();
}
