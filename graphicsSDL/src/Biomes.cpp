#include "../include/Biomes.h"
int Biomes::ii;
int Biomes::perD;

Biomes::Biomes()
{
    //ctor
}

double Biomes::Noise1(double x, double y)
{
    int n=(int)x+(int)y*57;
 n=(n<<13)^n;
 int nn=(n*(n*n*60493+19990303)+1376312589)&0x7fffffff;
 return 1.0-((double)nn/1073741824.0);
    /*
    int n = x + y * 57;
    n = (n<<13) ^ n;
    return ( 1.0 - ( (n * (n * n * 15731 + 789221) + 1376312589) & 2147483647) / 1073741824.0);
    */
}

double Biomes::SmoothNoise1(double x, double y)
{
    double corners = ( Noise1(x-1, y-1)+Noise1(x+1, y-1)+Noise1(x-1, y+1)+Noise1(x+1, y+1) ) / 16;
    double sides   = ( Noise1(x-1, y)  +Noise1(x+1, y)  +Noise1(x, y-1)  +Noise1(x, y+1) ) /  8;
    double center  =  Noise1(x, y) / 4;
    return corners + sides + center;
}

  double Biomes::InterpolatedNoise_1(double x, double y)
{
      int integer_X    = (int)x;
      double fractional_X = x - integer_X;

      int integer_Y    = (int)y;
      double fractional_Y = y - integer_Y;

     double v1 = SmoothNoise1(integer_X,     integer_Y);
     double v2 = SmoothNoise1(integer_X + 1, integer_Y);
     double v3 = SmoothNoise1(integer_X,     integer_Y + 1);
     double v4 = SmoothNoise1(integer_X + 1, integer_Y + 1);

     double i1 = Cosine_Interpolate(v1 , v2 , fractional_X);
     double i2 = Cosine_Interpolate(v3 , v4 , fractional_X);

      return Cosine_Interpolate(i1 , i2 , fractional_Y);
}

double Biomes::Cosine_Interpolate(double a,double b,double x)
{
	double ft = x * 3.1415927;
	double f = (1 - cos(ft)) * .5;

	return  a*(1-f) + b*f;
}

double Biomes::PerlinNoise_2D(double x, double y)
{
    double total = 0;
    double p = 0.50;//Persistance
    int n = 6 - 1;//octaves-1

    for(int i =0; i < n; i++)
    {

        double frequency = 2*i;
        double amplitude = p*i;

        total = total + InterpolatedNoise_1(x * frequency, y * frequency) * amplitude;
    }
    return total;
}

void Biomes::GenerateChunkMap(GridTree * GT, std::vector<GameObject*> &mapTiles,std::vector<H2DTexture> Pics){

    int si = mapTiles.size();
    int pod=0;
    //Uint32 Timer,Diff=0;
    for(int i =0; i < si;i++)
    {
        pod = i%ii;
        Tile * Atile = new Tile(Pics[0]);

        Atile->phys->posX = pod * (perD) + (GT->mapArea.x);
        Atile->phys->posY = ((i-pod)/ii) * (perD) + (GT->mapArea.y);
        Atile->Pic->setPosition(Atile->phys->posX,Atile->phys->posY);
        Atile->Pic->dimension = perD;
        Atile->Pic->setupAnimation(SDL_Rect()={(i%6)*64,0,64,64},40,6);
        Atile->phys->BoundingRect = {0,0,0,0};
        Atile->SetTile();
        //Atile->BBtoDSPLY();
        GT->Sort(Atile);
        //A->phys->Update(A);
        mapTiles[i] = Atile;
        if(Atile->tileType==2 && ((int)PerlinNoise_2D(Atile->phys->posX/50,Atile->phys->posY/50)*7)==7)
        {
            Tree * Atree = new Tree(Pics[1]);
            Atree->isStatic = false;
            Atree->phys->posX = Atile->phys->posX-Pics[1].mDisplay->w+Pics[0].mDisplay->w;
            Atree->phys->posY = Atile->phys->posY-Pics[1].mDisplay->h+Pics[0].mDisplay->h;
            Atree->phys->Collidable = false;
            Atree->Pic->setPosition(Atree->phys->posX,Atree->phys->posY);
            Atree->Pic->depth = 0.1f;
            Atree->phys->BoundingRect.w = 93;
            Atree->phys->BoundingRect.h = 93;
            Atree->phys->BBoffset.x = Pics[1].mDisplay->w-93;
            Atree->phys->BBoffset.y = Pics[1].mDisplay->h-93;
            Atree->phys->BoundingRect.x = Atree->phys->posX+Atree->phys->BBoffset.x;
            Atree->phys->BoundingRect.y = Atree->phys->posY+Atree->phys->BBoffset.y;
            Atree->Pic->renderType = 1;
            GT->Sort(Atree);
            Atree = NULL;
        }
        if(Atile->tileType==1 && ((int)PerlinNoise_2D(Atile->phys->posX/100,Atile->phys->posY/100)*13)==13)
        {
            Enemy * Ene = new Enemy(Pics[2]);
            Ene->isStatic = false;
            Ene->phys->posX = Atile->phys->posX;//-Pics[2].mDisplay->w;
            Ene->phys->posY = Atile->phys->posY;//-Pics[2].mDisplay->h;
            Ene->phys->Collidable = false;
            Ene->Pic->setPosition(Ene->phys->posX,Ene->phys->posY);
            Ene->Pic->depth = 0.2f;
            Ene->Pic->renderType = 1;
            Ene->BBtoDSPLY();
            GT->Sort(Ene);
            Ene = NULL;
        }
        Atile = NULL;

    }
    //std::cout << "SET Tile generation: " << Diff<<std::endl;
}

void Biomes::ChunkSetting(Quadrant*ChunkRoot)
{
    Quadrant*Hold = ChunkRoot;
    Quadrant* Hold2 = ChunkRoot->South;
    //Hold2 = Hold2->East;
    std::list<GameObject*>::iterator HoldGO;
    std::vector<int> largest = std::vector<int>(6);
    while(Hold != NULL)
    {
        HoldGO = Hold->items.begin();
        while(HoldGO != Hold->items.end())
        {
            if( (*HoldGO)->typecast.compare("Tile")==0)
            {
                Tile * A = dynamic_cast<Tile*>((*HoldGO));
                if(A != NULL)
                {
                    largest[A->tileType]+=1;//A->Pic->mFrame->x/A->Pic->mFrame->w
                }

            }
            HoldGO++;
        }
        int large = largest[0];
        largest[0]=0;
        int i=0;
        for(int ii =1; ii < 6; ii++)
        {
            if(large < largest[ii])
            {
                large = largest[ii];

                i = ii;
            }
            largest[ii]=0;
        }
        HoldGO = Hold->items.begin();

        while(HoldGO != Hold->items.end())
        {
            if((*HoldGO)->typecast == "Tile")
            {
                Tile * A = (Tile*)(*HoldGO);
                if(A != NULL)
                    A->SetTile(i);
            }
            HoldGO++;
        }
        Hold = Hold->East;
        if(Hold == NULL)
        {
            if(Hold2 == NULL)
                return;
            Hold = Hold2;
            Hold2 = Hold2->South;
        }
    }
}


Biomes::~Biomes()
{
    //dtor
}
