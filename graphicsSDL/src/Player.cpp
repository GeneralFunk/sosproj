#include "../include/Player.h"

Player::Player():GameObject("Player")
{
    isStatic = false;
    phys->Collidable = true;
    kstate = SDL_GetKeyboardState(&KeyLength);
    oldkstate = new Uint8[KeyLength];
    memcpy(oldkstate,kstate,KeyLength);
    LastUpdate = SDL_GetTicks();
    UseDelta = true;
    OnMouseOver= &MouseOver;//(&Player::MouseOver);
    //std::copy(kstate,kstate+KeyLength,oldkstate);
    //oldkstate = kstate;
}

Player::Player(H2DTexture & thePic):GameObject("Player")
{
    isStatic = false;
    phys->Collidable = true;
    Pic = new H2DTexture(thePic);
    BBtoDSPLY();
    kstate = SDL_GetKeyboardState(&KeyLength);
    oldkstate = new Uint8[KeyLength];
    memcpy(oldkstate,kstate,KeyLength);
    LastUpdate = SDL_GetTicks();
    UseDelta = true;
    OnMouseOver = &MouseOver;
    //std::copy(kstate,kstate+KeyLength,oldkstate);
    //oldkstate = kstate;
}

bool Player::Move(glm::vec3 & Eye)// SDL_mutex * tls_lock)
{
    float moveX=0,moveY=0;
    glm::vec3 Dcomp = phys->Direction;

    if(phys->Collided == Seperate)
    {
        //SDL_LockMutex(tls_lock);
        Pic->modAbsScale(1);
        phys->Ctimes = 0;
        //SDL_UnlockMutex(tls_lock);
    }
    float MaxSpeed = 1000;
    //SDL_LockMutex(tls_lock);
    Pic->AnimateState = 0;
    //SDL_UnlockMutex(tls_lock);

    if(oldkstate[SDL_SCANCODE_T] && !kstate[SDL_SCANCODE_T])
    {
        //SDL_LockMutex(tls_lock);
       // UseDelta = !UseDelta;
       phys->Collided = Seperate;
        //SDL_UnlockMutex(tls_lock);
    }

    if(kstate[SDL_SCANCODE_W])
    {
        if(kstate[SDL_SCANCODE_A])
        {
            moveX -=MaxSpeed;
        }
        else if(kstate[SDL_SCANCODE_D])
        {
            moveY -=MaxSpeed;
        }
        else {
            moveX -=MaxSpeed*cos(0.785398163);
            moveY -=MaxSpeed*sin(0.785398163);
        }
        //SDL_LockMutex(tls_lock);
        Pic->AnimateState = 1;
        //SDL_UnlockMutex(tls_lock);
    }
    else if(oldkstate[SDL_SCANCODE_W] && !kstate[SDL_SCANCODE_W])
    {
        //SDL_LockMutex(tls_lock);
        Pic->StopAnim(false);
        //SDL_UnlockMutex(tls_lock);
    }

    if(kstate[SDL_SCANCODE_S])
    {
        if(kstate[SDL_SCANCODE_A])
        {
            moveY +=MaxSpeed;
        }
        else if(kstate[SDL_SCANCODE_D])
        {
            moveX +=MaxSpeed;
        }
        else {

            moveY +=MaxSpeed*cos(0.785398163);
            moveX +=MaxSpeed*sin(0.785398163);
        }
        //SDL_LockMutex(tls_lock);
        Pic->AnimateState = -1;
        //SDL_UnlockMutex(tls_lock);
    }
    else if(oldkstate[SDL_SCANCODE_S] && !kstate[SDL_SCANCODE_S])
    {
        //SDL_LockMutex(tls_lock);
        Pic->StopAnim(false);
        //SDL_UnlockMutex(tls_lock);
    }

    if(kstate[SDL_SCANCODE_A] && !(kstate[SDL_SCANCODE_W] || kstate[SDL_SCANCODE_S]))
    {
        moveY +=MaxSpeed*cos(0.785398163);
        moveX -=MaxSpeed*sin(0.785398163);
        //SDL_LockMutex(tls_lock);
        Pic->AnimateState = -1;
        //SDL_UnlockMutex(tls_lock);
    }
    else if(oldkstate[SDL_SCANCODE_A] && !kstate[SDL_SCANCODE_A])
    {
        //SDL_LockMutex(tls_lock);
        Pic->StopAnim(false);
        //SDL_UnlockMutex(tls_lock);
    }

    if(kstate[SDL_SCANCODE_D] && !(kstate[SDL_SCANCODE_W] || kstate[SDL_SCANCODE_S]))
    {
        moveY -=MaxSpeed*cos(0.785398163);
        moveX +=MaxSpeed*sin(0.785398163);
        //SDL_LockMutex(tls_lock);
        Pic->AnimateState = 1;
        //SDL_UnlockMutex(tls_lock);
    }
    else if(oldkstate[SDL_SCANCODE_D] && !kstate[SDL_SCANCODE_D])
    {
        //SDL_LockMutex(tls_lock);
        Pic->StopAnim(false);
        //SDL_UnlockMutex(tls_lock);
    }

    //SDL_LockMutex(tls_lock);
    if(!UseDelta)
        phys->TeleportTo((float)phys->posX + moveX, (float)phys->posY + moveY,this);
    else{
            if(moveX || moveY)
            {
             phys->Direction = glm::normalize(glm::vec3(moveX,moveY,0));
                phys->Direction.x *=3;
                phys->Direction.y *=3;
            }
            else
                phys->Direction = glm::vec3(0);
    //phys->HomeIn((float)phys->posX + moveX, (float)phys->posY + moveY,this);
    }
   phys->moveTween(this);
    Eye.x = phys->posX+Pic->mDisplay->w/2;
    Eye.y = phys->posY+Pic->mDisplay->h/2;
    memcpy(oldkstate,kstate,KeyLength);
    //SDL_UnlockMutex(tls_lock);
    //std::copy(kstate,kstate+KeyLength,oldkstate);

    return (Dcomp == phys->Direction);
}

void Player::MouseOver(GameObject * Obj)
{
    Player * P = (Player *)Obj;
    P->Pic->modAbsScale(0.5f + cos((float)(P->phys->Ctimes)/1000));
}

void Player::setID(unsigned long long ID)
{
    ServerId = ID;
    Name = "";
    while(ID > 100)
    {
        Name += (char)(ID%26+65);
        ID /= 100;
    }
}

Player::~Player()
{
    if(CurrentQ)
        CurrentQ->RemoveItem(this);
    delete Pic;
    delete phys;
    //delete CurrentQ;
    phys = NULL;
    CurrentQ = NULL;
    Pic = NULL;
    delete kstate;
    delete oldkstate;

}
