#include "../include/Quadrant.h"

Quadrant::Quadrant(SDL_Rect newArea)
{
    //ctor
    TilesSet = false;
    Area = newArea;
    items = std::list<GameObject*>();
    North=NULL;
    South=NULL;
    West=NULL;
    East=NULL;
}
//Intial Quad makers
void Quadrant::CreateSouthEast(int N,Quadrant * Last,SDL_Rect * TotalArea)
{
    CreateEast(N,NULL,TotalArea);
    if(North == NULL)
        North=Last;
    if(South!=NULL)
        South->CreateSouthEast(N,this,TotalArea);

    if(Area.y + Area.h >= TotalArea->y + TotalArea->h)
        return;
    South = new Quadrant(SDL_Rect()={Area.x,Area.y+Area.h,Area.w,Area.h});
    South->gtnum = gtnum;
    South->qPosX=qPosX;
    South->qPosY=qPosY+1;

    South->CreateSouthEast(N,this,TotalArea);
}
void Quadrant::CreateEast(int N,Quadrant*Last,SDL_Rect * TotalArea)
{
    if(West == NULL)
        West=Last;
    if(East!=NULL)
        East->CreateEast(N,this,TotalArea);

    if(Area.x + Area.w >= TotalArea->x+TotalArea->w)
        return;
    East = new Quadrant(SDL_Rect()={Area.x+Area.w,Area.y,Area.w,Area.h});
    East->gtnum = gtnum;
    East->qPosX=qPosX+1;
    East->qPosY=qPosY;
    East->CreateEast(N,this,TotalArea);
}
/*  //Secondary Maker
void Quadrant::CreateNorthEast(int N,Quadrant*Last,SDL_Rect * TotalArea)
{
    CreateEast(N,NULL,TotalArea);
    if(South==NULL)
        South=Last;
    if(North != NULL)
        North->CreateNorthEast(N,this,TotalArea);


    if(Area.y <= TotalArea->y)
        return;
    North = new Quadrant(SDL_Rect()={Area.x,Area.y+Area.h,Area.w,Area.h});

    North->qPosX=qPosX;
    North->qPosY=qPosY++;

    North->CreateNorthEast(N,this,TotalArea);
}*/

//Links Initial
void Quadrant::LinkTheRest(Quadrant * Q)
{
    Q=East;
    while(Q != NULL)
    {
        Q->LinkNS(NULL);
        Q=Q->East;
    }
}

//Links Initial
void Quadrant::LinkNS(Quadrant* Last)
{
    if(West != NULL)
        if(West->South != NULL)
            if(West->South->East != NULL)
                West->South->East->LinkNS(this);
    if(North == NULL)
        North = Last;
    if(North!=NULL)
        if(North->South ==NULL)
            North->South = this;
}

void Quadrant::UpdateQuadrants(int Radius)//,SDL_mutex * tls_lock)
{
    if(Radius <= 0)
    {
        UpdateItems();//tls_lock);
        return;
    }
    Quadrant * Hold = this;//FindQuadByPos(Area.x-Radius*Area.w,Area.y+Area.h*-Radius);
    Quadrant * LastHold = 0;
    for(int x = -Radius; x <= Radius; x++)
    {
        for(int y = -Radius; y <= Radius; y++)
        {
            Hold = Hold->FindQuadByPos(Area.x+x*Area.w+1,Area.y+Area.h*y+1);
            if(Hold != LastHold)
            {
                Hold->UpdateItems();//tls_lock);
                LastHold = Hold;
            }
        }
    }
    Hold = 0;
    LastHold = 0;
}


void Quadrant::UpdateItems()
{
    (*items.begin())->Update();
    std::list<GameObject*>::iterator Hold = items.begin();
    Hold++;
    std::list<GameObject*>::iterator Holdprev = items.begin();
    if(Hold == items.end()|| Holdprev == items.end())
        return;

    while(Hold != items.end() && (*Hold) != (GameObject*)0xFEEEFEEE)
    {

        if((*Hold)->AIupdate != NULL){
            (*Hold)->AIupdate(*Hold);
        }
        else{
            (*Hold)->Update();
        }
        if(listModded){
            Hold = Holdprev;
            listModded = false;
            Hold++;
        }
        else
        {
            Hold++;
            Holdprev++;
        }
    }
}

void Quadrant::Render(int Radius,glm::mat4 VP)
{
    if(Radius <= 0)
    {
        Render(VP);
        return;
    }
    Quadrant * Hold = FindQuadByPos(Area.x+(-Radius)*Area.w,Area.y+Area.h*(-Radius));
    Quadrant * LastHold = 0;
    for(int x = -Radius; x <= Radius; x++)
    {
        for(int y = -Radius; y <= Radius; y++)
        {
            Hold = Hold->FindQuadByPos(Area.x+x*Area.w,Area.y+Area.h*y);
            if(Hold != LastHold)
            {
                //Hold->RenderByRadius(Area.x+Area.w/2,Area.y+Area.h/2,Area.w*Radius,VP);
                Hold->Render(VP);
                LastHold = Hold;
            }
        }
    }
    Hold = 0;
    LastHold = 0;
}

void Quadrant::RenderByRadius(int x,int y, int Radius,glm::mat4 VP)
{
    std::list<GameObject*>::iterator Hold = items.begin();

    while(Hold != items.end())
    {
        if((*Hold)->CheckType("Player"))
            (*Hold)->Pic->renderTransOnly(VP);
        else if((*Hold)->Within(x,y,Radius))
            (*Hold)->Pic->render(VP);

        Hold++;
    }
}

void Quadrant::Render(glm::mat4 VP)
{
    std::list<GameObject*>::iterator Hold = items.begin();

    while(Hold != items.end() && (*Hold) !=(GameObject*)0xFEEEFEEE)
    {
        if((*Hold) != (GameObject*)0xfeeefeee)
            (*Hold)->Pic->AutoRender(VP);
        Hold++;
    }
}

void Quadrant::RemoveItem(GameObject * item)
{
    items.remove(item);
    listModded=true;
}
void Quadrant::AddItem(GameObject * item)
{
    items.push_back(item);
}

Quadrant * Quadrant::FindQuadByPos(int x, int y)
{
    Quadrant * hold = this;
    if(hold == NULL || hold == (Quadrant *)0xFEEEFEEE)
        return NULL;
    while(x >= hold->Area.x+hold->Area.w)
    {
        if(hold->East == NULL || hold->East == (Quadrant *)0xFEEEFEEE)
            break;
        hold = hold->East;
    }
    while(x < hold->Area.x)
    {
        if(hold->West == NULL || hold->West == (Quadrant *)0xFEEEFEEE)
            break;
        hold = hold->West;
    }
    while(y >= hold->Area.y+hold->Area.h)
    {
        if(hold->South == NULL || hold->South == (Quadrant *)0xFEEEFEEE)
            break;
        hold = hold->South;
    }
    while(y < hold->Area.y)
    {
        if(hold->North == NULL || hold->North == (Quadrant *)0xFEEEFEEE)
            break;
        hold = hold->North;
    }
    return hold;
}

bool Quadrant::UpdateItem(GameObject * gObject)
{
    if(gObject == NULL)
        return false;
   /* if(gObject->phys->posX >= Area.x && gObject->phys->posX < Area.x + Area.w)
    {
        if(gObject->phys->posY >= Area.y && gObject->phys->posY < Area.y + Area.h)
        {
            return true;
        }
    } */
    if(FindQuadByPos(gObject->phys->posX,gObject->phys->posY)==this)
        return true;

    RemoveItem(gObject);

    if(gObject == NULL)
        return false;

    SortObject(gObject);

    return true;
}

void Quadrant::SortObject(GameObject * gObject)
{
    Quadrant * Hold = FindQuadByPos(gObject->phys->posX,gObject->phys->posY);
    if(Hold != NULL)
    {
        Hold->AddItem(gObject);
        gObject->CurrentQ=Hold;
    }

}


Quadrant::~Quadrant()
{
    North=NULL;
    South=NULL;
    West=NULL;
    East=NULL;
    std::list<GameObject*>::iterator Hold = items.begin();

    while(Hold != items.end() && (*Hold) != (GameObject*)0xFEEEFEEE){
        if((*Hold) && (*Hold) != (GameObject*)0xFEEEFEEE)
            delete (*Hold);
        Hold++;

    }
    items.clear();
    //dtor
}
