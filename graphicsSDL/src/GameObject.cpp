#include "../include/GameObject.h"

GameObject::GameObject()
{
    Pic = NULL;
    phys = new Physics();
    typecast = "gaob";
    CurrentQ = NULL;
    isStatic = true;
//    D=0;
    OnCollide = NULL;
    OnMouseOver = NULL;
    OnClick = NULL;
    AIupdate = NULL;
}

GameObject::GameObject(std::string theType)
{
    Pic = NULL;
    phys = new Physics();
    typecast = theType;
    CurrentQ = NULL;
    isStatic = true;
//    D=0;

    OnCollide = NULL;
    OnMouseOver = NULL;
    OnClick = NULL;

    AIupdate = NULL;
}

GameObject::GameObject(H2DTexture & image)
{
    Pic = new H2DTexture(image);
    phys = new Physics();
    typecast = "gaob";
    CurrentQ = NULL;
    isStatic = true;
//    D=0;

    OnCollide = NULL;
    OnMouseOver = NULL;
    OnClick = NULL;
    AIupdate = NULL;
}

GameObject::GameObject(GameObject & ToCopy)
{
    Pic = new H2DTexture(*(&ToCopy)->Pic);
    phys = new Physics();
    typecast = ToCopy.typecast;
    CurrentQ = ToCopy.CurrentQ;
    isStatic = ToCopy.isStatic;
//    D=0;

    OnCollide = NULL;
    OnMouseOver = NULL;
    OnClick = NULL;
    AIupdate = NULL;
}
/*
GameObject::GameObject(GameObject * ToCopy)
{
    Pic = new H2DTexture(*ToCopy->Pic);
    phys = new Physics();
    typecast = ToCopy->typecast;
    CurrentQ = ToCopy->CurrentQ;
    isStatic = ToCopy->isStatic;
//    D=0;

    OnCollide = NULL;
    OnMouseOver = NULL;
    OnClick = NULL;
}*/

GameObject::GameObject(H2DTexture & image,std::string theType)
{
    Pic = new H2DTexture(image);
    phys = new Physics();
    typecast = theType;
    CurrentQ = NULL;
    isStatic = true;
//    D=0;

    OnCollide = NULL;
    OnMouseOver = NULL;
    OnClick = NULL;
    AIupdate = NULL;
}

GameObject GameObject::operator=(const GameObject * rhs)
{
    Pic = new H2DTexture(*rhs->Pic);
    phys = new Physics();
    typecast = rhs->typecast;
    CurrentQ = rhs->CurrentQ;
    isStatic = rhs->isStatic;
//    D=0;

    OnCollide = NULL;
    OnMouseOver = NULL;
    OnClick = NULL;
    AIupdate = rhs->AIupdate;
    return *this;
}

bool GameObject::CheckType(std::string toCheck)
{
    return !(typecast.compare(toCheck));
}

bool GameObject::Within(int x, int y, int Radius)
{
    int A = phys->posX-x;
    int B = phys->posY-y;
    if(Radius*Radius >= A*A + B*B)
        return true;
    return false;
}


void GameObject::SetColor()
{
    //Pic->setColor(phys->posX%235+20,(phys->posX+phys->posY)%235+20,phys->posY%235+20);
    //Pic->setAlpha(((int)abs(235*cos(D))+20)%255);
}
void GameObject::BBtoDSPLY()
{
    phys->BoundingRect = *Pic->mDisplay;
}

void GameObject::KeepWithin( float posX, float posY, float Distance,Quadrant * Entry){

    float a = posX-phys->posX;
    float b = posY-phys->posY;
    if((a*a)+(b*b) > Distance*Distance){
        if(CurrentQ)
        {
            CurrentQ->RemoveItem(this);
        }
    }else {
        if(!CurrentQ)
        {
            Entry->SortObject(this);
        }
    }

}

void GameObject::Update()//SDL_mutex * tls_lock)
{
//    D+=0.02;
    if(this == (GameObject*)0xfeeefeee || !this)
        return;
    if(Pic&&CurrentQ)
    {
        if(phys)
        {

            if(!isStatic)
                phys->Update(this);
            //SDL_LockMutex(tls_lock);
            Pic->setPosition(phys->posX,phys->posY);
            //SDL_UnlockMutex(tls_lock);
            Pic->UpdateAnimate();

        }
    }
}

void GameObject::render()
{
    //Pic->render(phys->posX,phys->posY);
    Pic->render();
    //Pic->batch();
}

GameObject::~GameObject()
{
   // if(CurrentQ)
   //     CurrentQ->RemoveItem(this);
    delete Pic;
    delete phys;

    //delete CurrentQ;
    phys = NULL;
    CurrentQ = NULL;
    Pic = NULL;
}
