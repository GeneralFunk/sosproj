#version 330

in  vec4 vPosition;
in  vec4 vNormal;
in  vec4 vColor;
in vec4 vTexCoords;
out vec4 color;
out vec4 modulation;
out vec2 vtc;
out float radius;

uniform vec4 Target;
uniform float frameOffset;
uniform mat4 Trix;
uniform mat4 View;

void main()
{
    vtc = vec2(vTexCoords.x+frameOffset,vTexCoords.y);//vTexCoords.xy*16);
    color = vColor*Trix;
    //vec4 pos = vPosition;
    //pos.xy *= 10*cos(pos.x);0
    vec4 Pos = Trix * vPosition;
    vec4 vOffset = Target-Pos;//-Target;
    modulation = vOffset;
    vOffset.z -=0.05;
    radius = 361;
 /*   if(vOffset.x*vOffset.x+vOffset.y*vOffset.y >=radius*radius){//+Target.x*Target.y){
        vOffset = vOffset-radius*normalize(vOffset);
        mat4 off = mat4(1.0f);
        off[3].xyz = vOffset.xyz;
        gl_Position = gl_ModelViewProjectionMatrix * View * off*(Pos);//+vOffset);//*Trix;//rz * ry * rx * (vPosition*Squish);
    }
    else*/
        gl_Position = gl_ModelViewProjectionMatrix * View * Trix* vPosition;//*Trix;//rz * ry * rx * (vPosition*Squish);
}
