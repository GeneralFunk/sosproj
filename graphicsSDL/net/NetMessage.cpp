#include "NetMessage.h"

NetMessage::~NetMessage() {
    delete buffer;
}

NetMessage::NetMessage(size_t type, size_t size) {
    this->type = type;
    this->pos = 0;
    this->size = size;
    buffer = new char[size];
    put(type);
}

bool NetMessage::put(bool b) {
    if ((pos+sizeof(bool)) > size) return false;
    *((bool*)(pos+buffer)) = b;
    pos += sizeof(bool);
    return true;
}
bool NetMessage::put(char c) {
    if ((pos+sizeof(char)) > size) return false;
    *((char*)(pos+buffer)) = c;
    pos += sizeof(char);
    return true;
}

bool NetMessage::put(int n) {
    if ((pos+sizeof(int)) > size) return false;
    *((int*)(pos+buffer)) = n;
    pos += sizeof(int);
    return true;
}

bool NetMessage::put(long l) {
    if ((pos+sizeof(long)) > size) return false;
    *((long*)(pos+buffer)) = l;
    pos += sizeof(long);
    return true;
}

bool NetMessage::put(float v) {
    if ((pos+sizeof(float)) > size) return false;
    *((float*)(pos+buffer)) = v;
    pos += sizeof(float);
    return true;
}

bool NetMessage::put(long long l) {
    if ((pos+sizeof(long long)) > size) return false;
    *((long long*)(pos+buffer)) = l;
    pos += sizeof(long long);
    return true;
}

bool NetMessage::put(double d) {
    if ((pos+sizeof(double)) > size) return false;
    *((double*)(pos+buffer)) = d;
    pos += sizeof(double);
    return true;
}

bool NetMessage::put(unsigned char c) {
    if ((pos+sizeof(unsigned char)) > size) return false;
    *((unsigned char*)(pos+buffer)) = c;
    pos += sizeof(unsigned char);
    return true;
}

bool NetMessage::put(unsigned int n) {
    if ((pos+sizeof(unsigned int)) > size) return false;
    *((unsigned int*)(pos+buffer)) = n;
    pos += sizeof(unsigned int);
    return true;
}

bool NetMessage::put(unsigned long l) {
    if ((pos+sizeof(unsigned long)) > size) return false;
    *((unsigned long*)(pos+buffer)) = l;
    pos += sizeof(unsigned long);
    return true;
}

bool NetMessage::put(unsigned long long l) {
    if ((pos+sizeof(unsigned long long)) > size) return false;
    *((unsigned long long*)(pos+buffer)) = l;
    pos += sizeof(unsigned long long);
    return true;
}

bool NetMessage::put(const char * str) {
    int i;
    for (i = 0; ((buffer+pos)[i] = str[i]); i++)
        if (pos+i > size)
            return false;
    pos+=i+1;
    return true;
}

bool NetMessage::put(const void * obj, size_t sz) {
    size_t i;
    if (pos + sz > size) return false;

    /* Fast copy */
    for (i = 0; i < sz; i+=sizeof(long long)) {
        *((long long *)(buffer+pos+i)) = *((long long*)obj);
    }
    i-=sizeof(long long);

    /* Remainder copy */
    for (; i < sz; i++) {
        *((char *)(buffer+pos+i)) = *((char*)obj);
    }

    pos += sz;

    return true;
}

const char * NetMessage::getData() {
    return buffer;
}

size_t NetMessage::getSize() {
    return size;
}

NetMessage& NetMessage::operator<<(bool b) {
    put(b);
    return *this;
}

NetMessage& NetMessage::operator<<(char c) {
    put(c);
    return *this;
}

NetMessage& NetMessage::operator<<(int n) {
    put(n);
    return *this;
}
NetMessage& NetMessage::operator<<(long l) {
    put(l);
    return *this;
}

NetMessage& NetMessage::operator<<(float v) {
    put(v);
    return *this;
}

NetMessage& NetMessage::operator<<(long long l) {
    put(l);
    return *this;
}

NetMessage& NetMessage::operator<<(double d) {
    put(d);
    return *this;
}

NetMessage& NetMessage::operator<<(unsigned char c) {
    put(c);
    return *this;
}

NetMessage& NetMessage::operator<<(unsigned int n) {
    put(n);
    return *this;
}

NetMessage& NetMessage::operator<<(unsigned long l) {
    put(l);
    return *this;
}

NetMessage& NetMessage::operator<<(unsigned long long l) {
    put(l);
    return *this;
}

NetMessage& NetMessage::operator<<(const char * str) {
    put(str);
    return *this;
}
