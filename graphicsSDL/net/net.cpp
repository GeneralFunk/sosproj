#include "net.h"

bool ____net_init = false;

#ifndef __WINDOWS
void closesocket(int socket) {close(socket);};
#endif // __WINDOWS

void closeNet() {
    if(!____net_init) return;
    #ifdef __WINDOWS
    WSACleanup();
    #endif
    ____net_init = false;
}

void startNet() {
    if (!____net_init) {
        #ifdef __WINDOWS
        WSADATA wsaData;
        WSAStartup(0x0202, &wsaData);
        #endif
        ____net_init = true;
    }
}
