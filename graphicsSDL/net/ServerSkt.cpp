#include "ServerSkt.h"

static bool server_init = false;
static STDRand * srv_random = NULL;

void ServerSkt::init() {
    if (server_init) return;
    srv_random = new STDRand();
}


ServerSkt::ServerSkt(int port, size_t size) {
    init();
    this->disconnect     = NULL;
    this->packetReceived = NULL;
    this->port = port;
    this->size = size;
}

ServerSkt::~ServerSkt() {
    stop();
}

ServerSkt * ServerSkt::setPacketCallback(void (*packetReceived)(ServerSkt * server, SessionID * id, Packet * packet)) {
    this->packetReceived = packetReceived;
    return this;
}

ServerSkt * ServerSkt::setDicconectCallback(void (*disconnect)(ServerSkt * server, SessionID * id)) {
    this->disconnect = disconnect;
    return this;
}


void ServerSkt::receive(Connection * conn) {
        /* receive packet */
        char buffer[size];
        Packet * packet;

        int newData;

        while ((newData = recv(conn->socket, buffer, size, 0)) > 0) {
            packet = new Packet(buffer, size);
            if (packetReceived)
                packetReceived(this, (SessionID*)conn, packet);


            delete packet;
        }

        closesocket(conn->socket);
        connSet.erase(conn);
}

int ServerSkt::recvEntry(void * args) {
    ((Connection*)args)->server->receive((Connection*)args);
    return 0;
}

void ServerSkt::listenFor() {
    /* accept socket */
	int clientSize = sizeof(sockaddr_in);

	Connection * conn = new Connection();
	conn->server = this;
	while( ((conn->socket= accept(thisSocket, (sockaddr *)&conn->addr, (int *) &clientSize))>=0) )
	{
		cout << "\nConnection Established!" << endl;

        connSet.emplace(conn);
		SDL_DetachThread(SDL_CreateThread(recvEntry, "receive", (void*)conn));

		conn = new Connection();
		conn->server = this;
	}
	delete conn;
}

int ServerSkt::listenEntry(void * args) {
    ((ServerSkt*)args)->listenFor();
    return 0;
}

bool ServerSkt::sendTo(SessionID * id, NetMessage * msg) {
    if (connSet.find((Connection*)id) == connSet.end()) return false;
    send(((Connection*)id)->socket, msg->getData(), msg->getSize(), 0);
    delete msg;
    return true;
}

bool ServerSkt::sendAll(NetMessage * msg) {
    for (auto citr = connSet.begin(); citr != connSet.end(); ++citr) {
        send((*citr)->socket, msg->getData(), msg->getSize(), 0);
    }
    delete msg;
    return true;
}

bool ServerSkt::broadcast(SessionID * id, NetMessage * msg) {
    for (auto citr = connSet.begin(); citr != connSet.end(); ++citr) {
        if ((Connection*)id != *citr)
            send((*citr)->socket, msg->getData(), msg->getSize(), 0);
    }
    delete msg;
    return true;
}

int ServerSkt::start() {
    /* get socket */
	listenAddr.sin_family = AF_INET;
	thisSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (thisSocket < 0)
	{
		cout << "\nSocket Creation FAILED!";
		return 1;
	}

    /* bind (open) socket */
    listenAddr.sin_port = htons (port);
	listenAddr.sin_addr.s_addr = INADDR_ANY;
	if (bind(thisSocket, (sockaddr *)&listenAddr, sizeof(listenAddr))<0){
		cout << "\nBinding Socket FAILED!\n";
		if (thisSocket) closesocket(thisSocket);
		return 1;
	}

    /* listen on socket */
	cout << "\nListening on " << port << "..." << endl;
	if (listen(thisSocket, 5)<0){
		cout << "\nListening on Socket FAILED!\n";
		if (thisSocket) closesocket(thisSocket);
		return 1;
	}

    SDL_DetachThread(SDL_CreateThread(listenEntry, "listen", (void*)this));



    return 0;
}

bool ServerSkt::stop() {
	closesocket(thisSocket);
    return true;
}
