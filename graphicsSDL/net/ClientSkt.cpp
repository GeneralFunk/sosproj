#include "ClientSkt.h"

ClientSkt::ClientSkt(int port, const char * hostname, size_t size) {
    this->hostname = hostname;
    this->port = port;
    this->size = size;
    this->packetReceived = NULL;
    this->disconnect     = NULL;
    this->clientSocket = -1;
    qlock = SDL_CreateMutex();
}

ClientSkt * ClientSkt::setPacketCallback(void (*packetReceived)(ClientSkt * client, Packet * packet)) {
    this->packetReceived = packetReceived;
    return this;
}
ClientSkt * ClientSkt::setDicconectCallback(void (*disconnect)(ClientSkt * client)) {
    this->disconnect = disconnect;
    return this;
}


ClientSkt::~ClientSkt() {
    stop();
}

void ClientSkt::receive() {
	char buffer[size];
	while(recv(clientSocket, buffer, size, 0) > 0) {
        Packet * packet = new Packet(buffer, size);
        if (packetReceived)
            packetReceived(this, packet);
        delete packet;
	}
	if (disconnect)
        disconnect(this);
}

int ClientSkt::recvEntry(void * args) {
    ((ClientSkt*)args)->receive();
    return 0;
}

int ClientSkt::sendEntry(void * args) {
    ((ClientSkt*)args)->messageSender();
    return 0;
}

void ClientSkt::messageSender() {
    while (clientSocket >= 0) {
        SDL_LockMutex(qlock);
        while(!pqout.empty()) {
            NetMessage * message = pqout.front();
            pqout.pop();
            SDL_UnlockMutex(qlock);
            send(clientSocket, message->getData(), message->getSize(), 0);
            SDL_Delay(2);
            delete(message);
            SDL_LockMutex(qlock);
        }
        SDL_UnlockMutex(qlock);
        SDL_Delay(1);
    }
    while(!pqout.empty()) {
        NetMessage * message = pqout.front();
        pqout.pop();
        delete message;
    }
}

void ClientSkt::sendMsg(NetMessage * message) {
    if (clientSocket < 0) {
        delete message;
        return;
    }
    SDL_LockMutex(qlock);
    pqout.push(message);
    SDL_UnlockMutex(qlock);
}

int ClientSkt::start() {
    /* get socket */
	addr.sin_family = AF_INET;
	clientSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (clientSocket < 0)
	{
		cout << "\nSocket Creation FAILED!";
		return 1;
    }

    /* resolve hostname and bind (open) socket */
    hostent * hn = gethostbyname(hostname);

    if (!hn || *((unsigned long*)hn->h_addr_list[0]) == 0) {
        cout << "Could not resolve hostname" << endl;
        if (clientSocket) closesocket(clientSocket);
        return 1;
    }

    cout << "hn = " << *((unsigned long*)hn->h_addr_list[0]) << endl;
    addr.sin_port = htons(port);
	addr.sin_addr.s_addr = *((unsigned long*)hn->h_addr_list[0]);
	if (connect(clientSocket,(struct sockaddr *)&addr,sizeof(addr))!=0){
		cout << "\nSocket Connection FAILED!\n" << endl;
		if (clientSocket) closesocket(clientSocket);
		return 1;
	}

    /* send packet */

	SDL_DetachThread(SDL_CreateThread(recvEntry, "recv", (void*)this));
    SDL_DetachThread(SDL_CreateThread(sendEntry, "send", (void*)this));

    return 0;
}

bool ClientSkt::stop() {
    closesocket(clientSocket);
    clientSocket = -1;
    return true;
}
