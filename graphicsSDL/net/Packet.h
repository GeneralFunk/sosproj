#ifndef _PACKET_H
#define _PACKET_H

#include <string.h>
#include "../common/jahutils.h"
class Server;

class Packet {
public:
    struct Read {
        Packet* packet;
        unsigned int size;

        Read(unsigned int _size) : size(_size) {}

        friend Read& operator>>(Packet& packet, Read& rhs) {
            rhs.packet = &packet;
            return rhs;
        }

        friend Packet& operator>>(Read& rs, char * str) {
            rs.packet->getToStr(str, rs.size);
            return *rs.packet;
        }

        friend Packet& operator>>(Read& rs, void * obj) {
            rs.packet->getToObj(obj, rs.size);
            return *rs.packet;
        }
    };

    Packet(char * buffer, size_t size);

    bool getBool();
    char getChar();
    int getInt();
    float getFloat();
    long long getLLong();
    double getDouble();
    unsigned char getUChar();
    unsigned int getUInt();
    unsigned long getULong();
    unsigned long long getULLong();
    char * getStr();
    void getToStr(char * str, unsigned int size);
    void * getObj(size_t);
    void getToObj(void * obj, unsigned int size);
    Packet& operator>>(bool& b);
    Packet& operator>>(char& c);
    Packet& operator>>(int& n);
    Packet& operator>>(float& f);
    Packet& operator>>(long long& ll);
    Packet& operator>>(double& d);
    Packet& operator>>(unsigned char& c);
    Packet& operator>>(unsigned int& n);
    Packet& operator>>(unsigned long long& ll);
    Packet& operator>>(char *& str);
    Packet& operator>>(const char *& str);

    const char * getConstStr();
    const void * getConstObj(size_t);

    bool error;
    const char * getData();
    void reset();
    int getType();
protected:
private:
    char * buffer;
    size_t size;
    size_t pos;
    int type;
};

#endif // _PACKET_H
