#define GLM_FORCE_RADIANS

//#define NO_SDL_GLEXT


#include <windows.h>
#include <gl/glew.h>
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <stdio.h>
#include <string>
#include <cmath>
#include <sstream>
#include <vector>
#include <list>
#include <fstream>

//#include <psapi.h>

#include <glm/glm.hpp>
//#include <glm/ext.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include "./include/Biomes.h"
#include "./include/handleTTF.h"
#include "./include/H2DTexture.h"
#include "./include/GridTree.h"
#include "./include/GameObject.h"
#include "./include/Tile.h"
#include "./include/Player.h"
#include "./include/Tree.h"
#include "./include/Inventory.h"
#include "include/Enemy.h"
#include "net/ClientSkt.h"
#include "net/NetMessage.h"
#include "net/Packet.h"
#include "net/net.h"
#include "Messages.h"

#define NO_USE_NET 0

using namespace std;
//Screen dimension constants
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 640;


//Starts up SDL and creates window
bool init();

//Loads media
bool loadMedia();

//Frees media and shuts down SDL
void close();

//The window we'll be rendering to
SDL_Window* gWindow = NULL;

SDL_GLContext glcontext;

//The window renderer
SDL_Renderer* gRenderer = NULL;

//Globally used font
TTF_Font *gFont = NULL;

ClientSkt * client;

bool quit = false;

void packetReceived(ClientSkt * ce, Packet * packet);

bool uToR=false;

std::vector<GameObject*> GO(1);
std::vector<GameObject*> mapTiles;
H2DTexture Person = H2DTexture();

vector<GridTree *> ChunkMatrix(9);
vector<GridTree *> tobeDelChunkMatrix(9);
H2DTexture Grass =  H2DTexture();
H2DTexture Slimer = H2DTexture();
//GridTree GT,GT2,GT3,GT4,GT5,GT6,GT7,GT8,GT9;
Player * P1;

std::string username;
std::string hostName;

SDL_mutex *tls_lock,*tls_lockPlayerList;
bool doneloading = false;
#if NO_USE_NET <= 0
void packetReceived(ClientSkt * ce, Packet * packet) {

    Player * setPl;
    PlayerJoin * playerjoin;
    PlayerLeave * playerLeave;
    LoginAck * loginack;
    MoveAt * moveat;
    switch (packet->getType()) {
    case MSG_LOGIN_ACK:
        loginack = new LoginAck(false);
        *packet >> *loginack;
        if (loginack->success) {
            cout << "Successful login." << endl;
        } else {
            cout << "Login failed." << endl;
        }
        delete loginack;
        break;
    case MSG_PLAYER_JOIN:
        playerjoin = new PlayerJoin();
        *packet >> *playerjoin;
        cout << '"' << playerjoin->name << "\" has joined the game." << endl;
        setPl = new Player(Person);
        setPl->Name = playerjoin->name;
        setPl->phys->posX = playerjoin->x;
        setPl->phys->posY = playerjoin->y;
        SDL_LockMutex(tls_lockPlayerList);
        GO.push_back(setPl);
        SDL_UnlockMutex(tls_lockPlayerList);
        delete playerjoin;
        break;
    case MSG_MOVE_AT:
        moveat = new MoveAt();
        *packet >> *moveat;
        cout << '"' << moveat->name << "\" has changed movement." << endl;
        for(unsigned int i = 1; i < GO.size();i++){
            if(((Player*)GO[i])->Name == moveat->name){
                GO[i]->phys->Direction=glm::vec3(moveat->x,moveat->y,moveat->z);//Set new Direction here;
            }
        }
        delete moveat;
        break;
    case MSG_PLAYER_LEAVE:
        playerLeave = new PlayerLeave();
        *packet >> *playerLeave;
        cout << '"' << playerLeave->name << "\" has left the game." << endl;
        for(unsigned int i =1; i < GO.size(); i++){
            Player * Hold = (Player*)GO[i];
            if(playerLeave->name == Hold->Name)
            {
                if(Hold->CurrentQ)
                    while(!uToR);
                SDL_LockMutex(tls_lockPlayerList);
                GO.erase(GO.begin()+i);
                delete Hold;
                SDL_UnlockMutex(tls_lockPlayerList);
                break;
            }
        }
        delete playerLeave;
    default:
        break;
    }
}
#endif



int hcount=0,wcount=0;
double sccount=0;
bool reverseDir=false;


bool init()
{
    //opengl code start:1/15/14
 SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
     SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

    SDL_GL_SetAttribute(SDL_GL_RED_SIZE,            8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE,          8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE,           8);
    SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE,          8);

    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE,          16);
    SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE,            32);

    SDL_GL_SetAttribute(SDL_GL_ACCUM_RED_SIZE,        8);
    SDL_GL_SetAttribute(SDL_GL_ACCUM_GREEN_SIZE,    8);
    SDL_GL_SetAttribute(SDL_GL_ACCUM_BLUE_SIZE,        8);
    SDL_GL_SetAttribute(SDL_GL_ACCUM_ALPHA_SIZE,    8);

    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS,  1);

    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES,  2);

    /*if((Surf_Display = SDL_SetVideoMode(640, 480, 32, SDL_HWSURFACE | SDL_GL_DOUBLEBUFFER | SDL_OPENGL)) == NULL) {
        return false;
    }*/



    //opengl code end.

	//Initialization flag
	bool success = true;

	//Initialize SDL
	if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
	{
		printf( "SDL could not initialize! SDL Error: %s\n", SDL_GetError() );
		success = false;
	}
	else
	{
		//Enable VSync
		if( !SDL_SetHint( SDL_HINT_RENDER_VSYNC, "1" ) )
		{
			printf( "Warning: VSync not enabled!" );
		}

		//Set texture filtering to linear
		if( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) )
		{
			printf( "Warning: Linear texture filtering not enabled!" );
		}

        client = NULL;
        #if (NO_USE_NET <= 0)
        startNet();
        std::ifstream hostFile;

        int port;
        hostFile.open("host", std::ifstream::in);
        hostFile >> hostName >> port;
        cout << "Connecting to " << hostName << ':' << port << endl;
        client = new ClientSkt(port, hostName.c_str(), 1024);
        client->setPacketCallback(packetReceived);
        #endif
		//Create window
		gWindow = SDL_CreateWindow( "SDL Graphics Test", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL );


//glewExperimental = GL_TRUE;
    //glewInit();
        glcontext = SDL_GL_CreateContext(gWindow);


        glClearColor(0, 0, 0, 0);
        glEnable (GL_DEPTH_TEST);
        glEnable(GL_STENCIL_TEST);
        glDepthMask(true);
        //glDepthFunc(GL_LESS);

        //glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
         glEnable (GL_BLEND);
         glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        //glBlendFunc(GL_ONE_MINUS_DST_ALPHA,GL_DST_ALPHA);
        //glBlendFunc (GL_ONE, GL_ONE);
        glViewport(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        glOrtho(0, SCREEN_WIDTH, SCREEN_HEIGHT,0, -SCREEN_WIDTH, SCREEN_WIDTH);

        glMatrixMode(GL_MODELVIEW);

        glEnable(GL_TEXTURE_2D);
        //glDepthRange(-SCREEN_WIDTH,SCREEN_WIDTH);

        glEnable(GL_ALPHA_TEST);
        glAlphaFunc(GL_GREATER,0.1f);

        glLoadIdentity();
        //opengl code end.

		if( gWindow == NULL )
		{
			printf( "Window could not be created! SDL Error: %s\n", SDL_GetError() );
			success = false;
		}
		else
		{
			//Create renderer for window
			gRenderer = SDL_CreateRenderer( gWindow, -1, SDL_RENDERER_ACCELERATED );
			if( gRenderer == NULL )
			{
				printf( "Renderer could not be created! SDL Error: %s\n", SDL_GetError() );
				success = false;
			}
			else
			{
				//Initialize renderer color
				SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );

				//Initialize PNG loading
				int imgFlags = IMG_INIT_PNG;
				if( !( IMG_Init( imgFlags ) & imgFlags ) )
				{
					printf( "SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError() );
					success = false;
				}

				 //Initialize SDL_ttf
				if( TTF_Init() == -1 )
				{
					printf( "SDL_ttf could not initialize! SDL_ttf Error: %s\n", TTF_GetError() );
					success = false;
				}
			}
		}
	}

	return success;
}

bool loadMedia()
{
	//Loading success flag
	bool success = true;

	//Open the font
	gFont = TTF_OpenFont( "./content/lazy.ttf", 18 );


	return success;
}

void close()
{
	//Free global font
	TTF_CloseFont( gFont );
	gFont = NULL;

	//Destroy window
	SDL_DestroyRenderer( gRenderer );
	SDL_DestroyWindow( gWindow );
	gWindow = NULL;
	gRenderer = NULL;

	//Quit SDL subsystems
	TTF_Quit();
	IMG_Quit();
	SDL_Quit();
}


std::string Convert (float number){
    std::ostringstream buff;
    buff<<(int)number;
    return buff.str();
}
int mouX=0, mouY=0;
struct UpdateParams
{
    int * DiagSize;
    glm::vec4 * mousy;
    glm::vec3 *Eye;
    glm::vec3 *lasteye;
    glm::vec3 *Target;
    glm::vec3 *UP;
    glm::mat4 *View;
    handleTTF *httf;
    Tree *Atree;
    Inventory *P1inv;
    GameObject *Cursor;
};

Uint32 Ucurtime=0;


int Update(void * Data)
{
    UpdateParams &UpDa = *((UpdateParams*)(Data));

        bool mouseRelease;
        Player * P1= (Player*)GO[0];
        glm::vec3 SecondEye = *UpDa.Eye;
        float TickLimit = 1000,TickLimit2=1000;
        Uint32 CurTime = SDL_GetTicks(),CurTime2 = SDL_GetTicks();
//        bool togFS=true;
    while(!quit)
    {
        if((SDL_GetTicks()-CurTime) > 0)
        {
            TickLimit = (float)(SDL_GetTicks()-CurTime);
        }
        if((SDL_GetTicks()-CurTime2) > 0)
        {
            TickLimit2 = 1000/(float)(SDL_GetTicks()-CurTime2);
        }

        if(TickLimit > 10)
        {
            if(P1->Move(SecondEye)){
                MoveAt MA;
                MA.setName(P1->Name.c_str());
                MA.x = P1->phys->Direction.x;
                MA.y = P1->phys->Direction.y;
                MA.z = 0;
                NetMessage * NM = new NetMessage(MSG_MOVE_AT,1024);
                *NM << MA;
                client->sendMsg(NM);
                CurTime = SDL_GetTicks();
            }
        }

        if(TickLimit2 < 10)
        {
SDL_LockMutex(tls_lock);//l
            SDL_GetMouseState( &mouX, &mouY );
                CurTime2=SDL_GetTicks();
SDL_UnlockMutex(tls_lock);//u

                if(mouseRelease)
                {
                        /*if(togFS)
                        {
                            SDL_DisplayMode mode;
                            SDL_SetWindowFullscreen(gWindow,SDL_WINDOW_FULLSCREEN);
                            SDL_GetWindowDisplayMode(gWindow,&mode);
                            //SDL_SetWindowDisplayMode(gWindow,&mode);
                            glViewport(0, 0, mode.w, mode.h);

                            //SCREEN_WIDTH = mode.w;
                            //SCREEN_HEIGHT = mode.h;
                        }
                        else
                        {
                            SDL_DisplayMode mode;
                            SDL_SetWindowFullscreen(gWindow,0);
                            SDL_GetWindowDisplayMode(gWindow,&mode);
                            //SDL_SetWindowDisplayMode(gWindow,&mode);
                            glViewport(0, 0, mode.w, mode.h);

                            //SCREEN_WIDTH = mode.w;
                            //SCREEN_HEIGHT = mode.h;
                        }
                        togFS = !togFS;*/
                }
        }




        SDL_LockMutex(tls_lock);
        bool check = uToR;
        SDL_UnlockMutex(tls_lock);
        if(check)
        {
            H2DTexture::PlayerPos = glm::vec4(P1->phys->posX,P1->phys->posY,0,0);
            //SDL_LockMutex(tls_lock);//l
            *UpDa.Eye = SecondEye;
//SDL_UnlockMutex(tls_lock);//
            SDL_LockMutex(tls_lockPlayerList);
            for (unsigned int i = 0; i < GO.size(); i++)
            {
                    //Shares GO
                    GO[i]->Update();//tls_lock);
                    if(i>=1 && GO[i]!=NULL){
                            if(GO[i]->CurrentQ)
                                GO[i]->phys->moveTween(GO[i]);
                        GridTree * CGT = ((GridTree*)(GO[0]->CurrentQ->gtnum));
                        GO[i]->KeepWithin(GO[0]->phys->posX,GO[0]->phys->posY,CGT->mapArea.w,GO[0]->CurrentQ);

                    }
            }
            SDL_UnlockMutex(tls_lockPlayerList);
            //P1->Update();
            //Completely Shared. Updates Graphics. Ideal to have this be the only thing shared with graphics.
            P1->CurrentQ->UpdateQuadrants(*UpDa.DiagSize);//,tls_lock);
            //SDL_LockMutex(tls_lock);
            uToR=!uToR;
            Ucurtime = SDL_GetTicks();
           // SecondEye = *UpDa.Eye;
            //SDL_UnlockMutex(tls_lock);
        }
    }
    return 1;
}

SDL_mutex * GenLock;
std::vector<H2DTexture> ML;
int Generation(void * Data)
{
    GridTree * CGT = ((GridTree*)(GO[0]->CurrentQ->gtnum));
    while(!quit)
    {
        SDL_Delay(10);
        if(CGT !=((GridTree*)(GO[0]->CurrentQ->gtnum)))
        {
            CGT = ((GridTree*)(GO[0]->CurrentQ->gtnum));
     //       SDL_LockMutex(GenLock);
            CGT->GenerateSurroundingChunks(ChunkMatrix,ML);
       //     SDL_UnlockMutex(GenLock);
        }
    }
}

int Render(void * Data)
{
        string theStr="";
        int * DiagSize=((UpdateParams*)Data)->DiagSize;
        glm::mat4 View= glm::mat4(1.0f);
        handleTTF *hTTF=((UpdateParams*)Data)->httf;
        glm::vec3 *Eye=((UpdateParams*)Data)->Eye;
        glm::vec3 SecondEye;
        glm::vec3 Target=glm::vec3(0,0,0);
        glm::vec3 *UP=((UpdateParams*)Data)->UP;
        glm::vec4 *mousy=((UpdateParams*)Data)->mousy;
        Inventory *P1inv=((UpdateParams*)Data)->P1inv;
        GameObject * Cursor=((UpdateParams*)Data)->Cursor;

        vector<float> avgFPS = vector<float>(1000,0);
        vector<float> avguFPS = vector<float>(1000,0);
        int afcount=0;
        float deltax=0,deltay=0;
        float cameraRot= -0.78539816339;
        Uint32 CurTime = SDL_GetTicks();

        SDL_GL_MakeCurrent( gWindow,glcontext);
        string inittt = glewInit() == GLEW_OK ? "Yes" : "No";
        cout << "init: " << inittt  << endl;

        cout << "Version: " << glGetString(GL_VERSION) << endl;
        cout << ((glewIsSupported("GL_VERSION_2_0  GL_ARB_point_sprite")) ? "Yes" : "No") << endl;


        cout << "3..";

        for(int i = 0;i < 9; i++)
        {
            ChunkMatrix[i]->initObjectsVao();
            cout << "2." << 9-i << "..";
        }
        cout << "2..1..";

        H2DTexture::PlayerPos = glm::vec4(P1->phys->posX,P1->phys->posY,0,0);
        cout << "0\n";
        float FPS=0;

        std::vector<GLuint> toErase;
        while(!quit)
        {
          /*  if(H2DTexture::VaosToDeletes.size()){
                SDL_LockMutex(GenLock);
                cout << "Del: " << H2DTexture::VaosToDeletes.size() << " ";
                glDeleteVertexArrays(H2DTexture::VaosToDeletes.size(),&H2DTexture::VaosToDeletes[0]);
                H2DTexture::VaosToDeletes.clear();
                cout << "vao: " << " Done ";
                cout << "Del: " << H2DTexture::VbosToDeletes.size() << " ";
                glDeleteBuffers(H2DTexture::VbosToDeletes.size(),&H2DTexture::VbosToDeletes[0]);
                H2DTexture::VbosToDeletes.clear();
                cout << "vbo: " << " Done ";
                cout << "L: " << H2DTexture::VaosToDeletes.size() << "\n";
                SDL_UnlockMutex(GenLock);
            }*/
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            glLoadIdentity();



            SDL_LockMutex(tls_lock);
            bool check = !uToR;
            SDL_UnlockMutex(tls_lock);
            float FPScheck=0;

            for(unsigned int i =0; i< avgFPS.size(); i++)
            {
                FPScheck +=avgFPS[i];
            }
            FPScheck/=avgFPS.size();
           // avgFPS[afcount]--;
            if(SDL_GetTicks()-CurTime < 15){
                Uint32 delayit = 16-(SDL_GetTicks()-CurTime);//1000/(FPS-60);
                SDL_Delay(delayit);
            }
            if(check)
            {

                if((SDL_GetTicks()-CurTime) > 0)
                {
                FPS =(SDL_GetTicks()-CurTime);
                FPS = 1000/FPS;
                }

                //GridTree * CGT = ((GridTree*)(GO[0]->CurrentQ->gtnum));
                //CGT->GenerateSurroundingChunks(ChunkMatrix,Grass);
                SDL_LockMutex(tls_lock);
                SecondEye = *Eye;
                SDL_UnlockMutex(tls_lock);
                deltax = SecondEye.z*cos(cameraRot);
                deltay = -SecondEye.z*sin(cameraRot);
                Target.x = SecondEye.x-deltax;//*abs(sin(sccount));
                Target.y = SecondEye.y-deltay;//*abs(cos(sccount));
                View =glm::lookAt(SecondEye,Target,*UP);
                View = glm::translate(glm::mat4(1.0f),glm::vec3(SCREEN_WIDTH/2,SCREEN_HEIGHT/2,0))* View;
            SDL_LockMutex(tls_lock);//lockMutex
                SDL_GetMouseState( &mouX, &mouY );
                mousy->x = mouX-SCREEN_WIDTH/2;//+Eye.x;
                mousy->y = mouY-SCREEN_HEIGHT/2;//+Eye.y;
                mousy->z = SecondEye.z;
                *mousy = *mousy * View;
            //Shares Cursor as a GameObject
                Cursor->phys->posX = mousy->x-cos(-0.78539816339)*mousy->z+SecondEye.x;
                Cursor->phys->posY = mousy->y-cos(-0.78539816339)*mousy->z+SecondEye.y;
                Cursor->Pic->setPosition(Cursor->phys->posX,Cursor->phys->posY);
            SDL_UnlockMutex(tls_lock);//lockMutex
                GO[0]->CurrentQ->Render(*DiagSize,View);

//            SDL_LockMutex(tls_lock);
                theStr = (P1->Name);
                hTTF->doCenter = true;
                hTTF->setPos(P1->Pic->mDisplay->x+P1->Pic->mDisplay->w,P1->Pic->mDisplay->y+P1->Pic->mDisplay->h+20);
                hTTF->render(theStr,View);

                theStr = "Players: ";
                theStr += Convert(GO.size());

                hTTF->doCenter = true;
                hTTF->setPos(360*sin(sccount),360*cos(sccount));
                hTTF->render(theStr,View);

                hTTF->doCenter = false;
                hTTF->setPos(10,10);
                hTTF->render(theStr);

                theStr = "isoX ";
                theStr += Convert(cos(0.78539816339)*SecondEye.x - sin(0.78539816339)*SecondEye.y);
                theStr += " : isoY ";
                theStr += Convert(sin(0.78539816339)*SecondEye.x + cos(0.78539816339)*SecondEye.y);
                theStr += " AbsX: ";
                theStr += Convert(SecondEye.x);
                theStr += " AbsY: ";
                theStr += Convert(SecondEye.y);
                hTTF->setPos(10,30);
                hTTF->render(theStr);
                theStr = "Quadrants Displayed: ";
                theStr += Convert((2**DiagSize+1)*(2**DiagSize+1));
                //theStr += " Memory Usage: ";
                //theStr += Convert(memInfo.ullTotalPhys);
                hTTF->setPos(10,50);
                hTTF->render(theStr);
                theStr = "Qpos: ";
                theStr += Convert((GO[0]->CurrentQ->qPosX));
                theStr += ":";
                theStr += Convert((GO[0]->CurrentQ->qPosY));
                theStr += " Chunk:";
                theStr += Convert((int)(GO[0]->CurrentQ->gtnum));
                hTTF->setPos(10,70);
                hTTF->render(theStr);
                theStr = "FPS: ";
                float uFPS;
                avgFPS[afcount]=FPS;
                uFPS=0;
                for(unsigned int i =0; i< avgFPS.size(); i++)
                {
                    uFPS +=avgFPS[i];
                }
                uFPS/=avgFPS.size();

                theStr += Convert(uFPS);
                hTTF->setPos(10,90);
                hTTF->render(theStr);
                theStr = " uFPS:";
                //SDL_LockMutex(tls_lock);//lockMutex
                uFPS = SDL_GetTicks()-Ucurtime;
                //SDL_UnlockMutex(tls_lock);//lockMutex
                if(uFPS > 0)
                    uFPS = 1000/uFPS;
                avguFPS[afcount]=uFPS;

                uFPS=0;
                for(unsigned int i =0; i< avguFPS.size(); i++)
                {
                    uFPS +=avguFPS[i];
                }
                uFPS/=avguFPS.size();
                theStr += Convert(uFPS);
                CurTime = SDL_GetTicks();
                 hTTF->setPos(90,90);
                hTTF->render(theStr);
                theStr = "Z ";
                theStr += Convert(mousy->z);
                hTTF->setPos(mousy->x-cos(-0.78539816339)*mousy->z+SecondEye.x,mousy->y-cos(-0.78539816339)*mousy->z+SecondEye.y);
                hTTF->render(theStr,View);
                theStr = "mouX ";
                theStr += Convert(mousy->x);
                theStr += " : mouY ";
                theStr += Convert(mousy->y);
                theStr += " : mouZ ";
                theStr += Convert(mousy->z);
                 hTTF->setPos(10,110);
                hTTF->render(theStr);

                theStr = "Inventory Empty";
                hTTF->setPos(10,130);
                if(P1inv->thisManyItems > 0)
                {
                    theStr.clear();
                    for(int i = 0; i < P1inv->thisManyItems; i++)
                    {
                        theStr = P1inv->PrintInv(i);
                        hTTF->setPos(10,130+i*20);
                        hTTF->render(theStr);
                        theStr.clear();
                    }
                }
                else
                {
                    hTTF->render(theStr);
                }

                theStr.clear();

                SDL_GL_SwapWindow(gWindow);

                SDL_LockMutex(tls_lock);
                uToR = !uToR;
SDL_UnlockMutex(tls_lock);//u
            }
            afcount = (afcount+1)%avguFPS.size();
        }

    return 1;
}

/*void getLocalPath(const char * arg0, char ** str) {
    int len;
    for (len = 0; arg0[len]; len++);
    *str = new char[len+1];
    for (int i = 0; i < len; i++) {
        (*str)[i] = arg0[i];
    }
    int end;
    for (end = len-1; !((*str)[end] == '\\' || (*str)[end] == '/'); end--);
    end++;
    (*str)[end] = 0;
}*/

void setName() {
    ifstream nfs;
    nfs.open("username", std::ifstream::in);
    nfs >> username;
}

int main( int argc, char* args[] )
{
/*char *Path;
getLocalPath(args[0],&Path);
//InitShader::WorkingPath = std::string(Path);
ShaderProgram::WorkingPath = std::string(Path);
delete Path;*/
setName();
    int denominator = 3;
    for(int i=0; i < 9; i++)
    {
        ChunkMatrix[i]=new GridTree();
    }
    ChunkMatrix[4]->init(-500,-500,SCREEN_WIDTH*denominator,denominator,4);
    ChunkMatrix[3]->init(ChunkMatrix[4]->mapArea.x-ChunkMatrix[4]->mapArea.w,ChunkMatrix[4]->mapArea.y,ChunkMatrix[4]->mapArea.w,denominator,3);
    ChunkMatrix[5]->init(ChunkMatrix[4]->mapArea.x+ChunkMatrix[4]->mapArea.w,ChunkMatrix[4]->mapArea.y,ChunkMatrix[4]->mapArea.w,denominator,5);
    ChunkMatrix[1]->init(ChunkMatrix[4]->mapArea.x,ChunkMatrix[4]->mapArea.y-ChunkMatrix[4]->mapArea.h,ChunkMatrix[4]->mapArea.w,denominator,1);
    ChunkMatrix[7]->init(ChunkMatrix[4]->mapArea.x,ChunkMatrix[4]->mapArea.y+ChunkMatrix[4]->mapArea.h,ChunkMatrix[4]->mapArea.w,denominator,7);
    ChunkMatrix[0]->init(ChunkMatrix[3]->mapArea.x,ChunkMatrix[1]->mapArea.y,ChunkMatrix[4]->mapArea.w,denominator,0);
    ChunkMatrix[2]->init(ChunkMatrix[5]->mapArea.x,ChunkMatrix[1]->mapArea.y,ChunkMatrix[4]->mapArea.w,denominator,2);
    ChunkMatrix[6]->init(ChunkMatrix[3]->mapArea.x,ChunkMatrix[7]->mapArea.y,ChunkMatrix[4]->mapArea.w,denominator,6);
    ChunkMatrix[8]->init(ChunkMatrix[5]->mapArea.x,ChunkMatrix[7]->mapArea.y,ChunkMatrix[4]->mapArea.w,denominator,8);

    int dimension = ChunkMatrix[4]->mapArea.w/denominator;
    int perD = dimension/16;
	mapTiles = std::vector<GameObject*>();
	std::vector<GameObject*> mapGenTiles((ChunkMatrix[4]->mapArea.w/perD)*(ChunkMatrix[4]->mapArea.w/perD));

    Inventory P1inv = Inventory();
    tls_lock = SDL_CreateMutex();
    tls_lockPlayerList = SDL_CreateMutex();
    GenLock = SDL_CreateMutex();
	glm::vec3 &UP = *(new glm::vec3(0,0,-1));
	glm::vec3 &Eye = *(new glm::vec3(0,0,1));
	glm::vec3 lasteye = Eye;
	glm::vec3 &Target = *(new glm::vec3(0,0,0));
	glm::vec4 &mousy = *(new glm::vec4(0,0,0,0));
	glm::mat4 &View = *(new glm::mat4(1.0f));
    mouX=0; mouY=0;

	if( !init() )
	{
		printf( "Failed to initialize!\n" );
	}



    SDL_ShowCursor(0);
    int &DiagSize = *(new int);
    DiagSize = 1;

	handleTTF hTTF = handleTTF();
	hTTF.init(gRenderer);
    hTTF.loadMedia("./content/lazy.ttf",18);
    std::string theStr = "Wiki Wiki Waa Waa!";

    Person.masterTexture = true;
	Person.init(gRenderer);
    Person.loadFromFile("./content/basic.png");
    Person.setupAnimation(SDL_Rect()={0,0,42,50},20,30);
    Person.setPosition(( SCREEN_WIDTH) / 2 , ( SCREEN_HEIGHT  ) / 2 );
    Person.setDisplay({0,0,42,50});
    Person.depth = 0.2f;
    Person.renderType = 2;


    Grass.masterTexture = true;
	Grass.init(gRenderer);
    Grass.loadFromFile("./content/basic_tileset.png");
    Grass.setupAnimation(SDL_Rect()={0,0,64,64},0,0);
    Grass.setPosition(( SCREEN_WIDTH) / 2 , ( SCREEN_HEIGHT  ) / 2 );
    Grass.setDisplay({0,0,perD,perD});
    Grass.renderType = 1;

    Slimer.masterTexture = true;
	Slimer.init(gRenderer);
    Slimer.loadFromFile("./content/slime.png");
    Slimer.setupAnimation(SDL_Rect()={0,0,64,64},12,3);
    Slimer.setPosition(( SCREEN_WIDTH) / 2 , ( SCREEN_HEIGHT  ) / 2 );
    Slimer.setDisplay({0,0,perD,perD});
    Slimer.renderType = 1;

    H2DTexture picTree;
    picTree.masterTexture = true;
	picTree.init(gRenderer);
    picTree.loadFromFile("./content/xtreeset.png");
    picTree.setupAnimation(SDL_Rect()={0,0,256,256},0,0);
    picTree.setPosition(( SCREEN_WIDTH) / 2 , ( SCREEN_HEIGHT  ) / 2 );
    picTree.setDisplay({0,0,256,256});

    H2DTexture picCursor;
    picCursor.masterTexture = true;
	picCursor.init(gRenderer);
    picCursor.loadFromFile("./content/Cursor.png");
    picCursor.setupAnimation(SDL_Rect()={0,0,18,21},0,0);
    picCursor.setPosition(( SCREEN_WIDTH) / 2 , ( SCREEN_HEIGHT  ) / 2 );
    picCursor.setDisplay({0,0,18,21});


    H2DTexture h2dt;
    h2dt.masterTexture = true;
	h2dt.init(gRenderer);
    h2dt.loadFromFile("./content/sexy house.png");
    h2dt.setupAnimation(SDL_Rect()={0,0,320,320},0,0);
    h2dt.setPosition(( SCREEN_WIDTH) / 2 , ( SCREEN_HEIGHT  ) / 2 );
    h2dt.setDisplay({0,0,320,320});
    h2dt.depth = 1.0f;
    h2dt.renderType = 1;

    int ii = ChunkMatrix[4]->mapArea.w/perD;

    P1 = new Player(Person);
    //P1->phys->Collidable = true;

    P1->Name = "Me";
    Player::PlayerInv = &P1inv;
    GO[0] = P1;
    ChunkMatrix[4]->Sort((GO[0]));

    Biomes B = Biomes();
    Biomes::ii = ii; Biomes::perD = perD;
    for(int i = 1; i < 3; i++)
    {
        Tile * A = new Tile(h2dt);
        A->phys->Collidable = true;
        A->typecast = "House";
        A->phys->posX = -(8*64)+((16*64)*(i%2));
        A->phys->posY = -(8*64)+((16*64)*(i%2));
        A->Pic->setPosition(A->phys->posX,A->phys->posY);
        A->Pic->setupAnimation(SDL_Rect()={0,0,320,320},0,0);
        A->Pic->depth = 0.2f;
        A->phys->collideResponse=0;
        A->BBtoDSPLY();
        ChunkMatrix[4]->Sort(A);
    }

    ML.push_back(Grass);
    ML.push_back(picTree);
    ML.push_back(Slimer);
    B.GenerateChunkMap(ChunkMatrix[4],mapGenTiles,ML);

    B.GenerateChunkMap(ChunkMatrix[0],mapGenTiles,ML);

    B.GenerateChunkMap(ChunkMatrix[1],mapGenTiles,ML);

    B.GenerateChunkMap(ChunkMatrix[2],mapGenTiles,ML);

    B.GenerateChunkMap(ChunkMatrix[3],mapGenTiles,ML);

    B.GenerateChunkMap(ChunkMatrix[5],mapGenTiles,ML);

    B.GenerateChunkMap(ChunkMatrix[6],mapGenTiles,ML);

    B.GenerateChunkMap(ChunkMatrix[7],mapGenTiles,ML);

    B.GenerateChunkMap(ChunkMatrix[8],mapGenTiles,ML);

    Tree * Atree = new Tree(picTree);
    Atree->isStatic = false;
    Atree->phys->posX = 1000;
    Atree->phys->posY = 1000;
    Atree->phys->Collidable = true;
    Atree->Pic->setPosition(Atree->phys->posX,Atree->phys->posY);
    Atree->Pic->depth = GO[0]->Pic->depth;
    Atree->BBtoDSPLY();
    Atree->Pic->renderType = 1;
    ChunkMatrix[4]->Sort(Atree);

    GameObject * Cursor = new GameObject(picCursor,"cursor");
    Cursor->isStatic = false;
    Cursor->phys->posX = 0;
    Cursor->phys->posY = 0;
    Cursor->Pic->setPosition(Cursor->phys->posX,Cursor->phys->posY);
    Cursor->BBtoDSPLY();
    Cursor->phys->Collidable = true;
    Cursor->Pic->depth = GO[0]->Pic->depth;
    Cursor->Pic->renderType = 1;
    ChunkMatrix[4]->Sort(Cursor);

    doneloading = true;

    GO[0]->phys->posX = 0;
    GO[0]->phys->posY = 0;
    ChunkMatrix[4]->Stitch(ChunkMatrix[1]);
    ChunkMatrix[4]->Stitch(ChunkMatrix[3]);
    ChunkMatrix[4]->Stitch(ChunkMatrix[5]);
    ChunkMatrix[4]->Stitch(ChunkMatrix[7]);

    ChunkMatrix[1]->Stitch(ChunkMatrix[0]);
    ChunkMatrix[1]->Stitch(ChunkMatrix[2]);

    ChunkMatrix[3]->Stitch(ChunkMatrix[0]);
    ChunkMatrix[3]->Stitch(ChunkMatrix[6]);

    ChunkMatrix[5]->Stitch(ChunkMatrix[2]);
    ChunkMatrix[5]->Stitch(ChunkMatrix[8]);

    ChunkMatrix[7]->Stitch(ChunkMatrix[6]);
    ChunkMatrix[7]->Stitch(ChunkMatrix[8]);

    SDL_Event e;
    UpdateParams UpDa;
    UpDa.DiagSize=&DiagSize;
    UpDa.mousy=&mousy;
    UpDa.Eye=&Eye;
    UpDa.lasteye=&lasteye;
    UpDa.Target=&Target;
    UpDa.UP=&UP;
    UpDa.View=&View;
    UpDa.Atree=Atree;
    UpDa.P1inv=&P1inv;
    UpDa.Cursor=Cursor;
    UpDa.httf = &hTTF;


    #if NO_USE_NET <= 0
    if(client->start() == 0)
    {
        cout << "Successfully connected to server." << endl;
        NetMessage * nmsg = new NetMessage(MSG_LOGIN, 1024);
        cout << "Attempting login as " << username << endl;
        *nmsg << Login(username.c_str(), "doesn't matter");
        client->sendMsg(nmsg);

    } else {
        cout << "Failed to connect to server." << endl;
    }
    #endif


    //JAH::CreateThread(Update,"Update",(void *)&UpDa);
    SDL_DetachThread(SDL_CreateThread(Update,"Update",(void*)&UpDa));


    SDL_GL_MakeCurrent(gWindow, NULL);
    //JAH::CreateThread(Render, "Render",(void*)&UpDa);
    SDL_DetachThread(SDL_CreateThread(Render, "Render",(void*)&UpDa));


    SDL_DetachThread(SDL_CreateThread(Generation, "Generate",(void*)0));

    Uint32 Timer = SDL_GetTicks(),change=0;
    while( !quit )
    {
        //User Input for movement in Player.cpp
        //Handle events on queue
        while( SDL_PollEvent( &e ) != 0 )
        {
            //User requests quit
            if( e.type == SDL_QUIT )
            {
                quit = true;
            }
            else if( e.type == SDL_KEYDOWN )
            {
                switch( e.key.keysym.sym )
                {
                    case SDLK_ESCAPE:
                        quit = true;
                    break;

                    case SDLK_q:
                        DiagSize--;
                        if(DiagSize < 0)
                        {
                            DiagSize = 0;
                        }
                    break;

                    case SDLK_o:

                    break;

                    case SDLK_e:
                    DiagSize++;
                    break;

                }
            }
        }

        if(change)
            sccount= (sccount +(double)(change-SDL_GetTicks())/1000000000);
        else
            sccount= (sccount +(double)(SDL_GetTicks()-Timer)/1000000000);

        if(sccount >= 2*3.14159265)
        {
            Timer = SDL_GetTicks();
            change = 0;
            sccount=0;
            reverseDir = !reverseDir;
        }
        else if(sccount >= 3.14159265 && change ==0)
        {
            change = SDL_GetTicks()-Timer+SDL_GetTicks();
        }
        if(reverseDir)
        {
            hcount = (int)(100*cos(2*3.14159265-sccount));
            wcount = (int)(100*sin(2*3.14159265-sccount));
        }
        else
        {
            hcount = (int)(100*cos(sccount));
            wcount = (int)(100*sin(sccount));
        }

    }
    #if NO_USE_NET <= 0
    client->stop();
    closeNet();
    #endif

    SDL_Delay(100);

    for(unsigned int i = 0; i < mapTiles.size(); i++)
    {
        delete mapTiles[i];
    }
    mapTiles.clear();
    mapGenTiles.clear();
    for(unsigned int i = 0; i < GO.size(); i++)
    {
        delete GO[i];
    }
    GO.clear();

    hTTF.close();
	//Free resources and close SDL
	close();
	return 0;
}
