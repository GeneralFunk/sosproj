#ifndef _JAHUTILS_H
#define _JAHUTILS_H
#include <climits>
#include <ctime>
#include <cfloat>
#include <cstdlib>

#ifndef UINT_MIN
#define UINT_MIN 0
#endif

int /**<  */sstrlen(const char * str, int size);
int intRange(int min, int max);
unsigned int uintRange(unsigned int min, int max);

#ifndef __RANGES
#define __RANGES
template <typename T>
class RandRange {
public:
    RandRange(T min, T max, T& val) {
        minmax(min, max);
        this->val = val;
    }

    RandRange& value(T& val) {
        this->val = val;
    }

    void setVal(T val) {
        this->val = val;
    }

    RandRange& minmax(T min, T max) {
        if (min <= max) {
            this->min = min;
            this->max = max;
        } else {
            this->max = min;
            this->min = max;
        }
    }

    T getMin();
    T getMax();
private:
    T min;
    T max;
    T& val;
};
#endif

class GRand {
public:
    GRand() {}
    virtual bool nextBool() = 0;
    virtual unsigned int nextUInt() = 0;
    virtual int nextInt() = 0;
    virtual long long nextLLong() = 0;
    virtual unsigned long long nextULLong() = 0;
    virtual float nextFloat() = 0;
    virtual double nextDouble() = 0;

    GRand& operator>>(bool &x);
    GRand& operator>>(unsigned int &x);
    GRand& operator>>(int &x);
    GRand& operator>>(long long &x);
    GRand& operator>>(unsigned long long &x);
    GRand& operator>>(float &x);
    GRand& operator>>(double &x);

    ~GRand() {}
};


class STDRand : public GRand {
public:
    STDRand() : GRand() {
    }

    bool nextBool();
    unsigned int nextUInt();
    int nextInt();
    long long nextLLong();
    unsigned long long nextULLong();
    float nextFloat();
    double nextDouble();


    static void seed(unsigned int seed);

    static void seedTime();

private:
};


class JAHRand : public GRand {
public:
    JAHRand(unsigned int seed) : GRand() {
        this->seed = seed;

        larry = 348927 * ((seed>>0 ) & 0xff);
        curly = 893489 * ((seed>>8 ) & 0xff);
        moe   = 492879 * ((seed>>16) & 0xff);
        shemp = 734852 * ((seed>>24)  & 0xff);
    }


    bool nextBool();
    unsigned int nextUInt();
    int nextInt();
    long long nextLLong();
    unsigned long long nextULLong();
    float nextFloat();
    double nextDouble();


    int getSeed() {
        return seed;
    }

private:
    int seed;
    int larry, curly, moe, shemp;
    void doRandom();
};

#endif // _JAHUTILS_H
