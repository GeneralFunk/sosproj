#include "jahutils.h"

int sstrlen(const char * str, int size) {
    if (!str) return -1;
    for (int i = 0; i < size; i++) {
        if (*(str+i) == '\0') return i;
    }
    return -1;
}

bool STDRand::nextBool() {
    return rand()&1;
}

unsigned int STDRand::nextUInt() {
    return rand();
}

int STDRand::nextInt() {
    int r = nextUInt();
    return r;
}

long long STDRand::nextLLong() {
    long long r = nextUInt();
    r <<= 32;
    r |= nextUInt();
    return r;
}

unsigned long long STDRand::nextULLong() {
    unsigned long long r = nextUInt();
    r <<= 32;
    r |= nextUInt();
    return r;
}

float STDRand::nextFloat() {
    return (float)nextUInt() / (float)UINT_MAX;
}

double STDRand::nextDouble() {
    return (double)nextULLong() / (double)ULLONG_MAX;
}


void STDRand::seed(unsigned int seed) {
    srand(seed);
}

void STDRand::seedTime() {
    srand(time(NULL));
}



GRand& GRand::operator>>(bool &x) {
    x = nextBool();
    return *this;
}

GRand& GRand::operator>>(unsigned int &x) {
    x = nextUInt();
    return *this;
}

GRand& GRand::operator>>(int &x) {
    x = nextInt();
    return *this;
}

GRand& GRand::operator>>(long long &x) {
    x = nextLLong();
    return *this;
}

GRand& GRand::operator>>(unsigned long long &x) {
    x = nextULLong();
    return *this;
}

GRand& GRand::operator>>(float &x) {
    x = nextFloat();
    return *this;
}

GRand& GRand::operator>>(double &x) {
    x = nextDouble();
    return *this;
}








