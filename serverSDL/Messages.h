#ifndef __NET_MESSAGES__
#define __NET_MESSAGES__

#include "Net/Packet.h"
#include "Net/NetMessage.h"

#define MSG_MOVE_AT     100
#define MSG_LOGIN       400
#define MSG_LOGIN_ACK   401
#define MSG_PLAYER_JOIN 410

struct MoveAt {
    unsigned int id;
    float x;
    float y;
    float z;
    char * name;

    ~MoveAt() {
        delete name;
    }

    MoveAt(unsigned int _id, float _x, float _y, float _z) :
        id(_id), x(_x), y(_y), z(_z), name(NULL) {}
    MoveAt(unsigned int _id, const char * _name, float _x, float _y, float _z) :
        id(_id), x(_x), y(_y), z(_z) {
        unsigned int len = strlen(name);
        name = new char[len];
        memcpy(name, _name, len);
    }
    MoveAt() : id(0), x(0.0f), y(0.0f), z(0.0f), name(NULL) {}

    void setName(const char * name) {
        delete this->name;
        int len = strlen(name)+1;
        this->name = new char[len];
        for (int i = 0; i < len; i++) {
            this->name[i] = name[i];
        }
    }

    friend Packet& operator>>(Packet& packet, MoveAt& rhs) {
        packet >> rhs.x >> rhs.y >> rhs.z >> rhs.name;
        return packet;
    }

    friend NetMessage& operator<<(NetMessage& message, const MoveAt& rhs) {
        message << rhs.x << rhs.y << rhs.z << rhs.name;
        return message;
    }
};

struct Login {
    char * name;
    char * pass;

    ~Login() {
        delete name;
        delete pass;
    }

    Login() : name(NULL), pass(NULL) {}

    Login(const char * _name, const char * _pass) {
        int len = strlen(_name);
        name = new char[len];
        memcpy(name, _name, len);
        len = strlen(_pass);
        pass = new char[len];
        memcpy(pass, _pass, len);
    }

    friend Packet& operator>>(Packet& packet, Login& rhs) {
        packet>>rhs.name>>rhs.pass;
        return packet;
    }

    friend NetMessage& operator<<(NetMessage& message, const Login& rhs) {
        message<<rhs.name<<rhs.pass;
        return message;
    }
};

struct LoginAck {
    bool success;

    LoginAck(bool _success) : success(_success) {}

    friend Packet& operator>>(Packet& packet, LoginAck& rhs) {
        packet>>rhs.success;
        return packet;
    }

    friend NetMessage& operator<<(NetMessage& message, const LoginAck& rhs) {
        message<<rhs.success;
        return message;
    }
};

struct PlayerJoin {
    unsigned int id;
    float x;
    float y;
    float z;
    char * name;

    ~PlayerJoin() {
        delete name;
    }

    PlayerJoin(unsigned int _id, float _x, float _y, float _z) :
        id(_id), x(_x), y(_y), z(_z), name(NULL) {}
    PlayerJoin(unsigned int _id, const char * _name, float _x, float _y, float _z) :
        id(_id), x(_x), y(_y), z(_z) {
        unsigned int len = strlen(_name);
        name = new char[len];
        memcpy(name, _name, len);
    }
    PlayerJoin() : id(0), x(0.0f), y(0.0f), z(0.0f), name(NULL) {}

    void setName(const char * name) {
        delete this->name;
        int len = strlen(name)+1;
        this->name = new char[len];
        for (int i = 0; i < len; i++) {
            this->name[i] = name[i];
        }
    }

    friend Packet& operator>>(Packet& packet, PlayerJoin& rhs) {
        packet >> rhs.x >> rhs.y >> rhs.z >> rhs.name;
        return packet;
    }

    friend NetMessage& operator<<(NetMessage& message, const PlayerJoin& rhs) {
        message << rhs.x << rhs.y << rhs.z << rhs.name;
        return message;
    }
};





#endif
