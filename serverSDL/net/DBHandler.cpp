#include "DBHandler.h"

DBHandler::DBHandler(const string& database, const string& host, uint16_t port, const string& user, const string & password) {
    this->database = database;
    this->host = host;
    this->port = port;
    this->user = user;
    this->password = password;
    conn = NULL;
}

bool DBHandler::connect() {
    stringstream query;
    query << "dbname="   << database << " "
          << "host="     << host     << " "
          << "port="     << port     << " "
          << "user="     << user     << " "
          << "password=" << password;

    conn = PQconnectdb(query.str().c_str());
    if (PQstatus(conn) == CONNECTION_BAD) {
        close();
        return false;
    }
    return true;
}

void DBHandler::close() {
    PQfinish(conn);
    conn = NULL;
}

bool DBHandler::tableExist(const string& name) {
    PGresult * res = NULL;
    stringstream query;
    if (!conn) return false;

    query << "SELECT * FROM " << name << ";";

    res = PQexec(conn, query.str().c_str());

    if (PQresultStatus(res) != PGRES_TUPLES_OK) {
        PQclear(res);
        return false;
    }

    PQclear(res);
    return true;
}

int32_t makeTable(const string& name, int32_t colCount, ...) {
    va_list columns;
    stringstream query;
    colCount *= 2;
    query << "CREATE TABLE " << name << " ";
    va_start(columns,colCount);
    for (int32_t i = 0; i < colCount; colCount += 2) {


    }
    va_end(columns);
    return 0;
}
