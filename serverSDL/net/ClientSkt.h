#ifndef _CLIENTTCP_H
#define _CLIENTTCP_H
#include <SDL.h>
#include <SDL_thread.h>
#include <winsock2.h>

#include <sstream>
#include <string>
#include <iostream>
#include <queue>

#include <string.h>

#include "net.h"
#include "NetMessage.h"
#include "Packet.h"

#ifndef UUID
#define UUID unsigned long long
#endif

#define CLIENT_RUNNING        1
#define CLIENT_CANNOT_RESOLVE 2
#define CLIENT_CANNOT_CONNECT 3

using namespace std;

extern bool net_init;

class NetMessage;
class Packet;

class ClientSkt {
public:
    ClientSkt(int port, const char * hostname, size_t size);
    ~ClientSkt();
    void sendMsg(NetMessage * message);
    int start();
    bool stop();
    ClientSkt * setPacketCallback(void (*packetReceived)(ClientSkt * client, Packet * packet));
    ClientSkt * setDicconectCallback(void (*disconnect)(ClientSkt * client));
protected:
private:
    static int recvEntry(void * args);
    void receive();
    static int sendEntry(void * args);
    void messageSender();

    void (*packetReceived)(ClientSkt * client, Packet * packet);
    void (*disconnect)(ClientSkt * client);

    int clientSocket;
    sockaddr_in addr;
    const char * hostname;
    int port;
    SDL_mutex * qlock;
    queue<NetMessage*> pqout;
    size_t size;
};

#endif // _CLIENTTCP_H
