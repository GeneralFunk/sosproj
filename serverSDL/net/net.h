#ifndef ____NET
#define ____NET

/**/
#define __WINDOWS
/**/

#ifdef __WINDOWS
    #include <winsock2.h>
#else
    #include <sys/types.h>
    #include <sys/socket.h>
    #include <netinet/in.h>
    #include <apra/inet.h>
    void closesocket(int socket);
#endif

#include <iostream>

extern bool ____net_init;
void startNet();
void closeNet();

#endif // ____NET
