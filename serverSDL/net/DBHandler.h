#ifndef __DB_HANDLER
#define __DB_HANDLER
#include <string>
#include <sstream>
#include <libpq-fe.h>
#include <vector>
#include <cstdarg>
#include <cstdint>

#define DBH_MT_SUCCESS             0
#define DBH_MT_ERROR_NO_CONNECTION 1
#define DBH_MT_ERROR_INVALID_ARGS  2

using namespace std;

class DBHandler;

struct Column {
    string name;
    uint8_t type;
    uint16_t flags;
    uint32_t arg1;
    uint32_t arg2;

    Column(const string& _name, uint8_t _type) :
        name(_name), type(_type), flags(0), arg1(0), arg2(0) {}
    Column(const string& _name, uint8_t _type, uint16_t _flags) :
        name(_name), type(_type), flags(_flags), arg1(0), arg2(0) {}
    Column(const string& _name, uint8_t _type, uint16_t _flags, uint32_t _arg1) :
        name(_name), type(_type), flags(_flags), arg1(_arg1), arg2(0) {}
    Column(const string& _name, uint8_t _type, uint16_t _flags, uint32_t _arg1, uint32_t _arg2) :
        name(_name), type(_type), flags(_flags), arg1(_arg1), arg2(_arg2) {}
};

class DBHandler {
public:
    DBHandler(const string& database, const string& host, uint16_t port, const string& user, const string & password);
    bool connect();
    void close();
    bool tableExist(const string& name);
    int32_t makeTable(const string& name, int32_t pKeyCol, int32_t colCount, ...);
    bool columnExists(const string& tableName, const string& columnName);
    void setValues(const string& tableName, const string& columnName);
protected:
private:
    string database;
    string host;
    uint16_t port;
    string user;
    string password;
    PGconn * conn;
};

#endif // __DB_HANDLER
