#ifndef _SERVERTCP_H
#define _SERVERTCP_H
#include <SDL.h>
#include <SDL_thread.h>
#include <winsock2.h>

#include <unordered_set>
#include <string>
#include <sstream>

#include "net.h"
#include "NetMessage.h"
#include "Packet.h"

#ifndef UUID
#define UUID unsigned long long
#endif

#ifndef SOCKADDR_SIZE
#define SOCKADDR_SIZE sizeof(sockaddr_in)
#endif

#define SERVER_RUNNING        1
#define SERVER_CANNOT_RESOLVE 2
#define SERVER_CANNOT_CONNECT 3

using namespace std;

class NetMessage;
class Packet;

struct SessionID {};

class ServerSkt {
public:
    ServerSkt(int port, size_t size);
    ~ServerSkt();
    ServerSkt * setPacketCallback(void (*packetReceived)(ServerSkt * server, SessionID * id, Packet * packet));
    ServerSkt * setDicconectCallback(void (*disconnect)(ServerSkt * server, SessionID * id));
    bool sendTo(SessionID * id, NetMessage * message);
    bool sendAll(NetMessage * message);
    bool broadcast(SessionID * id, NetMessage * message);
    int start();
    bool stop();
    static void init();
protected:
private:
    struct Connection {
        sockaddr_in addr;
        int socket;
        ServerSkt * server;
    };

    void (*packetReceived)(ServerSkt * server, SessionID * id, Packet * packet);
    void (*disconnect)(ServerSkt * server, SessionID * id);

    void listenFor();
    void receive(Connection * conn);

    static int listenEntry(void * args);
    static int recvEntry(void * args);

    int port;
    int thisSocket;
    sockaddr_in listenAddr;
    unordered_set<Connection*> connSet;

    size_t size;
};

#endif // _SERVERTCP_H
