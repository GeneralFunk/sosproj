#ifndef _TOKEN_H
#define _TOKEN_H
#include <cstdlib>
#include <stdio.h>
#include <string.h>
#include "../common/jahutils.h"

class NetMessage {

public:
    NetMessage(size_t type, size_t size);
    ~NetMessage();

    bool put(bool b);
    bool put(char c);
    bool put(int n);
    bool put(long l);
    bool put(float v);
    bool put(long long l);
    bool put(double d);
    bool put(unsigned char c);
    bool put(unsigned int n);
    bool put(unsigned long l);
    bool put(unsigned long long l);
    bool put(const char * str);
    bool put(const void * obj, size_t size);

    NetMessage& operator<<(bool b);
    NetMessage& operator<<(char c);
    NetMessage& operator<<(int n);
    NetMessage& operator<<(long l);
    NetMessage& operator<<(float v);
    NetMessage& operator<<(long long l);
    NetMessage& operator<<(double d);
    NetMessage& operator<<(unsigned char c);
    NetMessage& operator<<(unsigned int n);
    NetMessage& operator<<(unsigned long l);
    NetMessage& operator<<(unsigned long long l);
    NetMessage& operator<<(const char * str);

    const char * getData();
    size_t getSize();

protected:
private:
    size_t size;
    size_t pos;
    int type;
    char * buffer;
};

#endif // _TOKEN_H
