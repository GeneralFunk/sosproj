#include "Packet.h"

Packet::Packet(char * buffer, size_t size)
{
    this->size = size;
    this->buffer = buffer;
    this->pos = 0;
    this->error = false;
    type = getUInt();
}

void Packet::reset() {
    this->pos = 0;
}

int Packet::getType()
{
    return type;
}

const char * Packet::getData()
{
    return buffer;
}

bool Packet::getBool()
{
    if (sizeof(bool) + pos >= size)
    {
        error = true;
        return false;
    }
    bool b = *((bool*)(buffer + pos));
    pos += sizeof(bool);
    return b;
}

char Packet::getChar()
{
    if (sizeof(char) + pos >= size)
    {
        error = true;
        return 0;
    }
    char c = *((char*)(buffer+pos));
    pos += sizeof(char);
    return c;
}

int Packet::getInt()
{
    if (sizeof(int) + pos >= size)
    {
        error = true;
        return 0;
    }
    int n = *((int*)(buffer + pos));
    pos += sizeof(int);
    return n;
}

float Packet::getFloat()
{
    if (sizeof(float) + pos >= size)
    {
        error = true;
        return 0;
    }
    float v = *((float*)(buffer + pos));
    pos += sizeof(float);
    return v;
}

long long Packet::getLLong()
{
    if (sizeof(long long) + pos >= size)
    {
        error = true;
        return 0;
    }
    long long ll = *((long long*)(buffer + pos));
    pos += sizeof(long long);
    return ll;
}

double Packet::getDouble()
{
    if (sizeof(double) + pos >= size)
    {
        error = true;
        return 0;
    }
    double d = *((double*)(buffer + pos));
    pos += sizeof(double);
    return d;
}

unsigned char Packet::getUChar()
{
    if (sizeof(unsigned char) + pos >= size)
    {
        error = true;
        return 0;
    }
    unsigned char uc = *((unsigned char*)(buffer + pos));
    pos += sizeof(unsigned char);
    return uc;
}

unsigned int Packet::getUInt()
{
    if(sizeof(unsigned int) + pos >= size)
    {
        error = true;
        return 0;
    }
    unsigned int un = *((unsigned int*)(buffer + pos));
    pos += sizeof(unsigned int);
    return un;
}

unsigned long Packet::getULong()
{
    if(sizeof(unsigned long) + pos >= size)
    {
        error = true;
        return 0;
    }
    unsigned long ul = *((unsigned long*)(buffer + pos));
    pos += sizeof(unsigned long);
    return ul;
}

unsigned long long Packet::getULLong()
{
    if(sizeof(unsigned long long) + pos >= size)
    {
        error = true;
        return 0;
    }
    unsigned long long ull = *((unsigned long long*)(buffer + pos));
    pos += sizeof(ull);
    return ull;
}

/*
// Get string length (+1 to include null terminator)
// Allocate space for new string
// put buffer data into string
// move buffer position
// return string
*/
char * Packet::getStr()
{
    int len = sstrlen(buffer + pos, size-pos);
    if (len == -1)
    {
        error = true;
        return NULL;
    }
    len++;
    if (len + pos >= size)
    {
        error = true;
        return NULL;
    }
    char * str = new char[len];
    memcpy(str, buffer+pos, len);
    pos += len;
    return str;
}

/*
// Check for size
// Allocate space for object
// Copy buffer data to object
// move buffer position
// return object
*/
void * Packet::getObj(size_t size)
{
    if (pos + size >= this->size)
    {
        error = true;
        return NULL;
    }
    char * obj = new char[size];
    memcpy(obj, buffer+pos, size);
    pos += size;
    return obj;
}

void getToStr(char * str, unsigned int size) {

}

void getToObj(void * obj, unsigned int size) {

}

const char * Packet::Packet::getConstStr()
{
    int len = sstrlen(buffer + pos, size-pos);
    if (len == -1)
    {
        error = true;
        return NULL;
    }
    char * str = buffer+pos;
    pos += len;
    return str;
}

const void * Packet::Packet::getConstObj(size_t size)
{
    void * obj = buffer+pos;
    pos+=size;
    return obj;
}


Packet& Packet::operator>>(bool& b)
{
    b = getBool();
    return *this;
}

Packet& Packet::operator>>(char& c)
{
    c = getChar();
    return *this;
}

Packet& Packet::operator>>(int& n)
{
    n = getInt();
    return *this;
}

Packet& Packet::operator>>(float& f)
{
    f = getFloat();
    return *this;
}

Packet& Packet::operator>>(long long& ll)
{
    ll = getLLong();
    return *this;
}

Packet& Packet::operator>>(double& d)
{
    d = getDouble();
    return *this;
}

Packet& Packet::operator>>(unsigned char& c)
{
    c = getUChar();
    return *this;
}

Packet& Packet::operator>>(unsigned int& n)
{
    n = getUInt();
    return *this;
}

Packet& Packet::operator>>(unsigned long long& ll)
{
    ll = getULLong();
    return *this;
}

Packet& Packet::operator>>(char *& str)
{
    str = getStr();
    return *this;
}

Packet& Packet::operator>>(const char *& str)
{
    str = getConstStr();
    return *this;
}
